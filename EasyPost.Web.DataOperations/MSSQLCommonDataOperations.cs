﻿using UFS.DatabaseEntity;
using UFS.Web.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace UFS.Web.DataOperations
{
    public class MSSQLCommonDataOperations
    {
        private MSSQLDataService dao = null;
        public List<KeyValuePairClass> GetCarriers()
        {
            List<KeyValuePairClass> lstCarriers = new List<KeyValuePairClass>();
            dao = new MSSQLDataService();
            string query = "SELECT *  FROM carriers ";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    lstCarriers.Add(
                    new KeyValuePairClass()
                    {
                        Key = Utility.GetString(item["Value"]),
                        Value = Utility.GetString(item["Description"])
                    }
                    );
                }


            }
            return lstCarriers;
        }
        public List<KeyValuePairClass> GetParcels(string carrier)
        {
            List<KeyValuePairClass> lstCarriersParcel = new List<KeyValuePairClass>();
            lstCarriersParcel.Add(new KeyValuePairClass()
            {
                Value = "Custom",
                Key = "Custom"
            });
            dao = new MSSQLDataService();
            string query = "SELECT *  FROM parcel where Carrier = '" + carrier + "'";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    lstCarriersParcel.Add(
                        new KeyValuePairClass()
                        {
                            Key = Utility.GetString(item["ParcelCode"]),
                            Value = Utility.GetString(item["Parcel"])
                        }
                        );
                }

            }
            return lstCarriersParcel;
        }

        public void CancelOrder(string orderId, int userId)
        {
            try
            {
                string query = "UPDATE [dbo].[ShopifyOrders] SET OrderStatus='Cancelled' WHERE OrderId = " + orderId + " AND UserId = " + userId.ToString();
                new MSSQLDataService().ExecuteQuery(query);
            }
            catch (Exception ex)
            {

            }
        }

        public void DeleteOrder(string orderId, int userId)
        {
            try
            {
                MSSQLDataService srv = new MSSQLDataService();
                string query = "DELETE FROM [dbo].[OrderLineItems] WHERE OrderNumber IN ( SELECT OrderNumber FROM  ShopifyOrders WHERE OrderId= " + orderId + " AND UserId = " + userId.ToString() + " ) AND  UserId = " + userId.ToString();
                srv.ExecuteQuery(query);
                query = "DELETE FROM [dbo].[ShopifyOrders] WHERE OrderId = " + orderId + " AND UserId = " + userId.ToString();
                srv.ExecuteQuery(query);

            }
            catch (Exception ex)
            {

            }
        }

        public List<KeyValuePairClass> GetStates()
        {
            List<KeyValuePairClass> lstCarriersParcel = new List<KeyValuePairClass>();

            dao = new MSSQLDataService();
            string query = "Select  StateName,StateAbbrivation from States  ORDER BY StateName";
            DataSet ds = dao.ExecuteDataSet(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    lstCarriersParcel.Add(
                        new KeyValuePairClass()
                        {
                            Key = Utility.GetString(item["StateAbbrivation"]),
                            Value = Utility.GetString(item["StateName"])
                        }
                        );
                }

            }
            return lstCarriersParcel;
        }

        public void UpdateOrdersForSummary(int userId, string orderids)
        {
            try
            {
                string summaryId = "";// DateTime.Now.ToString("yyyyMMddHHmmss") + "" + orderids.Split(new char[] { ',' })[0].Replace(",", "");
                Random rnd = new Random();
                int myRandomNo = rnd.Next(1, 9999);
                summaryId = myRandomNo.ToString("0000");
                string[] arr = orderids.Split(new char[] { ',' });
                string query = "";
                foreach (var item in arr)
                {
                    query = query + "  UPDATE [dbo].[ShopifyOrders] SET SummaryNumber='" + summaryId + "',UserId=" + userId.ToString() + ",Updated=GETDATE(),LastUpdated=GETDATE(),SummaryDate=GETDATE() WHERE OrderId = " + item + " AND UserId = " + userId.ToString() + "   ";
                }

                new MSSQLDataService().ExecuteQuery(query);
            }
            catch (Exception ex)
            {

            }
        }

        public string CancelSummary(string summarynumber)
        {
            try
            {
                string query = "  UPDATE [dbo].[ShopifyOrders] SET SummaryNumber=NULL WHERE SummaryNumber = '" + summarynumber + "'";
                return new MSSQLDataService().ExecuteQuery(query) > 0 ? "Summary has been cancelled. Click 'Close' to return" : "Unable to cancel the order this time. Please try again later or contact system administrator";
            }
            catch (Exception ex)
            {
                return "Unable to cancel the order this time. Please try again later or contact system administrator.";
            }
        }
        public List<string> GetSummaryIds(int userId)
        {
            List<string> res = new List<string>();
            string query = "SELECT DISTINCT SummaryNumber FROM [dbo].[ShopifyOrders] WHERE UserId=" + userId.ToString();
            DataSet ds = new MSSQLDataService().ExecuteDataSet(query);
            if (Utility.IsValidDataSet(ds))
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (!string.IsNullOrWhiteSpace(Utility.GetString(ds.Tables[0].Rows[i]["SummaryNumber"])))
                    {
                        res.Add(Utility.GetString(ds.Tables[0].Rows[i]["SummaryNumber"]));
                    }
                }
            }
            return res;
        }
        public string CancelOrderSummary(string orderNumber, int userId)
        {
            try
            {
                string query = "  UPDATE [dbo].[ShopifyOrders] SET SummaryNumber=NULL WHERE OrderNumber = '" + orderNumber + "' AND UserId=" + userId.ToString();
                return new MSSQLDataService().ExecuteQuery(query) > 0 ? "Summary has been cancelled. Click 'Close' to return" : "Unable to cancel the order this time. Please try again later or contact system administrator";
            }
            catch (Exception ex)
            {
                return "Unable to cancel the order this time. Please try again later or contact system administrator.";
            }
        }
        public string CancelMultiOrderSummary(string orderNumbers, int userId)
        {
            try
            {
                string query = "  UPDATE [dbo].[ShopifyOrders] SET SummaryNumber=NULL WHERE UserId=" + userId.ToString() + " AND OrderId IN(" + orderNumbers + ")";

                return new MSSQLDataService().ExecuteQuery(query) > 0 ? "Orders has been removed from summary. Click 'Close' to return" : "Unable to cancel the order this time. Please try again later or contact system administrator";
            }
            catch (Exception ex)
            {
                return "Unable to cancel the order this time. Please try again later or contact system administrator.";
            }
        }

        public void UpdateSummaryLabels(string orderids, int userId)
        {
            try
            {
                string[] arr = orderids.Split(new char[] { ',' });
                string query = "";
                foreach (var item in arr)
                {
                    query = query + "  UPDATE [dbo].[ShopifyOrders] SET SummaryNumber=NULL,Updated=GETDATE(),LastUpdated=GETDATE() WHERE OrderId = " + item + " AND UserId = " + userId.ToString() + "   ";
                }

                new MSSQLDataService().ExecuteQuery(query);
            }
            catch (Exception ex)
            {

            }
        }

        public void AddOrderSummary(string orderId, string summaryId, int userId, bool isMainButton)
        {
            try
            {
                string query = "";

                if (!isMainButton)
                {
                    query = "  UPDATE [dbo].[ShopifyOrders] SET SummaryNumber='" + summaryId + "',Updated=GETDATE(),LastUpdated=GETDATE() WHERE OrderId IN(" + orderId + ") AND UserId = " + userId.ToString() + "   ";
                }
                else
                {
                    query = "  UPDATE [dbo].[ShopifyOrders] SET SummaryNumber='" + summaryId + "',Updated=GETDATE(),LastUpdated=GETDATE() WHERE OrderNumber = '" + orderId + "' AND UserId = " + userId.ToString() + "   ";
                }
                new MSSQLDataService().ExecuteQuery(query);
            }
            catch (Exception ex)
            {

            }
        }
    }

}
