﻿using EasyPost;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace UFS.Web.Wrapper
{
    public class FedExOperations
    {
        public FedExOperations()
        {

        }
        public Address FromAddress { get; set; }
        public Address ToAddress { get; set; }
        public Parcel Parcel { get; set; }
        public List<Parcel> Parcels { get; set; }
        public Shipment ShipmentObject { get; set; }

        public Dictionary<string,object> CreateOrders(string carrier,string service)
        {
            Order order = new Order();
            order.from_address = FromAddress;
            order.to_address = ToAddress;
            if (Parcels != null && Parcels.Count > 0)
            {
                foreach (var item in Parcels)
                {
                    order.shipments.Add(new Shipment() { parcel = item });
                }
            }
            else {
                order.shipments.Add(new Shipment() { parcel = Parcel });
            }
            order.Buy(carrier, service);
            Dictionary<string, object> dict = new Dictionary<string, object>();
            List<string> labels = new List<string>();
            labels.AddRange(order.shipments.Select(m => m.postage_label.label_url));
            dict.Add("label",labels);

            return dict;
        }

    }
}
