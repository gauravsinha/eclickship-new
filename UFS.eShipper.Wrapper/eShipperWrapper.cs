﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UFS.Web.BusinesEntities;
using UFS.eShipper.Service;
using UFS.eShipper.Service.Response;
using UFS.Web.DataOperations;
using System.IO;

namespace UFS.eShipper.Wrapper
{
    public class eShipperWrapper
    {
        public Dictionary<object, string> Messages { get; set; }
        public List<eShipperRate> Rates { get; set; }
        public ShipmentDetail Shipment { get; set; }
        public List<string> Images { get; set; }
        public List<string> TrackingNumbers { get; set; }
        public Dictionary<string, string> ShippingLabel { get; set; }
        public string ServiceName { get; set; }
        public string CarrierName { get; set; }
        public string FromAddressId { get; set; }
        public string ToAddressId { get; set; }
        public string ServiceId { get; set; }
        public string AmountCharged { get; set; }
        public string UniqueObjectCode
        {
            get
            {

                return DateTime.Now.Year.ToString() + "" + DateTime.Now.Month.ToString() + "" + DateTime.Now.Day.ToString() + "" + DateTime.Now.Hour.ToString() + "" + DateTime.Now.Minute.ToString() + "" + DateTime.Now.Second.ToString() + "" + DateTime.Now.Millisecond.ToString();
            }
        }

        public long OrderNumber { get; set; }

        public void Create()
        {
            eShipperService srv = new eShipperService();
            Service.Request.eShipperQuoteRequest rq = new Service.Request.eShipperQuoteRequest();

            rq.Data = new Service.Request.EShipper();
            rq.Data.QuoteRequest = new Service.Request.QuoteRequest();
            if (Shipment.FromAddress != null)
            {
                rq.Data.QuoteRequest.From = new Service.Request.From();
                rq.Data.QuoteRequest.From.Address1 = Shipment.FromAddress.Street1 + "" + Shipment.FromAddress.Street2;
                rq.Data.QuoteRequest.From.City = Shipment.FromAddress.City;
                rq.Data.QuoteRequest.From.Company = Shipment.FromAddress.Company;
                rq.Data.QuoteRequest.From.Country = Shipment.FromAddress.Country;
                rq.Data.QuoteRequest.From.State = Shipment.FromAddress.State;
                rq.Data.QuoteRequest.From.Zip = Shipment.FromAddress.Zip;
            }
            if (Shipment.ToAddress != null)
            {
                rq.Data.QuoteRequest.To = new Service.Request.To();
                rq.Data.QuoteRequest.To.Address1 = Shipment.ToAddress.Street1 + "" + Shipment.ToAddress.Street2;
                rq.Data.QuoteRequest.To.City = Shipment.ToAddress.City;
                rq.Data.QuoteRequest.To.Company = Shipment.ToAddress.Company;
                rq.Data.QuoteRequest.To.Country = Shipment.ToAddress.Country;
                rq.Data.QuoteRequest.To.State = Shipment.ToAddress.State;
                rq.Data.QuoteRequest.To.Zip = Shipment.ToAddress.Zip;
            }
            if (Shipment.Parcels != null && Shipment.Parcels.Count > 0)
            {
                rq.Data.QuoteRequest.Packages = new Service.Request.Packages();
                rq.Data.QuoteRequest.Packages.Type = Shipment.Parcel.PredefinedParcel;
                rq.Data.QuoteRequest.Packages.Package = new List<Service.Request.Package>();
                Service.Request.Package pac;
                foreach (var item in Shipment.Parcels)
                {
                    pac = new Service.Request.Package();
                    pac.Width = item.Width;
                    pac.Height = item.Height;
                    pac.Length = item.Length;
                    pac.Weight = item.Weight;
                    pac.Type = item.PredefinedParcel;
                    rq.Data.QuoteRequest.Packages.Package.Add(pac);
                }
            }
            Service.Response.eShipperQuoteResponse rs = srv.GetQuote(rq);
            if (rs != null)
            {
                ProcessQuoteResponse(rs);
            }
            else
            {
                Messages = new Dictionary<object, string>();
                Messages.Add("100", "Unable to get rate at this time");
            }
        }

        private void ProcessQuoteResponse(eShipperQuoteResponse rs)
        {
            if (rs.ResponseData != null && rs.ResponseData.QuoteReply != null && rs.ResponseData.QuoteReply.Quote != null && rs.ResponseData.QuoteReply.Quote.Count > 0)
            {
                //FromAddressId = rs.ResponseData.QuoteReply.Quote[0].
                Rates = new List<eShipperRate>();
                foreach (var item in rs.ResponseData.QuoteReply.Quote)
                {
                    Rates.Add(new eShipperRate()
                    {
                        Amount = item.TotalCharge,
                        Currency = item.Currency,
                        DeliveryDate = item.DeliveryDate,
                        Message = item.CarrierName,
                        RateId = item.ServiceId,
                        ServiceId = item.CarrierId,
                        ServiceDesc = GetServiceName(item.ServiceId)// item.CarrierName + " - " + item.ServiceName
                    });
                }
                //if (rs.ResponseData.ErrorReply!=null && rs.ResponseData.ErrorReply.Error!=null)
                //{
                //    Messages = new Dictionary<object, string>();
                //    Messages.Add("100", rs.ResponseData.ErrorReply.Error.Message);
                //}
            }
            //rs.ResponseData.QuoteReply.Quote
        }

        private string GetServiceName(string serviceId)
        {
            MSSQLUserOperations op = new MSSQLUserOperations();
            return op.GetEShipperCarrierName(serviceId);
        }

        public void Buy(EasyPost.Order ordr)
        {
            eShipperService srv = new eShipperService();
            Service.Request.eShipperShippingRequest rq = new Service.Request.eShipperShippingRequest();
            rq.Data = new Service.Request.ShippingRequestObject();
            rq.Data.ShippingRequest = new Service.Request.ShippingRequest();
            rq.Data.ShippingRequest.ServiceId = ServiceId;
            rq.Data.ShippingRequest.InsuranceType = "eShipper";
            if (ordr.from_address != null)
            {
                rq.Data.ShippingRequest.From = new Service.Request.From();
                rq.Data.ShippingRequest.From.Id = "0";
                rq.Data.ShippingRequest.From.Address1 = ordr.from_address.street1;
                rq.Data.ShippingRequest.From.Address2 = ordr.from_address.street2;
                rq.Data.ShippingRequest.From.City = ordr.from_address.city;
                rq.Data.ShippingRequest.From.Company = !string.IsNullOrWhiteSpace(ordr.from_address.company) ? ordr.from_address.company : ordr.from_address.name;
                rq.Data.ShippingRequest.From.Country = ordr.from_address.country;
                rq.Data.ShippingRequest.From.State = ordr.from_address.state;
                rq.Data.ShippingRequest.From.Zip = ordr.from_address.zip;
                rq.Data.ShippingRequest.From.Phone = ordr.from_address.phone;
                rq.Data.ShippingRequest.From.Email = ordr.from_address.email;
                rq.Data.ShippingRequest.From.Residential = "true";
                rq.Data.ShippingRequest.From.Attention = !string.IsNullOrWhiteSpace(ordr.from_address.name) ? ordr.from_address.name : ordr.from_address.company;
            }
            if (ordr.to_address != null)
            {
                rq.Data.ShippingRequest.To = new Service.Request.To();
                rq.Data.ShippingRequest.To.Id = "0";
                rq.Data.ShippingRequest.To.Address1 = ordr.to_address.street1;
                rq.Data.ShippingRequest.To.Address2 = ordr.to_address.street2;
                rq.Data.ShippingRequest.To.City = ordr.to_address.city;
                rq.Data.ShippingRequest.To.Company = ordr.to_address.company;
                rq.Data.ShippingRequest.To.Country = ordr.to_address.country;
                rq.Data.ShippingRequest.To.State = ordr.to_address.state;
                rq.Data.ShippingRequest.To.Zip = ordr.to_address.zip;
                rq.Data.ShippingRequest.To.Phone = ordr.to_address.phone;
                rq.Data.ShippingRequest.To.Email = ordr.to_address.email;
                rq.Data.ShippingRequest.To.Residential = "true";
                rq.Data.ShippingRequest.To.Attention = !string.IsNullOrWhiteSpace(ordr.to_address.name) ? ordr.to_address.name : ordr.to_address.company;
            }
            
            if (ordr.shipments != null && ordr.shipments.Count > 0)
            {
                rq.Data.ShippingRequest.Packages = new Service.Request.Packages();
                rq.Data.ShippingRequest.Packages.Type = "Package";
                rq.Data.ShippingRequest.Packages.Package = new List<Service.Request.Package>();
                Service.Request.Package pac;
                foreach (var item in ordr.shipments)
                {
                    pac = new Service.Request.Package();
                    pac.Width = item.parcel.width.ToString();
                    pac.Height = item.parcel.height.ToString();
                    pac.Length = item.parcel.length.ToString();
                    pac.Weight = item.parcel.weight.ToString();
                    pac.InsuranceAmount = "300.00";
                    //pac.Type = item.parcel.predefined_package!=null? item.parcel.predefined_package : "Package";
                    rq.Data.ShippingRequest.Packages.Package.Add(pac);
                }
                rq.Data.ShippingRequest.Payment = new Service.Request.Payment()
                {
                    Type = "Check"
                };
            }
            rq.Data.ShippingRequest.Reference = new List<Service.Request.Reference>();
            rq.Data.ShippingRequest.Reference.Add(new Service.Request.Reference()
            {
                Name = "Order Number",
                Code = OrderNumber.ToString()
            });
            Service.Response.eShipperShippingResponse rs = srv.ShipPackage(rq);
            if (rs != null)
            {
                ProcessShippingResponse(rs);
            }
            else
            {
                Messages = new Dictionary<object, string>();
                Messages.Add("100", "Unable to generate lebel this time");
            }
        }

        private void ProcessShippingResponse(eShipperShippingResponse rs)
        {
            Images = new List<string>();
            TrackingNumbers = new List<string>();
            ShippingLabel = new Dictionary<string, string>();
            string imageURL = "";
            int iCount = 0;
            if (rs != null && rs.ResponseData != null && rs.ResponseData.ShippingReply != null && rs.ResponseData.ShippingReply.Labels != null)
            {
                Images = new List<string>();
                TrackingNumbers = new List<string>();
                ShippingLabel = new Dictionary<string, string>();
                var item = rs.ResponseData.ShippingReply;
                if (rs.ResponseData.ShippingReply.Quote != null && !string.IsNullOrWhiteSpace(rs.ResponseData.ShippingReply.Quote.TotalCharge))
                {
                    AmountCharged = rs.ResponseData.ShippingReply.Quote.TotalCharge;
                }
                else
                {
                    AmountCharged = "0.00";
                }
                if (item.LabelData != null)
                {
                    foreach (var item1 in rs.ResponseData.ShippingReply.LabelData.Label)
                    {

                        CarrierName = item.Carrier.CarrierName;
                        ServiceName = item.Carrier.ServiceName;
                        TrackingNumbers.Add(item.Package[iCount].TrackingNumber);
                        // string htmlImage = WriteHTML(item.Labels, item.Package.TrackingNumber);
                        imageURL = WriteImage(item1.Data, item1.Type, item.Package[iCount].TrackingNumber);
                        if (!string.IsNullOrWhiteSpace(imageURL))
                        {
                            Images.Add(imageURL);
                            ShippingLabel.Add(item.Package[iCount].TrackingNumber, imageURL);
                        }
                        iCount++;
                    }
                }
                else
                {
                    CarrierName = item.Carrier.CarrierName;
                    ServiceName = item.Carrier.ServiceName;
                    foreach (var pack in item.Package)
                    {
                        TrackingNumbers.Add(pack.TrackingNumber);
                        imageURL = WriteImage(item.Labels, "pdf", pack.TrackingNumber);
                        if (!string.IsNullOrWhiteSpace(imageURL))
                        {
                            Images.Add(imageURL);
                            ShippingLabel.Add(pack.TrackingNumber, imageURL);
                        }
                    }
                }
            }
            else
            {
                Messages = new Dictionary<object, string>();
                Messages.Add("100", rs.ResponseData.ErrorReply.Error.Message);
            }
        }
        private string WriteHTML(string hTMLImage, string trackingNumber)
        {
            string path = "~/Labels/" + trackingNumber + ".html";
            string htmlString = Base64Decode(hTMLImage);
            byte[] bytes = Encoding.ASCII.GetBytes(htmlString);
            CreateFile(path, bytes);
            return path;

        }

        private string WriteImage(string base64ImageString, string type, string trackingNumber)
        {
            string path = "~/Labels/label" + trackingNumber + "." + type;
            try
            {
                byte[] bytes = Convert.FromBase64String(base64ImageString);
                CreateFile(path, bytes);
            }
            catch (Exception ex)
            {
                path = "";
            }
            return path.Replace("~", "..");
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        private static void CreateFile(string path, byte[] bytes)
        {
            if (File.Exists(path))
                File.Delete(path);
            var fs = new BinaryWriter(new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(path), FileMode.CreateNew, FileAccess.Write));
            fs.Write(bytes);
            fs.Close();
        }

    }

    public static class Constants
    {
        public const string Package = "Package";
        public const string Envelope = "Envelope";
        public const string CourierPak = "Courier Pak";
        public const string Pallet = "Pallet";

    }
}
