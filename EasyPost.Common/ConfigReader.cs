﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UFS.Common;
namespace UFS.Common
{
    public sealed class ConfigReader
    {
        private static volatile ConfigReader _instance;
        private static object syncRoot = new object();
        private config _config;
        private ConfigReader()
        {
            string path = "";
            path = ConfigurationManager.AppSettings["Config"].ToString();
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            // Config result;

            var serializer = new XmlSerializer(typeof(config));

            using (var reader = XmlReader.Create(fs))
            {
                _config = (config)serializer.Deserialize(reader);
            }
        }
        public static ConfigReader Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new ConfigReader();
                        }
                    }
                }
                return _instance;
            }
        }

        public config LoadData()
        {
            return _config;
        }
    }
}
