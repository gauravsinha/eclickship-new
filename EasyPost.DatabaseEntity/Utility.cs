﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace UFS.DatabaseEntity
{
    public static class Utility
    {
        public static bool IsValidDataSet(DataSet ds)
        {
            return ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0;
        }
        public static bool IsValidDataSet(DataSet ds, int tableIndex)
        {
            return ds != null && ds.Tables != null && ds.Tables.Count > tableIndex && ds.Tables[tableIndex].Rows.Count > 0;
        }
        public static bool IsValidDataTable(DataTable dt)
        {
            return dt != null && dt.Rows.Count > 0;
        }
        public static int ParseInt(string value)
        {
            int result;
            if (Int32.TryParse(value, out result))
                return result;
            else
                return 0;
        }

        public static int ParseDecimalToInt(string value)
        {
            decimal result;
            int val = decimal.TryParse(value, out result) ? (int)result : 0;
            return val;
        }

        public static long ParseLong(string value)
        {
            long result;
            if (long.TryParse(value, out result))
                return result;
            else
                return 0;
        }
        public static bool ParseBool(string value)
        {
            if (value.Trim().Equals("1"))
                return true;

            bool result;
            bool.TryParse(value, out result);
            return result;
        }
        public static bool IsValidDate(string date)
        {
            DateTime dt;
            return DateTime.TryParse(date, out dt);
        }
        public static Nullable<DateTime> ParseDate(string date)
        {
            DateTime dt;
            if (DateTime.TryParse(date, out dt))
                return dt;
            else
                return null;
        }
        public static string GetString(object val)
        {
            return val != null ? val.ToString() : "";
        }
        public static Int32 GetInt(object val)
        {
            return val != null ? ParseInt(val.ToString()) : 0;
        }
        public static int GetDoubletoInt(object val)
        {
            return val != null ? ParseDecimalToInt(val.ToString()) : 0;
        }
        public static long GetLong(object val)
        {
            return val != null ? ParseLong(val.ToString()) : 0;
        }
        public static bool GetBoolean(object val)
        {
            return val != null ? ParseBool(val.ToString()) : false;
        }
        public static Nullable<DateTime> GetDateTime(object val)
        {
            return val != null ? ParseDate(val.ToString()) : null;
        }

        public static string GetDateAsString(object val)
        {
            if (val != null && !string.IsNullOrWhiteSpace(val.ToString()))
            {
                var dt = ParseDate(val.ToString());
                if (dt.HasValue)
                {
                    return dt.Value.Date.ToString("MM/dd/yyyy");
                }
                return string.Empty;
            }
            return string.Empty;
        }
        public static string GetDateTimeString(object val)
        {
            if (val != null && !string.IsNullOrWhiteSpace(val.ToString()))
            {
                var dt = ParseDate(val.ToString());
                if (dt.HasValue)
                {
                    return dt.Value.Date.ToString("MM/dd/yyyy hh:mm");
                }
                return string.Empty;
            }
            return string.Empty;
        }
        public static double GetDouble(object val)
        {
            double objval = 0;
            Double.TryParse(val.ToString(), out objval);
            return objval;
        }
    }
}
