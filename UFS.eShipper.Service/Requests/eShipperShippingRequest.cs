﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UFS.eShipper.Service.Request
{
    public class eShipperShippingRequest
    {
        public ShippingRequestObject Data { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Version { get; set; }
    }

    [XmlRoot(ElementName = "Payment", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class Payment
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "Reference", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class Reference
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "BillTo", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class BillTo
    {
        [XmlAttribute(AttributeName = "company")]
        public string Company { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "address1")]
        public string Address1 { get; set; }
        [XmlAttribute(AttributeName = "city")]
        public string City { get; set; }
        [XmlAttribute(AttributeName = "state")]
        public string State { get; set; }
        [XmlAttribute(AttributeName = "zip")]
        public string Zip { get; set; }
        [XmlAttribute(AttributeName = "country")]
        public string Country { get; set; }
    }

    [XmlRoot(ElementName = "Contact", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class Contact
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "phone")]
        public string Phone { get; set; }
    }

    [XmlRoot(ElementName = "Item", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class Item
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "description")]
        public string Description { get; set; }
        [XmlAttribute(AttributeName = "originCountry")]
        public string OriginCountry { get; set; }
        [XmlAttribute(AttributeName = "quantity")]
        public string Quantity { get; set; }
        [XmlAttribute(AttributeName = "unitPrice")]
        public string UnitPrice { get; set; }
    }

    [XmlRoot(ElementName = "DutiesTaxes", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class DutiesTaxes
    {
        [XmlAttribute(AttributeName = "dutiable")]
        public string Dutiable { get; set; }
        [XmlAttribute(AttributeName = "billTo")]
        public string BillTo { get; set; }
    }

    [XmlRoot(ElementName = "InBondManifest", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class InBondManifest
    {
        [XmlAttribute(AttributeName = "locationOfGoods")]
        public string LocationOfGoods { get; set; }
        [XmlAttribute(AttributeName = "nameOfCarrier")]
        public string NameOfCarrier { get; set; }
        [XmlAttribute(AttributeName = "vehicleIdentification")]
        public string VehicleIdentification { get; set; }
        [XmlAttribute(AttributeName = "customsClearedBy")]
        public string CustomsClearedBy { get; set; }
        [XmlAttribute(AttributeName = "handlingInfo")]
        public string HandlingInfo { get; set; }
        [XmlAttribute(AttributeName = "previousCargoControlNum")]
        public string PreviousCargoControlNum { get; set; }
        [XmlAttribute(AttributeName = "weight")]
        public string Weight { get; set; }
        [XmlAttribute(AttributeName = "weightUOM")]
        public string WeightUOM { get; set; }
    }

    [XmlRoot(ElementName = "CustomsInvoice", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class CustomsInvoice
    {
        [XmlElement(ElementName = "BillTo", Namespace = "http://www.eshipper.net/XMLSchema")]
        public BillTo BillTo { get; set; }
        [XmlElement(ElementName = "Contact", Namespace = "http://www.eshipper.net/XMLSchema")]
        public Contact Contact { get; set; }
        [XmlElement(ElementName = "Item", Namespace = "http://www.eshipper.net/XMLSchema")]
        public Item Item { get; set; }
        [XmlElement(ElementName = "DutiesTaxes", Namespace = "http://www.eshipper.net/XMLSchema")]
        public DutiesTaxes DutiesTaxes { get; set; }
        [XmlElement(ElementName = "InBondManifest", Namespace = "http://www.eshipper.net/XMLSchema")]
        public InBondManifest InBondManifest { get; set; }
        [XmlAttribute(AttributeName = "brokerName")]
        public string BrokerName { get; set; }
        [XmlAttribute(AttributeName = "contactCompany")]
        public string ContactCompany { get; set; }
        [XmlAttribute(AttributeName = "contactName")]
        public string ContactName { get; set; }
    }

    [XmlRoot(ElementName = "ShippingRequest", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class ShippingRequest
    {
        [XmlElement(ElementName = "From", Namespace = "http://www.eshipper.net/XMLSchema")]
        public From From { get; set; }
        [XmlElement(ElementName = "To", Namespace = "http://www.eshipper.net/XMLSchema")]
        public To To { get; set; }
        [XmlElement(ElementName = "COD", Namespace = "http://www.eshipper.net/XMLSchema")]
        public COD COD { get; set; }
        [XmlElement(ElementName = "Packages", Namespace = "http://www.eshipper.net/XMLSchema")]
        public Packages Packages { get; set; }
        [XmlElement(ElementName = "Pickup", Namespace = "http://www.eshipper.net/XMLSchema")]
        public Pickup Pickup { get; set; }
        [XmlElement(ElementName = "Payment", Namespace = "http://www.eshipper.net/XMLSchema")]
        public Payment Payment { get; set; }
        [XmlElement(ElementName = "Reference", Namespace = "http://www.eshipper.net/XMLSchema")]
        public List<Reference> Reference { get; set; }
        [XmlElement(ElementName = "CustomsInvoice", Namespace = "http://www.eshipper.net/XMLSchema")]
        public CustomsInvoice CustomsInvoice { get; set; }
        [XmlAttribute(AttributeName = "serviceId")]
        public string ServiceId { get; set; }
        [XmlAttribute(AttributeName = "stackable")]
        public string Stackable { get; set; }

        [XmlAttribute(AttributeName = "insuranceType")]
        public string InsuranceType { get; set; }
    }

    [XmlRoot(ElementName = "EShipper", Namespace = "http://www.eshipper.net/XMLSchema")]
    public class ShippingRequestObject
    {
        [XmlElement(ElementName = "ShippingRequest", Namespace = "http://www.eshipper.net/XMLSchema")]
        public ShippingRequest ShippingRequest { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "username")]
        public string Username { get; set; }
        [XmlAttribute(AttributeName = "password")]
        public string Password { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
    }
}
