﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace UFS.eShipper.Service.Response
{
    public class eShipperShippingResponse
    {
        public eShipperShippingResponseData ResponseData { get; set; }
    }

    [XmlRoot(ElementName = "Order", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class Order
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "Carrier", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class Carrier
    {
        [XmlAttribute(AttributeName = "carrierName")]
        public string CarrierName { get; set; }
        [XmlAttribute(AttributeName = "serviceName")]
        public string ServiceName { get; set; }
    }

    [XmlRoot(ElementName = "Package", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class Package
    {
        [XmlAttribute(AttributeName = "trackingNumber")]
        public string TrackingNumber { get; set; }
    }

    [XmlRoot(ElementName = "Pickup", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class Pickup
    {
        [XmlAttribute(AttributeName = "errorMessage")]
        public string ErrorMessage { get; set; }
    }

    [XmlRoot(ElementName = "Label", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class Label
    {
        [XmlElement(ElementName = "Type", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public string Type { get; set; }
        [XmlElement(ElementName = "Data", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public string Data { get; set; }
    }

    [XmlRoot(ElementName = "LabelData", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class LabelData
    {
        [XmlElement(ElementName = "Label", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public List<Label> Label { get; set; }
    }

    [XmlRoot(ElementName = "Quote", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class ShippingQuote
    {
        [XmlAttribute(AttributeName = "carrierId")]
        public string CarrierId { get; set; }
        [XmlAttribute(AttributeName = "carrierName")]
        public string CarrierName { get; set; }
        [XmlAttribute(AttributeName = "serviceId")]
        public string ServiceId { get; set; }
        [XmlAttribute(AttributeName = "serviceName")]
        public string ServiceName { get; set; }
        [XmlAttribute(AttributeName = "modeTransport")]
        public string ModeTransport { get; set; }
        [XmlAttribute(AttributeName = "transitDays")]
        public string TransitDays { get; set; }
        [XmlAttribute(AttributeName = "deliveryDate")]
        public string DeliveryDate { get; set; }
        [XmlAttribute(AttributeName = "currency")]
        public string Currency { get; set; }
        [XmlAttribute(AttributeName = "baseCharge")]
        public string BaseCharge { get; set; }
        [XmlAttribute(AttributeName = "totalTariff")]
        public string TotalTariff { get; set; }
        [XmlAttribute(AttributeName = "baseChargeTariff")]
        public string BaseChargeTariff { get; set; }
        [XmlAttribute(AttributeName = "fuelSurchargeTariff")]
        public string FuelSurchargeTariff { get; set; }
        [XmlAttribute(AttributeName = "fuelSurcharge")]
        public string FuelSurcharge { get; set; }
        [XmlAttribute(AttributeName = "totalCharge")]
        public string TotalCharge { get; set; }
    }

    [XmlRoot(ElementName = "ShippingReply", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class ShippingReply
    {
        [XmlElement(ElementName = "Order", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public Order Order { get; set; }
        [XmlElement(ElementName = "Carrier", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public Carrier Carrier { get; set; }
        [XmlElement(ElementName = "Package", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public List<Package> Package { get; set; }
        [XmlElement(ElementName = "TrackingURL", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public string TrackingURL { get; set; }
        [XmlElement(ElementName = "Pickup", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public Pickup Pickup { get; set; }
        [XmlElement(ElementName = "Labels", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public string Labels { get; set; }
        [XmlElement(ElementName = "LabelData", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public LabelData LabelData { get; set; }
        [XmlElement(ElementName = "Quote", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public ShippingQuote Quote { get; set; }
    }

    [XmlRoot(ElementName = "EShipper", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
    public class eShipperShippingResponseData
    {
        [XmlElement(ElementName = "ShippingReply", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public ShippingReply ShippingReply { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }

        [XmlElement(ElementName = "ErrorReply", Namespace = "http://www.eshipper.net/xml/XMLSchema")]
        public ErrorReply ErrorReply { get; set; }
    }

}
