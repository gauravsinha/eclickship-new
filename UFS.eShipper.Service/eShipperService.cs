﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UFS.Common;
namespace UFS.eShipper.Service
{
    public class eShipperService
    {
        public Response.eShipperQuoteResponse GetQuote(Request.eShipperQuoteRequest quoteRequest)
        {
            try
            {
                config cnf = ConfigReader.Instance.LoadData();
                quoteRequest.Data.Username = cnf.eShipper.MainConfig.UserName;
                quoteRequest.Data.Password = cnf.eShipper.MainConfig.Password;
                quoteRequest.Data.Version = cnf.eShipper.Version;
                Response.eShipperQuoteResponse resp = new Response.eShipperQuoteResponse();
                resp.ResponseData = HttpDataExchange.GetWebServiceResult<Response.EShipper>(cnf.eShipper.MainConfig.URL, quoteRequest.Data);
                return resp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Response.eShipperShippingResponse ShipPackage(Request.eShipperShippingRequest shipRequest)
        {
            config cnf = ConfigReader.Instance.LoadData();
            shipRequest.Data.Username = cnf.eShipper.MainConfig.UserName;
            shipRequest.Data.Password = cnf.eShipper.MainConfig.Password;
            shipRequest.Data.Version = cnf.eShipper.Version;
            Response.eShipperShippingResponse response = new Response.eShipperShippingResponse();
            response.ResponseData = HttpDataExchange.GetWebServiceResult<Response.eShipperShippingResponseData>(cnf.eShipper.MainConfig.URL, shipRequest.Data);
            return response;
        }
    }

}
