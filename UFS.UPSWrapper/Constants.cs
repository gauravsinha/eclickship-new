﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.UPSWrapper
{
    public class Constants
    {
    }

    public static class ServiceCodes
    {
        public static Dictionary<string, string> GetServices()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            dic.Add("Next Day Air", "01");
            dic.Add("2nd Day Air", "02");
            dic.Add("Ground", "03");
            dic.Add("Express", "07");
            dic.Add("Expedited ", "08");
            dic.Add("UPS Standard ", "11");
            dic.Add("3 Day Select", "12");
            dic.Add("Next Day Air Saver", "13");
            dic.Add("Next Day Air Early AM", "14");
            dic.Add("Express Plus", "54");
            dic.Add("2nd Day Air A.M.", "59");
            dic.Add("UPS Saver", "65");
            dic.Add("First Class Mail", "M2");
            dic.Add("Priority Mail", "M3");
            dic.Add("Expedited MaiI Innovations", "M4");
            dic.Add("Priority Mail Innovations", "M5");
            dic.Add("Economy Mail Innovations ", "M6");
            dic.Add("UPS Today Standard", "  82 ");
            dic.Add("UPS Today Dedicated Courier", "83");
            dic.Add("UPS Today Intercity", "84");
            dic.Add("UPS Today Express", "85");
            dic.Add("UPS Today Express Saver", "86");
            return dic;
        }
    }
}
