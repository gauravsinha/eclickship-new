﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace UFS.UPSWrapper
{
    public sealed class XMLReader
    {
        private static volatile XMLReader _instance;
        private static object syncRoot = new object();
        private config _config;
        private XMLReader()
        {
            string path = "";

            // path = @"D:\Projects\Sanganan\EasyPost_2\EasyPost\EasyPost.Web.UI\Config\Config.xml";
            path = @"C:\UFS_Dev\Config\Config.xml";
            //FileStream fs = new FileStream(@"C:\Meeku\Projects\Sanganan\Projects\EasyPost_2\EasyPost\EasyPost.Web.UI\Config\Config.xml", FileMode.Open, FileAccess.Read);
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            // Config result;

            var serializer = new XmlSerializer(typeof(config));

            //using (var stream = new StringReader(data))
            using (var reader = XmlReader.Create(fs))
            {
                _config = (config)serializer.Deserialize(reader);
            }
        }
        public static XMLReader Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new XMLReader();
                        }
                    }
                }
                return _instance;
            }
        }

        public config LoadData()
        {
            return _config;
        }
    }
}
