﻿using UFS.Web.BusinesEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UFS.UPSWrapper.AVService;
using UFS.Common;
namespace UFS.UPSWrapper
{
    public class AddressVerificationProcessor
    {
        config _config;
        XAVService _client;
        private UPSSecurity _upsSec;
        public UPSSecurity UPSSecurity { set { _upsSec = value; } }
        public int Securi { get; set; }
        public AddressVerificationProcessor()
        {
            _config = ConfigReader.Instance.LoadData();
        }

        /// <summary>
        /// Verify Shipping Address
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public XAVResponse VerifyAddress(Address address)
        {
            XAVResponse res;
            XAVRequest request = new XAVRequest();
            request.RegionalRequestIndicator = "";
            RequestType req = new RequestType();
            req.RequestOption = new string[] { "1" };
            AddressKeyFormatType format = new AddressKeyFormatType();
           // format.AddressLine = new string[] { address.Street1, address.Street2 };
            //format.AttentionName = address.Company;
            //format.ConsigneeName = address.PartialName;
            format.PoliticalDivision2 = address.City;
            format.PoliticalDivision1 = address.State;
            format.PostcodePrimaryLow = address.Zip;
            format.CountryCode = address.Country;
            request.Request = req;
            request.AddressKeyFormat = format;
            _client = new XAVService();
            _client.Url = _config.UPS.MainUrls.XAVServiceURL;
            _client.UPSSecurityValue = _upsSec;
            try
            {
                res = _client.ProcessXAV(request);
            }
            catch (Exception ex)
            {
                res = new XAVResponse()
                {
                    Response = new ResponseType()
                    {
                        ResponseStatus = new CodeDescriptionType()
                        {
                            Code = "-1",
                            Description = ex.Message
                        }
                    }
                };
            }
            return res;
        }
    }
}
