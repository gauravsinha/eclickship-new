﻿using UFS.Web.BusinesEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UFS.UPSWrapper.UPSShip;

namespace UFS.UPSWrapper
{
    public class ShipmentRequestProcessor
    {
        public string ShipperNumber { get; set; }
        public string ShipperAccountNumber { get; set; }
        public ShipmentRequestProcessor()
        {

        }

        public ShipConfirmRequest CreateConfirmRequest(ShipmentDetail shipmentData, string packgeType)
        {
            ShipConfirmRequest req = new ShipConfirmRequest();
            RequestType request = new RequestType();
            String[] requestOption = { "nonvalidate" };
            request.RequestOption = requestOption;
            req.Request = request;
            ShipmentType shipment = new ShipmentType();
            shipment.Description = "UFS Packaging Service";
            ShipperType shipper = new ShipperType();
            shipper.ShipperNumber =this.ShipperNumber;
            PaymentInfoType paymentInfo = new PaymentInfoType();
            ShipmentChargeType shpmentCharge = new ShipmentChargeType();
            BillShipperType billShipper = new BillShipperType();
            billShipper.AccountNumber = this.ShipperAccountNumber;
            shpmentCharge.BillShipper = billShipper;
            shpmentCharge.Type = "01";
            ShipmentChargeType[] shpmentChargeArray = { shpmentCharge };
            paymentInfo.ShipmentCharge = shpmentChargeArray;
            shipment.PaymentInformation = paymentInfo;
            shipper.Address = GetAddress(shipmentData.FromAddress);

            if (!string.IsNullOrWhiteSpace(shipmentData.FromAddress.Name))
                shipper.Name = shipmentData.FromAddress.Name;

            //if (!string.IsNullOrWhiteSpace(shipmentData.FromAddress.Company))
           //     shipper.AttentionName = shipmentData.FromAddress.Company;

            if (IsValidString(shipmentData.FromAddress.Phone))
            {
                ShipPhoneType shipperPhone = new ShipPhoneType();
                shipperPhone.Number = shipmentData.FromAddress.Phone;
                shipper.Phone = shipperPhone;
            }
            shipment.Shipper = shipper;
            ShipFromType shipFrom = new ShipFromType();
            shipFrom.Address = GetAddress(shipmentData.FromAddress);
          //  if (!string.IsNullOrWhiteSpace(shipmentData.FromAddress.Company))
          //      shipFrom.AttentionName = shipmentData.FromAddress.Company;
            if (!string.IsNullOrWhiteSpace(shipmentData.FromAddress.Name))
                shipFrom.Name = shipmentData.FromAddress.Name;
            shipment.ShipFrom = shipFrom;
            ShipToType shipTo = new ShipToType();

            shipTo.Address = GetToAddress(shipmentData.ToAddress);
            if (shipmentData.ToAddress.IsResidential)
            {
                shipTo.Address.ResidentialAddressIndicator = "";
            }
            if (!string.IsNullOrWhiteSpace(shipmentData.FromAddress.Name))
                shipTo.Name = shipmentData.ToAddress.Name;

          //  if (!string.IsNullOrWhiteSpace(shipmentData.FromAddress.Company))
          //      shipTo.AttentionName = shipmentData.ToAddress.Company;

            if (IsValidString(shipmentData.ToAddress.Phone))
            {
                ShipPhoneType shipToPhone = new ShipPhoneType();
                shipToPhone.Number = shipmentData.ToAddress.Phone;
                shipTo.Phone = shipToPhone;
            }
           
            shipment.ShipTo = shipTo;
            ServiceType service = new ServiceType();
            service.Code = packgeType;
            shipment.Service = service;
            ShipmentTypeShipmentServiceOptions optinsType = new ShipmentTypeShipmentServiceOptions();
            //if (shipmentData.Parcel.IsCOD)
            //{
            //    CODType cod = new CODType();
            //    cod.CODAmount = new CurrencyMonetaryType()
            //    {
            //        CurrencyCode = "USD",
            //        MonetaryValue = shipmentData.Parcel.CODAmount
            //    };
            //    cod.CODFundsCode = "9";
            //    optinsType.COD = cod;
            //}
            if (shipmentData.Parcel != null && shipmentData.Parcel.UFSOption != null)
            {
                if (shipmentData.Parcel.UFSOption.RecieveDeliveryConf)
                {
                    DeliveryConfirmationType conf = new DeliveryConfirmationType();
                    //conf.DCISNumber = "26464646";
                    conf.DCISType = GetType(shipmentData.Parcel.Signature);
                    optinsType.DeliveryConfirmation = conf;
                }
                if (shipmentData.Parcel.HoldForPickup == "true")
                {
                    optinsType.HoldForPickupIndicator = "";
                }
                if (shipmentData.Parcel.UFSOption.UFSCarbonNuetral)
                {
                    optinsType.UPScarbonneutralIndicator = "";
                }
                if (shipmentData.Parcel.UFSOption.SendEmailNotification)
                {
                    List<NotificationType> notification = new List<NotificationType>();
                    notification.Add(new NotificationType() { NotificationCode = "6", EMail = new EmailDetailsType() { SubjectCode = "01",EMailAddress = shipmentData.EmailAddress, FromEMailAddress = "newshipmentnotification@gmail.com", FromName = "UNITED FREIGHT SAVERS" } });
                    //  notification.Add(new NotificationType() { NotificationCode = "6", EMail = new EmailDetailsType() { SubjectCode = "01", EMailAddress = shipmentData.EmailAddress, FromEMailAddress = "newshipmentnotification@gmail.com", FromName = "UNITED FREIGHT SAVERS" } });
                    // notification.Add(new NotificationType() { NotificationCode = "7", EMail = new EmailDetailsType() { SubjectCode = "01", EMailAddress = shipmentData.EmailAddress, FromEMailAddress = "newshipmentnotification@gmail.com", FromName = "UNITED FREIGHT SAVERS" } });
                    //notification.Add(new NotificationType() { NotificationCode = "8", EMail = new EmailDetailsType() { SubjectCode = "01", EMailAddress = shipmentData.EmailAddress, FromEMailAddress = "newshipmentnotification@gmail.com", FromName = "UNITED FREIGHT SAVERS" } });
                    // notification.Add(new NotificationType() { NotificationCode = "2", EMail = new EmailDetailsType() { SubjectCode = "01", EMailAddress = shipmentData.EmailAddress, FromEMailAddress = "newshipmentnotification@gmail.com", FromName = "UNITED FREIGHT SAVERS" } });
                    optinsType.Notification = notification.ToArray();
                }
                if (shipmentData.Parcel.UFSOption.DeliverOnSunday)
                {
                    optinsType.SaturdayDeliveryIndicator = "";
                }

                if (optinsType != null)
                {
                    shipment.ShipmentServiceOptions = optinsType;
                }
            }
            PackageType[] pkgArray = GetPackage(shipmentData);
            shipment.Package = pkgArray;
            LabelSpecificationType labelSpec = new LabelSpecificationType();
            LabelStockSizeType labelStockSize = new LabelStockSizeType();
            labelStockSize.Height = "6";
            labelStockSize.Width = "4";
            labelSpec.LabelStockSize = labelStockSize;
            LabelImageFormatType labelImageFormat = new LabelImageFormatType();
            labelImageFormat.Code = "GIF";
            labelSpec.LabelImageFormat = labelImageFormat;
            labelSpec.HTTPUserAgent = "Mozilla/5.0";
            req.LabelSpecification = labelSpec;
            shipment.ShipmentRatingOptions = new RateInfoType() { NegotiatedRatesIndicator = "" };
            req.Shipment = shipment;
         /*   RateInfoType rateinfo = new RateInfoType();
            rateinfo.NegotiatedRatesIndicator = "";
            shipment.ShipmentRatingOptions = rateinfo;
            */
            // System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
            return req;

        }

        private string GetType(string signature)
        {
            if (signature == "NO_SIGNATURE")
                return "1";
            if (signature == "SIGNATURE")
                return "2";
            if (signature == "ADULT_SIGNATURE")
                return "3";
            return "1";
        }

        public ShipAcceptRequest CreateAcceptRequest(string shipmentDigest)
        {
            ShipAcceptRequest shipmentRequest = new ShipAcceptRequest();
            RequestType request = new RequestType();
            String[] requestOption = { "nonvalidate" };
            request.RequestOption = requestOption;
            shipmentRequest.Request = request;
            shipmentRequest.ShipmentDigest = shipmentDigest;
            return shipmentRequest;
        }

        private PackageType[] GetPackage(ShipmentDetail shp)
        {
            List<ParcelDetails> list = shp.Parcels;
            List<PackageType> packageList = new List<PackageType>();
            PackageType package;
            ShipUnitOfMeasurementType uom = new ShipUnitOfMeasurementType();
            PackageWeightType packageWeight;
            uom.Code = "LBS";
            foreach (var item in list)
            {
                package = new PackageType();
                packageWeight = new PackageWeightType();
                packageWeight.Weight = item.Weight;
                packageWeight.UnitOfMeasurement = uom;
                package.PackageWeight = packageWeight;
                
                if (item.PredefinedParcel != "Custom")
                {
                    PackagingType packType = new PackagingType();
                    packType.Code = item.PredefinedParcel;
                    package.Packaging = packType;
                }
                else
                {
                    PackagingType packType = new PackagingType();
                    packType.Code = "02"; // Customer Defined
                    package.Packaging = packType;

                    if (IsValidString(item.Height) && IsValidString(item.Width) && IsValidString(item.Length))
                    {
                        package.Dimensions = new DimensionsType();
                        package.Dimensions.UnitOfMeasurement = new ShipUnitOfMeasurementType()
                        {
                            Code = "IN",
                            Description = "Inch"
                        };
                        if (IsValidString(item.Height))
                            package.Dimensions.Height = item.Height;
                        if (IsValidString(item.Length))
                            package.Dimensions.Length = item.Length;
                        if (IsValidString(item.Width))
                            package.Dimensions.Width = item.Width;
                    }


                }
                PackageServiceOptionsType serviceOptions = null;

                if (IsValidString(item.DeclaredValue))
                {
                    serviceOptions = new PackageServiceOptionsType();
                    PackageDeclaredValueType decValues = new PackageDeclaredValueType();
                    decValues.CurrencyCode = "USD";
                    DeclaredValueType valType = new DeclaredValueType();
                    valType.Code = "01";
                    valType.Description = "EVS";
                    decValues.MonetaryValue = item.DeclaredValue;
                    decValues.Type = valType;
                    serviceOptions.DeclaredValue = decValues;
                }
                //else
                //{
                //    valType.Code = "01";
                //    valType.Description = "EVS";
                //}

                if (item.IsCOD)
                {
                    if (serviceOptions == null)
                        serviceOptions = new PackageServiceOptionsType();

                    serviceOptions.COD = new PSOCODType()
                    {
                        CODAmount = new CurrencyMonetaryType()
                        {
                            CurrencyCode = "USD",
                            MonetaryValue = item.CODAmount
                        },
                        CODFundsCode = "0"
                    };
                }
                if (item.Signature != "NO_SIGNATURE")
                {
                    if (serviceOptions == null)
                        serviceOptions = new PackageServiceOptionsType();

                    serviceOptions.DeliveryConfirmation = new DeliveryConfirmationType()
                    {
                        DCISType = item.Signature == "SIGNATURE" ? "1" : "2"
                    };
                }
                package.PackageServiceOptions = serviceOptions;
                packageList.Add(package);
            }
            return packageList.ToArray();
        }

        private static bool IsValidString(string strVal)
        {
            return !string.IsNullOrWhiteSpace(strVal) && strVal != "0";
        }
        private static PackageType[] GetPackage()
        {
            PackageType package = new PackageType();

            PackagingType packType = new PackagingType();
            packType.Code = "02";
            package.Packaging = packType;
            return null;
        }

        private static ShipToAddressType GetToAddress(Address address)
        {
            ShipToAddressType shipperAddress = new ShipToAddressType();
            String[] addressLine = { address.Street1, address.Street2 };
            shipperAddress.AddressLine = addressLine;
            shipperAddress.City = address.City;
            shipperAddress.PostalCode = address.Zip;
            shipperAddress.StateProvinceCode = address.State;
            shipperAddress.CountryCode = "US";
            shipperAddress.AddressLine = addressLine;
            return shipperAddress;
        }
        private static ShipAddressType GetAddress(Address address)
        {
            ShipAddressType shipperAddress = new ShipAddressType();
            String[] addressLine = { address.Street1, address.Street2 };
            shipperAddress.AddressLine = addressLine;
            shipperAddress.City = address.City;
            shipperAddress.PostalCode = address.Zip;
            shipperAddress.StateProvinceCode = address.State;
            shipperAddress.CountryCode = "US";
            shipperAddress.AddressLine = addressLine;
            return shipperAddress;
        }
        public ShipmentRequest CreateRequest(ShipmentDetail shipmentData)
        {

            ShipmentRequest shipmentRequest = new ShipmentRequest();
            RequestType request = new RequestType();
            String[] requestOption = { "nonvalidate" };
            request.RequestOption = requestOption;
            shipmentRequest.Request = request;
            ShipmentType shipment = new ShipmentType();
            shipment.Description = "Ship webservice example";
            ShipperType shipper = new ShipperType();
            shipper.ShipperNumber = "YW7594";
            PaymentInfoType paymentInfo = new PaymentInfoType();
            ShipmentChargeType shpmentCharge = new ShipmentChargeType();
            BillShipperType billShipper = new BillShipperType();
            billShipper.AccountNumber = "YW7594";
            shpmentCharge.BillShipper = billShipper;
            shpmentCharge.Type = "01";
            ShipmentChargeType[] shpmentChargeArray = { shpmentCharge };
            paymentInfo.ShipmentCharge = shpmentChargeArray;
            shipment.PaymentInformation = paymentInfo;
            ShipAddressType shipperAddress = new ShipAddressType();
            String[] addressLine = { "480 Parkton Plaza" };
            shipperAddress.AddressLine = addressLine;
            shipperAddress.City = "Timonium";
            shipperAddress.PostalCode = "21093";
            shipperAddress.StateProvinceCode = "MD";
            shipperAddress.CountryCode = "US";
            shipperAddress.AddressLine = addressLine;
            shipper.Address = shipperAddress;
            shipper.Name = "ABC Associates";
           // shipper.AttentionName = "ABC Associates";
            ShipPhoneType shipperPhone = new ShipPhoneType();
            shipperPhone.Number = "1234567890";
            shipper.Phone = shipperPhone;
            shipment.Shipper = shipper;
            ShipFromType shipFrom = new ShipFromType();
            ShipAddressType shipFromAddress = new ShipAddressType();
            String[] shipFromAddressLine = { "Ship From Street" };
            shipFromAddress.AddressLine = addressLine;
            shipFromAddress.City = "Timonium";
            shipFromAddress.PostalCode = "21093";
            shipFromAddress.StateProvinceCode = "MD";
            shipFromAddress.CountryCode = "US";
            shipFrom.Address = shipFromAddress;
           // shipFrom.AttentionName = "Mr.ABC";
            shipFrom.Name = "ABC Associates";
            shipment.ShipFrom = shipFrom;
            ShipToType shipTo = new ShipToType();
            ShipToAddressType shipToAddress = new ShipToAddressType();
            String[] addressLine1 = { "Some Street" };
            shipToAddress.AddressLine = addressLine1;
            shipToAddress.City = "Roswell";
            shipToAddress.PostalCode = "30076";
            shipToAddress.StateProvinceCode = "GA";
            shipToAddress.CountryCode = "US";
            shipTo.Address = shipToAddress;
          //  shipTo.AttentionName = "DEF";
            shipTo.Name = "DEF Associates";
            ShipPhoneType shipToPhone = new ShipPhoneType();
            shipToPhone.Number = "1234567890";
            shipTo.Phone = shipToPhone;
            shipment.ShipTo = shipTo;
            ServiceType service = new ServiceType();
            service.Code = "01";
            shipment.Service = service;
            PackageType package = new PackageType();
            PackageWeightType packageWeight = new PackageWeightType();
            packageWeight.Weight = "10";
            ShipUnitOfMeasurementType uom = new ShipUnitOfMeasurementType();
            uom.Code = "LBS";
            packageWeight.UnitOfMeasurement = uom;
            package.PackageWeight = packageWeight;
            PackagingType packType = new PackagingType();
            packType.Code = "02";
            package.Packaging = packType;
            PackageType[] pkgArray = { package };
            shipment.Package = pkgArray;
            LabelSpecificationType labelSpec = new LabelSpecificationType();
            LabelStockSizeType labelStockSize = new LabelStockSizeType();
            labelStockSize.Height = "6";
            labelStockSize.Width = "4";
            labelSpec.LabelStockSize = labelStockSize;
            LabelImageFormatType labelImageFormat = new LabelImageFormatType();
            labelImageFormat.Code = "SPL";
            labelSpec.LabelImageFormat = labelImageFormat;
            shipmentRequest.LabelSpecification = labelSpec;
            shipmentRequest.Shipment = shipment;
            Console.WriteLine(shipmentRequest);
            System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
            return shipmentRequest;
        }

        public UFS.UPSWrapper.RateService.RateRequest CreateRateRequest(ShipmentDetail shp, string serviceCode)
        {
            UFS.UPSWrapper.RateService.RateRequest rateRequest = new UFS.UPSWrapper.RateService.RateRequest();
            
            UFS.UPSWrapper.RateService.RequestType request = new UFS.UPSWrapper.RateService.RequestType();
            String[] requestOption = { "Rate" };
            request.RequestOption = requestOption;
            rateRequest.Request = request;
            UFS.UPSWrapper.RateService.ShipmentType shipment = new UFS.UPSWrapper.RateService.ShipmentType();
            UFS.UPSWrapper.RateService.ShipperType shipper = new UFS.UPSWrapper.RateService.ShipperType();
            shipper.ShipperNumber = "X2W424";
            UFS.UPSWrapper.RateService.AddressType shipperAddress = new UFS.UPSWrapper.RateService.AddressType();
            String[] addressLine = { "5555 main", "4 Case Cour", "Apt 3B" };
            shipperAddress.AddressLine = addressLine;
            shipperAddress.City = "Roswell";
            shipperAddress.PostalCode = "30076";
            shipperAddress.StateProvinceCode = "GA";
            shipperAddress.CountryCode = "US";
            shipperAddress.AddressLine = addressLine;
            shipper.Address = shipperAddress;
            shipment.Shipper = shipper;
            UFS.UPSWrapper.RateService.ShipFromType shipFrom = new UFS.UPSWrapper.RateService.ShipFromType();
            UFS.UPSWrapper.RateService.AddressType shipFromAddress = new UFS.UPSWrapper.RateService.AddressType();
            shipFromAddress.AddressLine = new string[] { shp.FromAddress.Company, shp.FromAddress.Street1,shp.FromAddress.Street2 };
            shipFromAddress.City = shp.FromAddress.City;
            shipFromAddress.PostalCode = shp.FromAddress.Zip;
            shipFromAddress.StateProvinceCode = shp.FromAddress.State;
            shipFromAddress.CountryCode = shp.FromAddress.Country;
            shipFrom.Address = shipFromAddress;
            shipment.ShipFrom = shipFrom;
            UFS.UPSWrapper.RateService.ShipToType shipTo = new UFS.UPSWrapper.RateService.ShipToType();
            UFS.UPSWrapper.RateService.ShipToAddressType shipToAddress = new UFS.UPSWrapper.RateService.ShipToAddressType();
            String[] addressLine1 = { shp.ToAddress.Company, shp.ToAddress.Street1, shp.ToAddress.Street2 };
            shipToAddress.AddressLine = addressLine1;
            shipToAddress.City = shp.ToAddress.City;
            shipToAddress.PostalCode = shp.ToAddress.Zip;
            shipToAddress.StateProvinceCode = shp.ToAddress.State;
            shipToAddress.CountryCode = "US";
            shipTo.Address = shipToAddress;
            shipment.ShipTo = shipTo;
            UFS.UPSWrapper.RateService.CodeDescriptionType service = new UFS.UPSWrapper.RateService.CodeDescriptionType();

            //Below code uses dummy date for reference. Please udpate as required.
            service.Code = serviceCode;
            shipment.Service = service;
            UFS.UPSWrapper.RateService.PackageType package = new UFS.UPSWrapper.RateService.PackageType();
            UFS.UPSWrapper.RateService.PackageWeightType packageWeight = new UFS.UPSWrapper.RateService.PackageWeightType();
            packageWeight.Weight =shp.Parcel.Weight;
            UFS.UPSWrapper.RateService.CodeDescriptionType uom = new UFS.UPSWrapper.RateService.CodeDescriptionType();
            uom.Code = "LBS";
            uom.Description = "pounds";
            packageWeight.UnitOfMeasurement = uom;
            package.PackageWeight = packageWeight;
            UFS.UPSWrapper.RateService.CodeDescriptionType packType = new UFS.UPSWrapper.RateService.CodeDescriptionType();
            packType.Code = "02";
            package.PackagingType = packType;
            package.Dimensions = new RateService.DimensionsType()
            {
                Height = shp.Parcel.Height,
                Length = shp.Parcel.Length,
                Width = shp.Parcel.Width,
                UnitOfMeasurement = new RateService.CodeDescriptionType()
                {
                    Code = "IN",
                    Description = "Inch"
                }
            };
            UFS.UPSWrapper.RateService.ShipmentRatingOptionsType shpOpsType = new RateService.ShipmentRatingOptionsType();
            shpOpsType.NegotiatedRatesIndicator = "";
            shipment.ShipmentRatingOptions = shpOpsType;
            UFS.UPSWrapper.RateService.PackageType[] pkgArray = { package };
            shipment.Package = pkgArray;
            rateRequest.Shipment = shipment;
            System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
            return rateRequest;
        }
    }
}
