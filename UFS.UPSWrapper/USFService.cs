﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UFS.Web.BusinesEntities;
using UFS.UPSWrapper.UPSShip;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.IO;
using UFS.Common;
namespace UFS.UPSWrapper
{
    public class USFService
    {
        static config _config;

        public USFService()
        {
            _config = ConfigReader.Instance.LoadData();
        }

        public List<uint> IgnoreErrorCode
        {
            get
            {
                //List<string> val = new List<string>();
                //val.Add("121210");
                //val.Add("121100");
                //val.Add("121261");
                //val.Add("120076");
                //val.Add("121500");
                //val.Add("120201");
                //val.Add("9120203");
                //val.Add("120124");
                //val.Add("120602");
                //val.Add("120600");
                //val.Add("121375");
                //val.Add("121211");
                //val.Add("12104");
                //return val;
                if(_config == null)
                    _config = ConfigReader.Instance.LoadData();

                return _config.UPS.SkipErrorCodes.ToList();
            }
        }
        public ShipmentDetail Shipment { get; set; }
        public string ShipmentDigest { get; set; }
        public List<string> Images { get; set; }
        public string Image { get; set; }
        public List<UFSRate> Rates { get; set; }
        public List<string> TrackingNumbers { get; set; }
        public Dictionary<string, string> ShippingLabel { get; set; }
        public Dictionary<string, object> Messages { get; set; }
        private Dictionary<string, string> tempMessages;
        public string UniqueObjectCode
        {
            get
            {

                return DateTime.Now.Year.ToString() + "" + DateTime.Now.Month.ToString() + "" + DateTime.Now.Day.ToString() + "" + DateTime.Now.Hour.ToString() + "" + DateTime.Now.Minute.ToString() + "" + DateTime.Now.Second.ToString() + "" + DateTime.Now.Millisecond.ToString();
            }
        }
        public void Create()
        {
            try
            {
                bool isValidAddres = true;
                isValidAddres = VerifyUPSAddress(isValidAddres);
                if (isValidAddres)
                {
                    ShipService shpSvc = new ShipService();
                    shpSvc.Url = _config.UPS.MainUrls.ShipServiceURL;
                    UPSSecurity upss = SetSecurityParams();
                    shpSvc.UPSSecurityValue = upss;

                    if (_config == null)
                        _config = ConfigReader.Instance.LoadData();
                    ShipConfirmRequest rq;
                    ShipmentRequestProcessor shp = new ShipmentRequestProcessor();
                    shp.ShipperNumber = _config.UPS.UPSKeys.ShipperNumber;
                    shp.ShipperAccountNumber = _config.UPS.UPSKeys.ShipperAccountNumber;
                    ShipConfirmResponse rs = null;
                    Rates = new List<UFSRate>();
                    string errorCode = "";
                    string errorDesc = "";
                    int i = 0;
                    foreach (var item in ServiceCodes.GetServices())
                    {
                        rq = shp.CreateConfirmRequest(Shipment, item.Value);
                        // LogWriter.SerializeObject<ShipConfirmRequest>(rq);
                        // LogWriter.SerializeObject<UPSSecurity>(upss);
                        try
                        {
                            rs = shpSvc.ProcessShipConfirm(rq);
                            if (rs != null && rs.Response != null && rs.Response.ResponseStatus.Code == "1")
                            {
                                Rates.Add(new UFSRate()
                                {
                                    ServiceId = item.Value,
                                    ServiceDesc = item.Key,
                                    Currency = rs.ShipmentResults.ShipmentCharges.TotalCharges.CurrencyCode,
                                    Amount = (rs.ShipmentResults.NegotiatedRateCharges!=null && rs.ShipmentResults.NegotiatedRateCharges.TotalCharge != null)
                                                ? rs.ShipmentResults.NegotiatedRateCharges.TotalCharge.MonetaryValue 
                                                : rs.ShipmentResults.ShipmentCharges.TotalCharges.MonetaryValue,
                                    RateId = rs.ShipmentResults.ShipmentDigest,
                                    DeliveryDate = DateTime.MinValue.ToString()
                                });
                            }
                        }
                        catch (SoapException ex)
                        {
                            errorCode = ex.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerText;
                            errorDesc = ex.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[1].InnerText;
                            if (tempMessages == null)
                                tempMessages = new Dictionary<string, string>();
                            if (!tempMessages.ContainsKey(errorCode))
                            {
                                tempMessages.Add(errorCode, errorDesc);
                            }

                            if (!IgnoreErrorCode.Contains(Convert.ToUInt32(errorCode)))
                            {
                                if (Messages == null)
                                {
                                    Messages = new Dictionary<string, object>();
                                }
                                if (!Messages.ContainsKey(errorCode))
                                {
                                    Messages.Add(errorCode, errorDesc);
                                }
                                i++;
                            }
                        }

                    }
                }

            }
            catch (SoapException sopEx)
            {
                // if (Messages == null)
                //{
                Messages = new Dictionary<string, object>();
                //}
                Messages.Add(sopEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerText, sopEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[1].InnerText);

            }
            finally
            {
                if (this.Rates != null && Rates.Count <= 0 && (Messages == null || Messages.Count <= 0) && tempMessages != null && tempMessages.Count > 0)
                {
                    Messages = new Dictionary<string, object>();
                    foreach (var item in tempMessages)
                    {
                        if (!Messages.ContainsKey(item.Key))
                            Messages.Add(item.Key, item.Value);
                    }
                }
            }
        }

        private bool VerifyUPSAddress(bool isValidAddres)
        {
            if (Shipment.Carrier == "UPS" && Shipment.Parcel.UFSOption.VefiryAddress)
            {
                UFS.UPSWrapper.AVService.XAVResponse resp = null;
                AddressVerificationProcessor pro = new AddressVerificationProcessor();
                pro.UPSSecurity = GetAVSecurity();
                resp = pro.VerifyAddress(Shipment.FromAddress);
                if (resp != null && resp.Response.ResponseStatus.Code == "1")
                    isValidAddres = true;
                else
                {
                    isValidAddres = false;
                    Messages = new Dictionary<string, object>();
                    Messages.Add("-1", "Address Verification Error : " + resp.Response.ResponseStatus.Description);
                }
                if (isValidAddres)
                {
                    resp = pro.VerifyAddress(Shipment.ToAddress);
                    isValidAddres = resp != null && resp.Response.ResponseStatus.Code == "1";
                    if (!isValidAddres)
                    {
                        if (Messages == null)
                            Messages = new Dictionary<string, object>();
                        if (!Messages.ContainsKey(resp.Response.ResponseStatus.Code))
                            Messages.Add(resp.Response.ResponseStatus.Code, "Address Verification Error : " + resp.Response.ResponseStatus.Description);
                    }
                }
                else
                {
                    isValidAddres = false;
                    if (Messages == null)
                        Messages = new Dictionary<string, object>();
                    if (!Messages.ContainsKey("-1"))
                        Messages.Add("-1", "Address Verification Error : " + resp.Response.ResponseStatus.Description);
                }
            }
            return isValidAddres;
        }
        public void Buy()
        {
            try
            {
                ShipService shpSvc = new ShipService();
                shpSvc.Url = _config.UPS.MainUrls.ShipServiceURL;
                UPSSecurity upss = SetSecurityParams();
                shpSvc.UPSSecurityValue = upss;
                ShipAcceptRequest rq = new ShipmentRequestProcessor().CreateAcceptRequest(ShipmentDigest);
                ShipAcceptResponse rs = shpSvc.ProcessShipAccept(rq);
                string imageURL = "";
                if (rs != null && rs.Response != null && rs.Response.ResponseStatus.Code == "1")
                {
                    Images = new List<string>();
                    TrackingNumbers = new List<string>();
                    ShippingLabel = new Dictionary<string, string>();
                    foreach (var item in rs.ShipmentResults.PackageResults)
                    {
                        TrackingNumbers.Add(item.TrackingNumber);
                        string htmlImage = WriteHTML(item.ShippingLabel.HTMLImage, item.TrackingNumber);
                        imageURL = WriteImage(item.ShippingLabel.GraphicImage, item.TrackingNumber);
                        if (!string.IsNullOrWhiteSpace(imageURL))
                        {
                            Images.Add(imageURL);
                            ShippingLabel.Add(item.TrackingNumber, imageURL);
                        }
                    }
                }
            }
            catch (SoapException ex)
            {
                if (Messages == null)
                {
                    Messages = new Dictionary<string, object>();
                }
                Messages.Add("BuyError", ex.Message);
            }
        }

        private string WriteHTML(string hTMLImage, string trackingNumber)
        {
            string path = "~/Labels/" + trackingNumber + ".html";
            string htmlString = Base64Decode(hTMLImage);
            byte[] bytes = Encoding.ASCII.GetBytes(htmlString);
            CreateFile(path, bytes);
            return path;

        }

        private string WriteImage(string base64ImageString, string trackingNumber)
        {
            string path = "~/Labels/label" + trackingNumber + ".gif";
            try
            {
                byte[] bytes = Convert.FromBase64String(base64ImageString);
                CreateFile(path, bytes);
            }
            catch (Exception ex)
            {
                path = "";
            }
            return path;
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        private static void CreateFile(string path, byte[] bytes)
        {
            if (File.Exists(path))
                File.Delete(path);
            var fs = new BinaryWriter(new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(path), FileMode.CreateNew, FileAccess.Write));
            fs.Write(bytes);
            fs.Close();
        }

        private static UPSSecurity SetSecurityParams()
        {
           // config config = ConfigReader.Instance.LoadData();
            if (_config == null)
                _config = ConfigReader.Instance.LoadData();

            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = _config.UPS.UPSKeys.AccessLicenseNumber;
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = _config.UPS.UPSKeys.UserId;
            upssUsrNameToken.Password = _config.UPS.UPSKeys.Password;
            upss.UsernameToken = upssUsrNameToken;            
            return upss;
        }
        private static AVService.UPSSecurity GetAVSecurity()
        {
           // config config = ConfigReader.Instance.LoadData();
            if (_config == null)
                _config = ConfigReader.Instance.LoadData();
            AVService.UPSSecurity upss = new AVService.UPSSecurity();
            AVService.UPSSecurityServiceAccessToken upssSvcAccessToken = new AVService.UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = _config.UPS.UPSKeys.AccessLicenseNumber;
            upss.ServiceAccessToken = upssSvcAccessToken;
            AVService.UPSSecurityUsernameToken upssUsrNameToken = new AVService.UPSSecurityUsernameToken();
            upssUsrNameToken.Username = _config.UPS.UPSKeys.UserId;
            upssUsrNameToken.Password = _config.UPS.UPSKeys.Password;
            upss.UsernameToken = upssUsrNameToken;
            return upss;
        }
        private static RateService.UPSSecurity SetRateSecurityParams()
        {
           // config config = ConfigReader.Instance.LoadData();
            if (_config == null)
                _config = ConfigReader.Instance.LoadData();
            RateService.UPSSecurity upss = new RateService.UPSSecurity();
            RateService.UPSSecurityServiceAccessToken upssSvcAccessToken = new RateService.UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = _config.UPS.UPSKeys.AccessLicenseNumber;
            upss.ServiceAccessToken = upssSvcAccessToken;
            RateService.UPSSecurityUsernameToken upssUsrNameToken = new RateService.UPSSecurityUsernameToken();
            upssUsrNameToken.Username = _config.UPS.UPSKeys.UserId;
            upssUsrNameToken.Password = _config.UPS.UPSKeys.Password;
            upss.UsernameToken = upssUsrNameToken;
            return upss;
        }

        public void CreateNegotiatedRate()
        {
            try
            {
                bool isValidAddres = true;
                isValidAddres = VerifyUPSAddress(isValidAddres);
                if (isValidAddres)
                {
                    RateService.RateService shpSvc = new RateService.RateService();
                    shpSvc.Url = _config.UPS.MainUrls.RateServiceURL;
                    RateService.UPSSecurity upss = SetRateSecurityParams();
                    shpSvc.UPSSecurityValue = upss;
                    RateService.RateRequest rq;
                    RateService.RateResponse rs;
                    List<UFSRate> rate = new List<UFSRate>();
                    if (_config == null)
                        _config = ConfigReader.Instance.LoadData();
                    ShipmentRequestProcessor shp = new ShipmentRequestProcessor();
                    shp.ShipperAccountNumber = _config.UPS.UPSKeys.ShipperAccountNumber;
                    shp.ShipperNumber = _config.UPS.UPSKeys.ShipperNumber;

                    if (_config == null)
                        _config = ConfigReader.Instance.LoadData();
                    Rates = new List<UFSRate>();
                    string errorCode = "";
                    string errorDesc = "";
                    int i = 0;
                    foreach (var item in ServiceCodes.GetServices())
                    {
                        rq = shp.CreateRateRequest(Shipment, item.Value);
                        try
                        {
                            rs = shpSvc.ProcessRate(rq);
                            if (rs != null && rs.Response != null && rs.Response.ResponseStatus.Code == "1")
                            {
                                Rates.Add(new UFSRate()
                                {
                                    ServiceId = item.Value,
                                    ServiceDesc = item.Key,
                                    Currency = rs.RatedShipment[0].NegotiatedRateCharges.TotalCharge.CurrencyCode,
                                    Amount = rs.RatedShipment[0].NegotiatedRateCharges.TotalCharge.MonetaryValue,
                                    RateId = rs.Response.TransactionReference.TransactionIdentifier,
                                    DeliveryDate = rs.RatedShipment[0].GuaranteedDelivery != null ? rs.RatedShipment[0].GuaranteedDelivery.DeliveryByTime : string.Empty
                                });
                            }
                        }
                        catch (SoapException ex)
                        {
                            errorCode = ex.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerText;
                            errorDesc = ex.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[1].InnerText;
                            if (tempMessages == null)
                                tempMessages = new Dictionary<string, string>();
                            if (!tempMessages.ContainsKey(errorCode))
                            {
                                tempMessages.Add(errorCode, errorDesc);
                            }

                            if (!IgnoreErrorCode.Contains(Convert.ToUInt32(errorCode)))
                            {
                                if (Messages == null)
                                {
                                    Messages = new Dictionary<string, object>();
                                }
                                if (!Messages.ContainsKey(errorCode))
                                {
                                    Messages.Add(errorCode, errorDesc);
                                }
                                i++;
                            }
                        }

                    }
                }

            }
            catch (SoapException sopEx)
            {
                // if (Messages == null)
                //{
                Messages = new Dictionary<string, object>();
                //}
                Messages.Add(sopEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerText, sopEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[1].InnerText);

            }
            finally
            {
                if (this.Rates != null && Rates.Count <= 0 && (Messages == null || Messages.Count <= 0) && tempMessages != null && tempMessages.Count > 0)
                {
                    Messages = new Dictionary<string, object>();
                    foreach (var item in tempMessages)
                    {
                        if (!Messages.ContainsKey(item.Key))
                            Messages.Add(item.Key, item.Value);
                    }
                }
            }
        }
        public List<RateService.RateResponse> GetRate()
        {
            List<RateService.RateResponse> lsrRes = new List<RateService.RateResponse>();
            RateService.RateService srv = new RateService.RateService();
            RateService.UPSSecurity upss = SetRateSecurityParams();
            srv.UPSSecurityValue = upss;
            RateService.RateRequest rq;
            RateService.RateResponse rs;
            List<UFSRate> rate = new List<UFSRate>();
            if (_config == null)
                _config = ConfigReader.Instance.LoadData();
            ShipmentRequestProcessor shp = new ShipmentRequestProcessor();
            shp.ShipperAccountNumber = _config.UPS.UPSKeys.ShipperAccountNumber;
            shp.ShipperNumber = _config.UPS.UPSKeys.ShipperNumber;
            foreach (var item in ServiceCodes.GetServices())
            {
                rq = shp.CreateRateRequest(Shipment, item.Value);
                rs = srv.ProcessRate(rq);
                if (rs != null && rs.Response != null && rs.Response.ResponseStatus.Code == "1")
                {
                    lsrRes.Add(rs);
                }
            }
            return lsrRes;
        }
    }
}
