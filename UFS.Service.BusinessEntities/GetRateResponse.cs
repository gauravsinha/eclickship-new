﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using EasyPost;
namespace UFS.Service.BusinessEntities
{
    [DataContract]
    public class GetRateResponse
    {
        [DataMember]
        public string Service { get; set; }

        [DataMember]
        public Rate Rate { get; set; }

        [DataMember]
        public string ExpDelivery { get; set; }

        public List<Message> Messages { get; set; }
    }
    
    [DataContract]
    public class Message
    {
        public int Code { get; set; }
        public string Description { get; set; }

    }
}
