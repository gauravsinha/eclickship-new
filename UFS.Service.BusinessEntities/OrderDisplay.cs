﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UFS.Service.BusinessEntities
{
    public class OrderDisplay
    {
        public long OrderId { get; set; }
        public string OrderNumber { get; set; }
        public List<string> OrderNumbers { get; set; }

        public List<string> FailedOrders { get; set; }
        public List<string> SuccessOrders { get; set; }
        public string OrderDate { get; set; }
        public string CustomerName { get; set; }

        public string CustomerAddress { get; set; }

        public string RecieverName { get; set; }

        public string RecieverAddress { get; set; }

        public string TrackingUrl { get; set; }

        public string OrderStatus { get; set; }

        public string ShippingService { get; set; }

        public string BatchNumber { get; set; }

        public string BatchStatus { get; set; }

        public string UserName { get; set; }

        public string ProcessedDate { get; set; }

        public int Count { get; set; }

        public int SuccessCount { get; set; }
        public int FailedCount { get; set; }
    }
}