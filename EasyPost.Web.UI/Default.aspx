﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Default.aspx.cs" Inherits="EasyPostDemo.Dashboard.Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <script src="../js/kendo.all.min.js"></script>
    <link href="../css/kendo.common.min.css" rel="stylesheet" />
    <link href="../css/kendo.metro.min.css" rel="stylesheet" />
    <div id="mainDiv">
        <div id="grid"></div>
    </div>
    <script type="text/javascript">
        var selectedButtonId = 0;


        $(document).ready(function () {
            // getData();
            var onClose = function () {
                mainGrid.data("kendoGrid").refresh();
            }
            // var dataService = getData();
            //  console.log(dataService);
            var selectedRows = [];
            mainGrid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Default.aspx/GetOrders",
                            dataType: "JSON"
                        }
                    },
                    schema: {
                        data: "d",
                        total: function (response) {
                            debugger;
                            return response.d.length;
                        },

                        model: {
                            fields: {
                                DateOrdered: { type: "date" },
                                Name: { type: "string" },
                                Address1: { type: "string" },
                                Address2: { type: "string" },
                                City: { type: "string" },
                                OrderStatus: { type: "string" }
                            }
                        }
                    },
                    pageSize: 20
                },
                sortable: true,
                selectable: "row",
                dataBound: function () {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                filterable: {
                    mode: "row"
                },
                pageable: {
                    refresh: true,
                    pageSize: 20,
                    numeric: true,
                    buttonCount: 20,
                    info: true
                },
                change: function (e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                        // console.log(dataItem.id);
                        selectedButtonId = dataItem.id;
                    }
                },
                columns: [
                     //{ field: "id", title: "Button Id", width: '10px', hidden: false },
                    { field: "DateOrdered", title: "Date", width: 100, hidden: false, format: "{0:MM/dd/yyyy}" },
                    //{ field: "OrderId", title: "Order Id", width: '100px' },
                      //{ field: "SiteOrderID", title: "Site Order Id", width: '100px' },
                        { field: "Name", title: "Customer Name", width: 180 },
                          { field: "Address1", title: "Address 1", width: 250 },
                            { field: "Address2", title: "Address 2", width: 230 },

    { field: "City", title: "City", width: 150 },

          {
              field: "OrderStatus",
              title: "Status",
              width: 90,
              filterabe: {
                  cell: {
                      operator: "contains"
                  }
              }


          },
                  { field: "SiteOrderID", title: "Ship", width: 90, template: "# if(OrderStatus=='Unshipped') {#<a id='toggle' href='../Packages/CreatePackage?siteId=#=SiteOrderID#' style='cursor:pointer'><img src='../Images/logo_nav.png' title='Ship this item'/></a>#} #" }
                ]
            });
        });

    </script>

    <script type="text/javascript">

        function toggleApproved(id, value) {
            console.log(id);
            callFunction = function (d) {
                $("#grid").data("kendoGrid").dataSource.read();
            };
            ajaxCall('order/generateLabel', 'id=' + id + "&val=" + value, callFunction);
        }


        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos');
        }

        function ajaxCall(url, data, customFunction) {
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: customFunction,
                error: function () {
                    alert(error);
                }
            });
        }

    </script>

</asp:Content>
