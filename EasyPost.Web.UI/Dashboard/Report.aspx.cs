﻿using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using UFS.Service.BusinessEntities;
using UFS.Web.Dashboard;

namespace EasyPostDemo.Dashboard
{
    public partial class Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx?");
                }
                else
                {
                    UserDetail usr = (UserDetail)Session["user"];
                    hdnrl.Value = (usr.RoleId == "1").ToString();
                }

            }
            catch (Exception)
            {

            }
        }

        [WebMethod]
        public static object GetReport()
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                var data = new MSSQLReportDataOperations().GetReportData(usr.UserId, usr.RoleId == "1");
                if (data != null && data.Count > 0)
                {
                    return data;
                }
                else

                    return data != null && data.Count > 0 ? data : new List<ReportData>();
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return null;
            }
        }

        [WebMethod]
        public static string GetLabels(string shipmentId)
        {
            try
            {
                string randString = UFS.Web.Common.CommonUtilities.GetRandomString(10);
                string images = new MSSQLReportDataOperations().GetLabels(shipmentId);
                if (!string.IsNullOrWhiteSpace(images))
                {
                    string[] arrLabels = images.Split(new char[] { ',' });
                    if (arrLabels.Length > 0)
                    {
                        HttpContext.Current.Session[randString] = arrLabels.ToList<string>();
                        return randString;
                    }
                    else
                    {
                        return "Unable to get labels at this time. Please try again in few moments or contatc system admin.";
                    }
                }
                else
                {
                    return "Unable to get labels at this time. Please try again in few moments or contatc system admin.";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}