﻿using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using UFS.Service.BusinessEntities;
using System.Drawing.Printing;
using System.Drawing;
using System.Diagnostics;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace EasyPostDemo.Dashboard
{
    public partial class PrintOrderLabel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx?");
                }
                else
                {
                    if (Request.QueryString != null && Request.QueryString["data"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["data"].ToString()))
                    {
                        string orderId = Request.QueryString["data"].ToString().Replace('_', '#');
                        GetOrderLabel(orderId);
                    }
                }
            }
            catch (Exception ex)
            {

                dvBody.InnerHtml = "<h3>Error occured . Contact System Admin. Error is " + ex.Message;
            }
        }

        public void GetOrderLabel(string orderId)
        {
            try
            {
                UserDetail usr = (UserDetail)Session["user"];
                string url = new MSSQLUserOperations().GetOrderLabel(orderId, usr.UserId);
                if (!string.IsNullOrWhiteSpace(url))
                {
                    string imgs = "";
                    string[] urls = url.Split(new char[] { ',' });
                    foreach (var item in urls)
                    {
                        imgs = imgs + "<img src='" + item + "'/><div class='break'></div>";
                    }
                    dvBody.InnerHtml = imgs;
                }
                else
                {
                    dvBody.InnerHtml = "<h3>Unable  to get the label. Please try again later. If issue persists please contact system administrator</h3>";
                }
            }
            catch (Exception ex)
            {
                dvBody.InnerHtml = "<h3>Error occured . Contact System Admin. Error is " + ex.Message;
            }
        }

        protected void printLabels_Click(object sender, EventArgs e)
        {
            try
            {
                iTextSharp.text.Rectangle size = new iTextSharp.text.Rectangle(288, 432);
                Document pdf = new Document(size,2,2,2,2);
                PdfWriter.GetInstance(pdf, Response.OutputStream);
                pdf.Open();
                string orderId = Request.QueryString["data"].ToString().Replace('_', '#');
                pdf.AddAuthor("UFS");
                string files = getImages();
                string[] fileName = files.Split(new char[] { ';' });
                foreach (var item in fileName)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        using (var imageStream = new FileStream(item, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageStream);
                            image.ScaleAbsoluteHeight(432);
                            image.ScaleAbsoluteWidth(288);
                            pdf.Add(image);
                        }
                    }
                }
               
               // pdf.Add(new Paragraph());
                pdf.Close();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;" +
                                               "filename=Label"+orderId+".pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdf);
                Response.End();
                
                //pdf.Add(new )
                /*
                //PrintDocument pd = new PrintDocument();
                //pd.PrintPage += PrintPage;
                //pd.Print();
                string files = getImages();
                string[] fileName = files.Split(new char[] { ';' });
                PrinterSettings settings = new PrinterSettings();
                // PrintFiles(settings.PrinterName, fileName);
                PrintPages(fileName);
                */
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void PrintPages(string[] fileName)
        {
            foreach (var item in fileName)
            {
                ProcessStartInfo psi = new ProcessStartInfo(item);
                if (!psi.Verbs.Contains("print"))
                {
                    psi.Verb = "print";
                    psi.UseShellExecute = true;
                    psi.WindowStyle = ProcessWindowStyle.Hidden;
                    psi.CreateNoWindow = true;
                    Process.Start(psi).WaitForExit();                   
                }
            }
        }

        public void PrintFiles(string printerName, params string[] fileNames)
        {
            var files = String.Join(" ", fileNames);
            var command = String.Format("/C print /D:{0} {1}", printerName, files);
            var process = new Process();
            var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "cmd.exe",
                Arguments = command,
                UseShellExecute = true,
                //Verb = "Print"
            };

            process.StartInfo = startInfo;
            process.Start();
        }

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            string filePath = getImages();
            if (!string.IsNullOrWhiteSpace(filePath))
            {
                string[] fileNames = filePath.Split(new char[] { ';' });
                //System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath(file));
                //Point loc = new Point(0, 0);
                //e.Graphics.DrawImage(img, loc);
                //Process p = new Process();
                //p.StartInfo.FileName = Server.MapPath(file);
                //p.StartInfo.Verb = "Print";
                //p.Start();
                PrinterSettings settings = new PrinterSettings();
                var files = String.Join(" ", fileNames);
                var command = String.Format("/C print /D: \"{0}\"  \"{1}\"", "\""+settings.PrinterName+"\"", files);
                var process = new Process();
                var startInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "cmd.exe",
                    Arguments = command
                };

                process.StartInfo = startInfo;
                process.Start();
            }
        }

        private string getImages()
        {
            string orderId = Request.QueryString["data"].ToString().Replace('_', '#');
            UserDetail usr = (UserDetail)Session["user"];
            string url = new MSSQLUserOperations().GetOrderLabel(orderId, usr.UserId);
            if (!string.IsNullOrWhiteSpace(url))
            {
                string imgs = "";
                string[] urls = url.Split(new char[] { ',' });
                foreach (var item in urls)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        imgs = imgs+Server.MapPath(item)+";";
                    }
                }
                return imgs.TrimEnd(';');
            }
            else
            {
                return string.Empty;
            }
        }
    }
}