﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Async="true" CodeBehind="Report.aspx.cs" Inherits="EasyPostDemo.Dashboard.Report" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <%--    <script src="http://cdn.kendostatic.com/2013.2.716/js/kendo.all.min.js"></script>--%>
    <script src="//kendo.cdn.telerik.com/2016.2.714/js/jquery.min.js"></script>
    <script src="//kendo.cdn.telerik.com/2016.2.714/js/jszip.min.js"></script>
    <script src="//kendo.cdn.telerik.com/2016.2.714/js/kendo.all.min.js"></script>
    <link href="../css/kendo.common.min.css" rel="stylesheet" />
    <link href="../css/kendo.metro.min.css" rel="stylesheet" />
    <style type="text/css">
        .k-grid .k-state-selected {
            background: white;
            color: black;
        }

        #grid .k-grid-toolbar {
            padding: .6em 1.3em;
        }

        #generateLabel {
            vertical-align: middle;
        }

        .toolbar {
            float: right;
        }

        .k-grid .k-grid-header .k-header .k-link {
            height: auto;
        }

        .k-grid .k-grid-header .k-header {
            white-space: normal;
        }
    </style>

    <div id="mainDiv" style="padding-top: 10px;">
        <%--  <h3><a href="Default.aspx">Order History</a>   |    <a href="Batch.aspx">Batch History</a>   |    Report</h3>--%>
        <div id="grid"></div>

    </div>
    <input type="hidden" runat="server" id="hdnrl" value="False" />
    <script type="text/javascript">
        var selectedButtonId = 0;
        $(document).ready(function () {
            var onClose = function () {
                mainGrid.data("kendoGrid").refresh();
            }
            var selectedRows = [];
            var mainGrid = $("#grid").kendoGrid({
                toolbar: ["excel", "pdf"],
                excel: {
                    fileName: "Order Report.xlsx",
                    filterable: true,
                    proxyURL: "Report.aspx/GetReport",
                    allPages: true
                },
                pdf: {
                    fileName: "Order Report.pdf",
                    allPages: true
                },
                dataSource: {
                    transport: {
                        read: {
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Report.aspx/GetReport",
                            dataType: "JSON"
                        }
                    },
                    schema: {
                        data: "d",
                        total: function (response) {
                            debugger;
                            return response.d.length;
                        },

                        model: {
                            fields: {
                                BatchNumber: { type: "string" },
                                Total: { type: "int" },
                                Failed: { type: "int" },
                                Succeeded: { type: "int" },
                                OverAllStatus: { type: "string" },
                                CreateDate: { type: "date" },
                                Processed: { type: "string" }
                            }
                        }
                    },
                    pageSize: 20
                },
                sortable: true,
                selectable: "row",
                filterable: true,
                pageable: {
                    refresh: true,
                    pageSize: 20,
                    numeric: true,
                    buttonCount: 20,
                    info: true
                },
                columns: [
                           { field: "BatchNumber", title: "Batch #", width: 50, hidden: "True" },
                           { field: "OrderId", title: "Order #", width: 30, filterable: false },
                           {
                               field: "CreateDate", title: "Report Date", width: 40, format: "{0:MM/dd/yyyy}", filterable: {
                                   ui: "datetimepicker"
                               }
                           },
                            // { field: "Price", title: "Org. Price", width: 40 },
                            //{ field: "ChargedPrice", title: "Price Charged", width: 40, hidden: $("#hdnrl").val() == "False" },
                            { field: "FromAddress", title: "Shipping Address", width: 150, filterable: false },
                            //{ field: "ToAddress", title: "Billing Address", width: 110 },
                            { field: "ShipmentId", title: "Tracking#", width: 50, filterable: false },
                              { field: "Service", title: "Service", width: 60, filterable: false },
                            { field: "Carrier", title: "Carrier ", width: 50, filterable: false,hidden:true },
                            { field: "LabelField", title: "View Label", width: 30, template: "<a  href='PrintOrderLabel.aspx?data=#=OrderId#' style='cursor:pointer' target='_blank'>View/Print</a>", filterable: false }
                            //{ field: "TrackingUrl", title: "Track", width: 90, template: "<a  href='#=TrackingUrl#' style='cursor:pointer' target='_blank'>View</a>" }
                ]
            });


        });


    </script>

    <div id="dialog-form" title="Label Batch Status">
    </div>
    <div id="dialog-form-1" title="Label Re-Generation">
        <div id="content">
        </div>
    </div>

</asp:Content>
