﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Async="true" CodeBehind="Default.aspx.cs" Inherits="EasyPostDemo.Dashboard.Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <script src="http://cdn.kendostatic.com/2013.2.716/js/kendo.all.min.js"></script>
    <link href="../css/kendo.common.min.css" rel="stylesheet" />
    <link href="../css/kendo.metro.min.css" rel="stylesheet" />
    <script src="../js/printMe.js"></script>
    <style type="text/css">
        .k-grid .k-state-selected {
            background: white;
            color: black;
        }

        #grid .k-grid-toolbar {
            padding: .6em 1.3em;
        }


        .toolbar {
            float: right;
        }

        @media print {
            footer {
                page-break-after: always;
            }
        }

        .numberCircle {
            border-radius: 50%;
            behavior: url(PIE.htc); /* remove if you don't care about IE8 */
            width: 20px;
            height: 20px;
            padding: 8px;
            background: #000000;
            color: white;
            border: 2px solid #000000;
            text-align: center;
            font: 18px Arial, sans-serif;
            position: fixed;
            z-index: 1000;
        }
    </style>
    <link href="../css/table.css" rel="stylesheet" />
    <div id="mainDiv" style="padding-top: 10px;">
        <%--   <h3>Order History   |    <a href="Batch.aspx">Batch History</a>   |    <a href="Report.aspx">Report</a></h3>--%>
        <div id="grid"></div>
        <script type="text/x-kendo-template" id="template">
                <div class="toolbar" style='width:100%'>
            <div style="float:left">
                     <div class="numberCircle" style="float:right">0</div>
            <div class="re-button" style="background-color:white;color:black;width:200px;margin-left:50px;font-weight:bold;display:none">Items selected</div></div>
           <div style="float:right">
             <input type="button" class="re-button" id="generateLabel" style="width: 150px" value="Generate Label"/> 
            <input type="button" class="re-button" id="btnReload" style="width: 150px" value="Sync from Shopify"/> 
            <input type="button" class="re-button" id="btnPrint" style="width: 150px" value="Print Orders"/> 
            <input type="button" class="re-button" id="btnPrintSummary" style="width: 150px" value="Create Summary"/>
            <input type="button" class="re-button" id="btnUpdateSummary" style="width: 150px" value="Add To Summary"/>
             <input type="button" class="re-button" id="btnDeleteOrders" style="width: 190px" value="Delete Selected Orders"/>
             </div>
             </div>
        </script>
        <input type="hidden" runat="server" id="hdnUserAddress" value="" />

    </div>
    <script type="text/javascript">
        var checkedIds = {};
        var checkedOrders = {};
        $(document).on("click", "#checkAll", function () {
            //debugger;
            //$(".checkbox").attr("checked", this.checked);
            // $(".checkbox").attr("checked", false);
            $(".checkbox").trigger('click');
            $(".checkbox").attr("checked", this.checked);
        });
        $(document).ready(function () {
            //$('#checkAll').click(function () {

            //    $(".checkbox").attr("checked", this.checked);
            //});
            // getData();
            var onClose = function () {
                mainGrid.data("kendoGrid").refresh();

            }
            // var dataService = getData();
            //  console.log(dataService);
            var selectedRows = [];
            var mainGrid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Default.aspx/GetOrders",
                            dataType: "JSON"
                        }
                    },
                    schema: {
                        data: "d",
                        total: function (response) {
                            debugger;
                            return response.d.length;
                        },

                        model: {
                            fields: {
                                OrderId: { type: "int" },
                                CustomerName: { type: "string" },
                                CustomerAddress: { type: "string" },
                                RecieverName: { type: "string" },
                                RecieverAddress: { type: "string" },
                                TrackingUrl: { type: "string" },
                                ItemsCount: { type: "int" },
                                OrderStatus:{type:"string"}
                            }
                        }
                    },
                    pageSize: 500
                },
                sortable: true,
                selectable: "row",
                toolbar: kendo.template($("#template").html()),
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if (checkedIds[view[i].id]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                        }
                    }
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                filterable: false,
                pageable: {
                    refresh: true,
                    pageSize: 500,
                    numeric: true,
                    buttonCount: 500,
                    info: true,
                    change: function (e) { checkedIds = []; checkedOrders = []; }
                },
                change: function (e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                    }
                },
                scrollable: true,
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        }
                    }
                },
                columns: [
                            {
                                template: "<input type='checkbox' class='checkbox' />",
                                width: 60,
                                title: "<span style='color:white'>Select</span><br/><input id='checkAll', type='checkbox', class='check-box' /> ",
                                field: "OrderId",
                                sortable: false,
                                filterable: false
                            },
                            {
                                field: "OrderId",
                                title: "Order Date",
                                filterable: false,
                                width: 80, template: "<a id='aprint' href='javascript:void(0)' style='cursor:pointer' onclick='OpenDetail(\"#=OrderId#\");'>#=OrderDate#</a>"
                            },
                            {
                                field: "OrderNumber",
                                title: "Order Number",
                                width: 80,
                                filterable: true,
                            },
                            {
                                field: "RecieverName",
                                title: "Name",
                                width: 150,
                                filterable: false
                            },
                            {
                                field: "SuccessCount",
                                title: "Items <br/> Count",
                                width: 40,
                                filterable: false
                            },
                            {
                                field: "ShippingService",
                                title: "Service",
                                width: 90,
                                filterable: true
                            },
                            {
                                field: "OrderStatus",
                                title: "Order<br/>Status",
                                width: 40,
                                filterable: false
                            },
                            {
                                field: "OrderId",
                                title: "Action",
                                width: 90,
                                filterable: false,
                                template: "<a id='aEdit' href='javascript:void(0)' style='cursor:pointer' onclick='EditData(\"#=OrderId#\",\"#=OrderNumber#\");'><img src='../Images/edit-32.jpg' title='Edit Order'/></a> | " +
                                    //"<a id='aCancel' href='javascript:void(0)' style='cursor:pointer' onclick='CancelOrder(\"#=OrderId#\");'>Cancel Order</a> | " +
                                    "<a id='aDelete' href='javascript:void(0)' style='cursor:pointer' onclick='DeleteOrder(\"#=OrderId#\");'><img src='../Images/delete-32.jpg' title='Delete Order'/></a> | " +
                                     "<a id='aAdd2Summary' href='javascript:void(0)' style='cursor:pointer' onclick='AddToSummary(\"#=OrderNumber#\",\"\");'>Add to Summary</a>"
                            },
                           // { field: "TrackingUrl", title: "Track Status", width: 90, template: "#=GetTrackingDetail(\"#=OrderId#\")#" },
                          //  { field: "TrackingUrl", title: "Track Status", width: 90, template: "<a  href='#=TrackingUrl#' style='cursor:pointer' target='_blank'>Track</a>" }
                ]
            });


            mainGrid.on("click", ".checkbox", selectRow);

            //on click of the checkbox:
            function selectRow() {
                var checked = this.checked,
                row = $(this).closest("tr"),
                grid = $("#grid").data("kendoGrid"),
                dataItem = grid.dataItem(row);
                checkedIds[dataItem.OrderId] = checked;
                checkedOrders[dataItem.OrderNumber] = checked;
                HandleCount(dataItem, checked);
                debugger;
                if (checked) {
                    row.addClass("k-state-selected");
                } else {
                    row.removeClass("k-state-selected");
                }
            }

            function HandleCount(args, checked) {
                var count = parseInt(args.SuccessCount);
                var finalCount = 0;
                var totalCount = parseInt($(".numberCircle").text());
                if (checked) {
                    finalCount = totalCount + count;
                } else {
                    finalCount = totalCount - count;
                }
                $(".numberCircle").text(finalCount);
            }
            mainGrid.find("#btnReload").bind("click", function () {
                window.location.href = 'LandingPage.aspx';
            });
            mainGrid.find("#btnPrint").bind("click", function () {
                var checked = [];
                for (var i in checkedIds) {
                    if (checkedIds[i]) {
                        checked.push(i);
                    } else {
                        checked = jQuery.grep(checked, function (a) {
                            return a !== i;
                        });
                    }
                }
                if (checked.length > 0) {
                    var dialog1 = $("#dialog-form").dialog({
                        autoOpen: false,
                        height: 480,
                        width: 800,
                        modal: true,
                        buttons: {
                            "Print": PrintOrderDetail,
                            "Close": function () {
                                dialog1.dialog("close");
                                //window.location.reload();
                            }
                        },
                        close: function () {
                            dialog1.dialog("close");
                            // window.location.reload();
                        }
                    });
                    dialog1.dialog("open");
                    $("#orderData").html("");
                    $("#loading").html("");
                    ShowPrintDetail(checked);
                }
            });
            
            mainGrid.find("#btnDeleteOrders").bind("click", function () {
                var checked = [];
                var chkOrders = [];
                for (var i in checkedIds) {
                    if (checkedIds[i]) {
                        checked.push(i);
                    } else {
                        checked = jQuery.grep(checked, function (a) {
                            return a !== i;
                        });
                    }
                }
                debugger;
                
                if (checked.length > 0) {
                    DeleteMultiOrder(checked.join(","));
                    //alert("You  deleted " + checked.join(","));
                } else {
                    alert("Select Orders to delete");
                }
            });


            mainGrid.find("#btnUpdateSummary").bind("click", function () {
                var checked = [];
                var chkOrders = [];
                for (var i in checkedIds) {
                    if (checkedIds[i]) {
                        checked.push(i);
                    } else {
                        checked = jQuery.grep(checked, function (a) {
                            return a !== i;
                        });
                    }
                }
                debugger;
                for (var i in checkedOrders) {
                    if (checkedOrders[i]) {
                        chkOrders.push(i);
                    } else {
                        chkOrders = jQuery.grep(chkOrders, function (a) {
                            return a !== i;
                        });
                    }
                }
                if (checked.length > 0) {
                    AddToSummary(checked.join(","), chkOrders.join(","));
                } else {
                    alert("Select Orders to add to summary");
                }
            });
            mainGrid.find("#btnPrintSummary").bind("click", function () {
                var checked = [];
                for (var i in checkedIds) {
                    if (checkedIds[i]) {
                        checked.push(i);
                    } else {
                        checked = jQuery.grep(checked, function (a) {
                            return a !== i;
                        });
                    }
                }
                if (checked.length > 0) {
                    var msg = '<br /><p>&nbsp;</p><center><img src="../checkedIds/waiting.gif" /><br /><h4>Please wait while order summary is being generated.</h4>.' +
                    '<br />';
                    $("#content1").html(msg);
                    var dialog1 = $("#dialog-form-2").dialog({
                        autoOpen: false,
                        height: 480,
                        width: 700,
                        modal: true,
                        buttons: {
                            "Print": PrintSummary,
                            "Close": function () {
                                $("#content1").html("");
                                dialog1.dialog("close");
                                location.reload();
                            }
                        },
                        close: function () {
                            $("#content1").html("");
                            dialog1.dialog("close");
                            location.reload();
                        }
                    });
                    dialog1.dialog("open");
                    ShowOrderSummary(checked);
                } else {
                    alert("Please select Orders");
                }
            });


            function ShowOrderSummary(args) {

                if (args.length > 0) {
                    var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while order summary is being generated.</h4>.' +
                    '<br />';
                    $("#content1").html(msg);
                    $.ajax({
                        type: 'POST',
                        url: "Default.aspx/GetOrderSummary",
                        dataType: 'json',
                        data: "{'orderids':'" + args + "'}",
                        contentType: 'application/json; charset=utf-8',
                        success: function (response) {
                            if (response.d == "-1") {
                                alert("You session has been expired. Please login again to continue.");
                                window.location.reload();
                            } else {
                                var total1 = 0;
                                var total2 = 0;
                                var data = JSON.parse(response.d);
                                var header = "";
                                var str = "<table class='customers'><tr><th>SKU Name</th><th>SKU</th><th>Quantity</th></tr>";
                                for (var i = 0; i < data.length; i++) {
                                    str = str + "<tr><td>" + data[i].Title + "</td><td>" + data[i].SKU + "</td><td>" + data[i].Quantity + "</td></tr>";
                                    if (data[i].SKU.startsWith("SPR")) {
                                        total1 = total1 + parseInt(data[i].Quantity);
                                    } else {
                                        total2 = total2 + parseInt(data[i].Quantity);
                                    }
                                }
                                header = data[0].OrderNumbers
                                header = header.slice(1);
                                header = "<h2>Summary for orders " + header + "</h2>Summary Date : " + new Date().toDateString();
                                str = str + "<tr><td colspan='2' style='text-align:right'>Total:</td><td>" + (total1 > 0 && total2 > 0 ? (total1 + " lbs" + total2 + " pc") : (total1 > 0 && total2 <= 0 ? total1 + " lbs" : total2 + " pc")) + "</td></tr></table>";
                                $("#content1").html(header + "<br/>" + str);
                            }
                        },
                        error: function (httpObj, textStatus) {
                            if (httpObj.status == "401") {
                                alert("You session has been expired. Please login again to continue.");
                                window.location.reload();
                            }
                        }
                    });
                } else {
                    alert("Please select Orders");
                }


            }
            function PrintSummary() {
                debugger;
                var data = $("#dialog-form-2").html();
                //  alert(data);
                var mywindow = window.open('', 'Item Details', 'height=400,width=600');
                mywindow.document.write('<html><head><title>Item Summary</title>');
                //mywindow.document.write('<style type="text/css">footer { page-break-after: always; } *{transition:none!important} -webkit-print-color-adjust:exact;</style>');
                mywindow.document.write(' <link href="../css/table.css" rel="stylesheet" />');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10
                setTimeout(function () {
                    mywindow.print();
                }, 1000);
                setTimeout(function () {
                    mywindow.close();
                }, 3000);



                return true;
            }
            function PrintOrderDetail() {
                debugger;
                var data = $("#orderData").html();
                var mywindow = window.open('', 'Item Details', 'height=400,width=600');
                mywindow.document.write('<html><head><title>Item Details</title>');
                //mywindow.document.write('<style type="text/css">footer { page-break-after: always; } *{transition:none!important} -webkit-print-color-adjust:exact;</style>');
                mywindow.document.write('<link href="../css/table.css" rel="stylesheet" />');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10

                //mywindow.print();
                // mywindow.close();

                setTimeout(function () {
                    mywindow.print();
                }, 1000);
                setTimeout(function () {
                    mywindow.close();
                }, 3000);
                return true;
            }
            function ShowPrintDetail(args) {
                var msg = '<center><img src="../Images/waiting.gif" /><br /><h4>Please wait while order summary is being generated.</h4>.';
                $("#loading").html(msg);
                var url = "Default.aspx/GetOrderDetails";
                debugger;
                // var array = args.split(',');
                var str = "";
                for (var i = 0; i < args.length; i++) {
                    // $("#loading").html(msg + "<center><h4>Loading order " + (i + 1) + " of " + args.length +"</h4></center>");
                    ShowDetail(args[i], url, str, args.length, msg);
                }

            }
            var k = 0;
            function ShowDetail(args, url, str, total, msg) {

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: "{'orderids':'" + args + "'}",
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        debugger;
                        k = k + 1;
                        $("#loading").html(msg + "<center><h4>Loading order " + k + " of " + total + "</h4></center>");

                        if (response.d == "-1") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        } else {
                            var data = JSON.parse(response.d);
                            var totalPrice = 0;
                            for (var j = 0; j < data.length; j++) {
                                str = str + "<table class='customers'>";
                                var shipdetail = JSON.parse(data[j]).order;
                                var lineItems = shipdetail.line_items[0];
                                str = str + "<tr><th colspan='5' style='text-align:center'>Packing Slip</th></tr>";
                                str = str + "<tr><td colspan='5'>" +
                                    "<div style='float:left;width:100%'><span>" + $("#hdnUserAddress").val() + "</span></div>" +
                                    "<br/>" +
                                    "<div style='float:left'><span><b>Ship To :</b>" + shipdetail.shipping_address.last_name + ", " + shipdetail.shipping_address.first_name + "<br/> " +
                                    shipdetail.shipping_address.address1 + " " + shipdetail.shipping_address.address2 + ", " + shipdetail.shipping_address.city + "<br/>" +
                                    shipdetail.shipping_address.province + ", " + shipdetail.shipping_address.zip + ", " + shipdetail.shipping_address.country + " " +
                                    "</div>" +
                                    "<div style='float:right'><span><b>Order Number : </b>" + shipdetail.name + "</span><br/><span><b>Order Date : </b>" + shipdetail.created_at + "</span>" +
                                    "<br/><span><b>Tracking# : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? shipdetail.fulfillments[0].tracking_number : 'Not Yet Generated') + "</span>" +
                                    "<br/><span><b>Status : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? ("<a href='" + shipdetail.fulfillments[0].tracking_url + "' target='_blank'>Click Here</a>") : "Not Available") + "</span>" +
                                    "</div>" +
                                    "</td></tr>";
                                str = str + "<tr><th>SKU</th><th>Title</th><th>Quantity</th><th>Price</th><th>Amount</th></tr>";
                                for (var i = 0; i < shipdetail.line_items.length; i++) {
                                    //str = str + 
                                    str = str + "<tr><td>" + shipdetail.line_items[i].sku + "</td><td>" + shipdetail.line_items[i].title + "</td><td style='text-align:center'>" + shipdetail.line_items[i].quantity + "</td><td style='text-align:center'> $" +
                                            shipdetail.line_items[i].price + "</td><td style='text-align:center'>$" + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price)).toFixed(2) + "</td></tr>";
                                    totalPrice = totalPrice + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price));
                                }
                                str = str + "<tr><td colspan='4' style='text-align:right'>Total</td><td style='text-align:center'>$" + totalPrice.toFixed(2) + "</td></tr></table><br/><br/><div style='page-break-after:always'></div>";

                                totalPrice = 0;


                            }
                            // $("#loading").html("<center><h4>Total " + args.length + " orders loaded for printing.</h4></center>");
                            $("#orderData").append(str);
                            $("#loading").html(msg + "<center><h4>Order " + k + " of " + total + " loaded.</h4></center>");
                            if (k == total) {
                                $("#loading").html("<center><h4>Total  " + total + " orders loaded.</h4></center>");
                                k = 0;
                            }
                        }
                    },
                    error: function (httpObj, textStatus) {
                        if (httpObj.status == "401") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        }
                    }
                });
            }
            mainGrid.find("#generateLabel").bind("click", function () {
                var checked = [];
                debugger;
                for (var i in checkedIds) {
                    if (checkedIds[i]) {
                        checked.push(i);
                    } else {
                        checked = jQuery.grep(checked, function (a) {
                            return a !== i;
                        });
                    }
                }
                if (checked.length > 0) {
                    if (checked.length > 26000) {
                        alert("At a time only 25000 orders are allowed to generate Labels. Please select only 25000 orders to proceed.");
                    } else {
                        var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while label is being generated.</h4>.' +
                        '<br /><p> Do not close or refresh this page until process is completed else label generation process will be interupted.</p></center>';
                        $("#content").html(msg);
                        var dialog = $("#dialog-form-1").dialog({
                            autoOpen: false,
                            height: 500,
                            width: 480,
                            modal: true,
                            buttons: {
                                "Print All Labels": function () {
                                    var batchId = $("#hdnBatchNumber").val();
                                    window.open("PrintOrderLabel.aspx?data=" + batchId);
                                },
                                "Close": function () {
                                    dialog.dialog("close");
                                    // window.location.href = "Batch.aspx";
                                }
                            },
                            close: function () {
                                dialog.dialog("close");
                                //window.location.href = "Batch.aspx";
                            }
                        });
                        dialog.dialog("open");
                        GenerateLabels(checked);
                    }
                } else {
                    alert("Please select orders to generate Labels");
                }
                // alert(checked);
            });
        });

        function GenerateLabels(args) {
            debugger;
            $.ajax({
                type: 'POST',
                url: "Default.aspx/GenerateLables",
                dataType: 'json',
                timeout: 0,
                data: "{'orderids':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();
                    debugger;
                    checkedIds = [];
                    var res = "<br/>Process Completed. See below result.<hr/<h4><ol>";
                    var arr;
                    $.each(response.d, function (index, value) {
                        if (index == "-1") {
                            alert("You session has been expired. Please login again to continue.");
                            checkedIds = [];
                            window.location.reload();
                        } else if (index == "-2") {
                            alert("Some technical error occured. Please contact System Admin.");
                            checkedIds = [];
                            window.location.reload();
                        } else {
                            arr = index.split('_');
                            res = res + "<li> Order #" + arr[0] + " :  " + value + "</li>";
                            checkedIds = [];
                        }
                    });
                    res = res + "</ol></h4></hr>";
                    $("#hdnBatchNumber").val(arr[1]);
                    $("#content").html(res);
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        checkedIds = [];
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        checkedIds = [];
                        alert("Some unexpedted error occurred. Please contact System Administrator");
                    }
                }
            });
        }
        function EditData(args, args1) {
            var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while data is loading for EDIT.</h4>.' +
                   '<br /><p> Do not close or refresh this page until process is completed else Editing will not be complete.</p></center>';
            $("#contentEdit").html(msg);
            var dialog1 = $("#dialog-edit-form").dialog({
                autoOpen: false,
                height: 480,
                width: 1000,
                modal: true,
                buttons: {
                    "Update": UpdateData,
                    "Close": function () {
                        $("#contentEdit").html("");
                        dialog1.dialog("close");
                    }
                },
                close: function () {
                    $("#contentEdit").html("");
                    dialog1.dialog("close");
                }
            });
            dialog1.dialog("open");
            LoadEditData(args, args1);
        }
        function LoadEditData(args, args1) {
            str = "<from id='frmEdit'>";
            $.ajax({
                type: 'POST',
                url: "Default.aspx/GetOrderDetailEdit",
                dataType: 'json',
                data: "{'orderid':'" + args + "','ordernum':'" + args1 + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    $.each(response.d, function (index, value) {
                        if (index == "-1") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        } else {
                            var str = "<table class='customers'>";
                            var data = JSON.parse(index);
                            var shipdetail = data.order;
                            var lineItems = value.Order;
                            // var lineItems = shipdetail.line_items[0];
                            var shipping = shipdetail.shipping_lines[0];
                            var srv = shipping.title;

                            var totalWeight = 0;
                            str = str + "<tr><th colspan='5'>Editing Order# " + shipdetail.name + "</th></tr>";
                            str = str + "<tr><td>Order #</td><td colspan='4'><input type='text' id='txtShippingServ' style='background-color:transparent;border:0px'readonly='readonly' value='" + shipdetail.name + "'/></td></tr>";
                            str = str + "<tr><td>Shipping Service</td><td  colspan='4'><select name='service' id='service'>" +
                                                                                                                "<option value='Economy (2 Day)' " + (srv == "Economy (2 Day)" ? "selected" : "") + ">Economy (2 Day)</option>" +
                                                                                                                "<option value='Expedited (One Day)' " + (srv == "Expedited (One Day)" ? "selected" : "") + ">Expedited (One Day)</option>" +
                                                                                                                "<option value='Economy' " + (srv == "Economy" ? "selected" : "") + ">Economy</option>" +
                                                                          "</select>" +
                                                                      "</td></tr>";
                            str = str + "<tr ><th>SKU</th><th>Title</th><th>QTY</th><th>Weight</th><th>Total Weight</th></tr>";

                            for (var i = 0; i < lineItems.line_items.length; i++) {
                                //str = str + 
                                str = str + "<tr>" +
                                    "<td><input type='text' name='sku' value='" + lineItems.line_items[i].sku + "' style='width:90%;background-color:transparent;border:0px' readonly='readonly'/></td>" +
                                    "<td>" + lineItems.line_items[i].title + "</td>" +
                                    "<td><input type='text' onchange='updatetotalweightQ(this)' name='quantity' value='" + lineItems.line_items[i].quantity + "' style='width:60px' /></td>" +
                                    "<td><input type='text' onchange='updatetotalweightW(this)' name='weight' value='" + (lineItems.line_items[i].grams1) + "' style='width:60px' /> lbs</td>" +
                                    "<td class='twt'>" + (parseFloat(lineItems.line_items[i].grams1) * lineItems.line_items[i].quantity).toFixed(2) + " lbs</td>" +
                                    "</tr>";
                                totalWeight = parseFloat(totalWeight) + parseFloat(parseFloat(lineItems.line_items[i].grams1) * lineItems.line_items[i].quantity);
                            }
                            str = str + "<tr><td colspan='4' style='text-align:right'>Total Weight</td><td class='gtwt'>" + (totalWeight).toFixed(2) + " lbs</td></tr></table>";
                            $("#contentEdit").html(str);
                        }
                    });

                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
        function updatetotalweightW(args) {
            debugger;
            var quantity = parseInt($("input[name=quantity]", $(args).parent().parent()).val());
            var totalWeight = (parseFloat($(args).val()) * parseFloat(quantity)).toFixed(2);
            // $(this).closest('tr').children('td.twt').text(totalWeight);
            $(".twt", $(args).parent().parent()).text(Math.ceil(totalWeight) + " lbs");
            var gs = 0;
            for (var i = 0; i < $(".twt").length; i++) {
                gs = parseFloat(gs) + parseFloat($(".twt")[i].innerText.replace(" lbs", ""));
            }
            $(".gtwt").text(gs + " lbs");
        }
        function updatetotalweightQ(args) {
            debugger;
            var weight = parseFloat($("input[name=weight]", $(args).parent().parent()).val()); //parseInt($(args).parent().find("input[name=weight]").val());
            var totalWeight = parseFloat($(args).val()) * weight;
            //$(args).parent().find("span[class='req']").html(totalWeight);
            $(".twt", $(args).parent().parent()).text(Math.ceil(totalWeight) + " lbs");

            var gs = 0;
            for (var i = 0; i < $(".twt").length; i++) {
                gs = parseFloat(gs) + parseFloat($(".twt")[i].innerText.replace(" lbs", ""));
            }
            $(".gtwt").text(gs + " lbs");

        }
        function UpdateData() {
            var shippingService = $("#service option:selected").val();;
            var ordrNum = $("#txtShippingServ").val();
            var arrQunatity = new Array();
            var arrWeight = new Array();
            var arrSKU = new Array();
            $("[name=sku]").each(function () {
                if (this.value != undefined) {
                    debugger;
                    arrSKU.push(this.value);
                } else {
                    debugger;
                    arrSKU.push(' ');
                }
            });
            $("[name=quantity]").each(function () {
                if (this.value != undefined) {
                    debugger;
                    arrQunatity.push(this.value);
                } else {
                    debugger;
                    arrQunatity.push('0');
                }
            });
            $("[name=weight]").each(function () {
                if (this.value != undefined) {
                    debugger;
                    arrWeight.push(this.value);
                } else {
                    debugger;
                    arrWeight.push('0');
                }
            });
            var arrDimensions = [];
            arrSKU.forEach(function (entry) {
                arrDimensions.push({ 'SKU': entry, 'Quantity': '0', 'Weight': '0', 'Title': '', 'OrderNumbers': '' });
            });
            var i = 0;
            arrQunatity.forEach(function (entry) {
                arrDimensions[i].Quantity = entry;
                i++;
            });
            i = 0;
            arrWeight.forEach(function (entry) {
                arrDimensions[i].Weight = entry;
                i++;
            });

            $.ajax({
                type: "POST",
                url: "Default.aspx/UpdateOrders",
                data: JSON.stringify({ 'dimension': arrDimensions, 'order': ordrNum, 'service': shippingService }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    alert(msg.d);
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });


        }

        function CancelOrder(args) {
            if (window.confirm("Are you sure you want to Cancel this order?")) {
                // alert(args);
                $.ajax({
                    type: 'POST',
                    url: "Default.aspx/CancelOrder",
                    dataType: 'json',
                    timeout: 0,
                    data: "{'orderId':'" + args + "'}",
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        // alert(response.d);
                        // var data = JSON.parse(response.d);
                        if (response.d == "1") {
                            //alert(data.notice);
                            alert("Order has been cancelled");
                        } else if (response.d == "-1") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        }
                        else {
                            alert("Error while cancelling order. Please try again");
                        }
                    },
                    error: function (httpObj, textStatus) {
                        if (httpObj.status == "401") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        }
                    }
                });
            }
        }

        function AddOrderToSummary() {
            var orderId = $("#spnOrderNum").text();
            var summryId = $("#ddlSummaryNumbers").val();
            var isMainButton = $("#spnOrderNum").is(":visible");
            $.ajax({
                type: 'POST',
                url: "Default.aspx/AddOrderToSummary",
                dataType: 'json',
                timeout: 0,
                data: "{'orderId':'" + orderId + "','summaryId':'" + summryId + "','IsMainButton':" + isMainButton + "}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "1") {
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();
                        alert("Order has been added to Summary");
                        dialogAdd.dialog("close");
                    } else if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                    else {
                        alert("Error while adding order to summary. Please try again");
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }

        var dialogAdd;
        function AddToSummary(args, nums) {
            dialogAdd = $("#dialog-add-to-summary").dialog({
                autoOpen: false,
                height: 250,
                width: 300,
                modal: true,
                buttons: {
                    "Add To Summary": AddOrderToSummary,
                    "Close": function () {
                        dialogAdd.dialog("close");
                        //window.location.reload();
                    }
                },
                close: function () {
                    dialogAdd.dialog("close");
                    //window.location.reload();
                }
            });
            debugger;
            $("#spnOrderNum").text(args);
            if (nums != "") {
                $("#spnOrderNum").hide();
                $("#spnOrderIds").text(nums);
                $("#spnOrderIds").show();
            } else {
                $("#spnOrderNum").show();
                $("#spnOrderIds").hide();
            }
            $.ajax({
                type: 'POST',
                url: "Default.aspx/GetSummaryNumbers",
                dataType: 'json',
                timeout: 0,
                data: "{}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    debugger;
                    if (response.d == "-3") {
                        window.alert("Session has been expired. Please login again");
                        window.location.reload();
                    }
                    if (response.d == "-1") {
                        alert("No Summary exists. Please create summary first then add orders to it");
                        //window.location.reload();
                    } else if (response.d == "-2") {
                        alert("Unable to retrieve summaries this time. Please try again later");
                    } else {
                        $("#dvContent").show();
                        dialogAdd.dialog("open");
                        var data = response.d;
                        // $ddl = $("#ddlSummaryNumbers");
                        $('#ddlSummaryNumbers').find('option').remove().end();
                        $.each(response.d, function (i, item) {
                            $('#ddlSummaryNumbers').append($('<option>', {
                                value: item,
                                text: "  " + item + "  "
                            }));
                        });

                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
        function DeleteOrder(args) {
            if (window.confirm("Are you sure you want to Delete this order?")) {
                //alert(args);
                $.ajax({
                    type: 'POST',
                    url: "Default.aspx/DeleteOrder",
                    dataType: 'json',
                    timeout: 0,
                    data: "{'orderId':'" + args + "'}",
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        if (response.d == "-1") {
                            window.alert("Session has been expired. Please login again");
                            window.location.reload();
                        }
                        if (response.d == "{}") {
                            alert("Order has been deleted");
                            window.location.reload();
                        } else {
                            alert("Unable to delete order this time. Please try again later");
                        }
                    },
                    error: function (httpObj, textStatus) {
                        if (httpObj.status == "401") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        }
                    }
                });
            }
        }

        function DeleteMultiOrder(args) {
            if (window.confirm("Are you sure you want to delete selected order(s)?")) {
                //alert(args);
                $.ajax({
                    type: 'POST',
                    url: "Default.aspx/DeleteMultipleOrder",
                    dataType: 'json',
                    timeout: 0,
                    data: "{'orderIds':'" + args + "'}",
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        if (response.d == "-1") {
                            window.alert("Session has been expired. Please login again");
                            window.location.reload();
                        }
                        if (response.d == "{}") {
                            alert("Selected Order(s) has been deleted");
                            window.location.reload();
                        } else {
                            alert("Unable to delete order this time. Please try again later");
                        }
                    },
                    error: function (httpObj, textStatus) {
                        if (httpObj.status == "401") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        }
                    }
                });
            }
        }



        function OpenDetail(args) {
            var dialog = $("#dialog-form").dialog({
                autoOpen: false,
                height: 480,
                width: 700,
                modal: true,
                buttons: {
                    "Print": PrintMe,
                    "Close": function () {
                        dialog.dialog("close");
                    }
                },
                close: function () {
                    dialog.dialog("close");
                }
            });
            function PrintMe() {
                debugger;
                var data = $("#dialog-form").html();
                var mywindow = window.open('', 'Item Details', 'height=400,width=600');
                mywindow.document.write('<html><head><title>Item Details</title>');
                //mywindow.document.write('<style type="text/css">footer { page-break-after: always; } *{transition:none!important} -webkit-print-color-adjust:exact;</style>');
                mywindow.document.write(' <link href="../css/table.css" rel="stylesheet" />');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10

                setTimeout(function () {
                    mywindow.print();
                }, 1000);
                setTimeout(function () {
                    mywindow.close();
                }, 3000);


                return true;
            }
            var flickerAPI = "Default.aspx/GetOrderDetail";
            $.ajax({
                type: 'POST',
                url: flickerAPI,
                dataType: 'json',
                data: "{'orderid':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        var str = "<table class='customers'>";
                        var data = JSON.parse(response.d);
                        var total = 0;
                        var shipdetail = data.order;
                        var lineItems = shipdetail.line_items[0];
                        str = str + "<tr><th colspan='5'>Order Detail for Order# " + shipdetail.name + "</th></tr>";
                        str = str + "<tr><td colspan='2'>Order Date</td><td colspan='4'>" + shipdetail.created_at + "</td></tr>";
                        str = str + "<tr><td>Billing Details</td><td  colspan='4'>" + shipdetail.billing_address.last_name + ", " + shipdetail.billing_address.first_name + ", " + shipdetail.billing_address.address1 + " " + shipdetail.billing_address.address2 + ", " + shipdetail.billing_address.city + ", " + shipdetail.billing_address.province + ", " + shipdetail.billing_address.zip + ", " + shipdetail.billing_address.country + "</td></tr>";
                        str = str + "<tr><td>Shipping Details</td><td  colspan='4'>" + shipdetail.shipping_address.last_name + ", " + shipdetail.shipping_address.first_name + ", " + shipdetail.shipping_address.address1 + " " + shipdetail.shipping_address.address2 + ", " + shipdetail.shipping_address.city + ", " + shipdetail.shipping_address.province + ", " + shipdetail.shipping_address.zip + ", " + shipdetail.shipping_address.country + "</td></tr>";
                        // str = str + "<tR><th><b>Ship From</b></th><td> </td></tr>";
                        //  str = str + "<tr><th colspan='5' >Line Items</th>";
                        str = str + "<tr ><th style='text-align:center;width:15%'>SKU</th><th style='text-align:center;width:50%'>Title</th><th style='text-align:center;width:5%'>QTY</th><th style='text-align:center;width:15%'>Price</th><th style='text-align:center;width:15%'>Amount</th></tr>";
                        for (var i = 0; i < shipdetail.line_items.length; i++) {
                            //str = str + 
                            str = str + "<tr><td>" + shipdetail.line_items[i].sku + "</td><td>" + shipdetail.line_items[i].title + "</td><td>" + (shipdetail.line_items[i].sku.indexOf("DOZ") !== -1 ? parseInt(shipdetail.line_items[i].quantity) * 12 : shipdetail.line_items[i].quantity) + "</td><td style='text-align:right'>$" +
                                    shipdetail.line_items[i].price + "</td><td style='text-align:right'>$" + (shipdetail.line_items[i].price * shipdetail.line_items[i].quantity).toFixed(2) + "</td></tr>";
                            total = total + parseFloat(shipdetail.line_items[i].price * shipdetail.line_items[i].quantity);
                        }
                        str = str + "<tr><td style='text-align:right' colspan='4'>Total</td><td style='text-align:right'>$" + total.toFixed(2) + "</td></tr>";
                        if (shipdetail.fulfillments.length > 0) {
                            str = str + "<tR><td colspan='2'>Tracking#</td><td colspan='3'>" + (shipdetail.fulfillments[0].tracking_number != undefined ? shipdetail.fulfillments[0].tracking_number : 'Not Yet Generated') + "</td></tr>";
                            str = str + "<tR><td colspan='2'>Check Status</td><td colspan='3'> <a href='" + shipdetail.fulfillments[0].tracking_url + "' target='_blank'>Click Here</a></td></tr>";
                        } else {
                            str = str + "<tR><td colspan='2'>Tracking Number</td><td colspan='3'>Not Available</td></tr>";
                            str = str + "<tR><td colspan='2'>Check Status</td><td colspan='3'> Not Available</td></tr>";
                        }

                        // str = str + "<tR><td colspan='2'><br/></td></tr>";
                        //  str = str + "<tR><td colspan='5'><b>*Weights are in LBS</td></tr>";
                        str = str + "</table>";
                        $("#dialog-form").html(str);
                        dialog.dialog("open");
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }

        $(window).scroll(function (e) {
            var $el = $('.numberCircle');
            var isPositionFixed = ($el.css('position') == 'fixed');
            if ($(this).scrollTop() > 200 && !isPositionFixed) {
                $('.numberCircle').css({ 'position': 'fixed', 'top': '0px' });
            }
            if ($(this).scrollTop() < 200 && isPositionFixed) {
                $('.numberCircle').css({ 'position': 'static', 'top': '0px' });
            }
        });
    </script>

    <div id="dialog-form" title="Order Detail">
        <div id="loading" style="width: 100%"></div>
        <div id="orderData" style="width: 100%">
        </div>
    </div>
    <div id="dialog-form-1" title="Label Generation">
        <input type="hidden" id="hdnBatchNumber" runat="server" value="" />
        <div id="content">
        </div>
    </div>
    <div id="dialog-form-2" title="Order Summary">
        <div id="content1">
        </div>
    </div>
    <div id="dialog-edit-form" title="Edit Order">
        <div id="contentEdit">
        </div>
    </div>
    <div id="dialog-add-to-summary" title="Add Order to Summary">
        <div id="dvContent" style="display: none">
            <br />
            <center>
            <p>
        Order Number(s) : &nbsp;&nbsp;<b><span style="word-wrap:break-word" id="spnOrderNum"></span><span style="word-wrap:break-word" id="spnOrderIds"></span></b>
        <br />
        Select Summary : <select id="ddlSummaryNumbers"></select>
            </p>
            </center>
        </div>
    </div>
</asp:Content>
