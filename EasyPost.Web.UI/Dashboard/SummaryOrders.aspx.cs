﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;

namespace UFS.Web.UI.Dashboard
{
    public partial class SummaryOrders : System.Web.UI.Page
    {
        private static string summaryId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                summaryId = "";
                if (Request.QueryString != null && Request.QueryString["summaryId"] !=null)
                {
                    summaryId = Request.QueryString["summaryId"].ToString();
                }
            }
            catch (Exception ex)
            {
                
            }

        }

        [WebMethod]
        public static List<UFS.Service.BusinessEntities.OrderDisplay> GetSummaryOrders()
        {
            if (HttpContext.Current.Session["user"] != null && !string.IsNullOrWhiteSpace(summaryId))
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                List<UFS.Service.BusinessEntities.OrderDisplay> data = new List<Service.BusinessEntities.OrderDisplay>();
                 data = new MSSQLUserOperations().GetSummaryOrders(usr.UserId,summaryId);
                return data;
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return null;
            }
        }

        [WebMethod]
        public static string CancelSummary(string summarynumber, int fromMainButton)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                if (fromMainButton ==1)
                {
                    return new MSSQLCommonDataOperations().CancelMultiOrderSummary(summarynumber, usr.UserId);
                }
                else
                {
                    return new MSSQLCommonDataOperations().CancelOrderSummary(summarynumber, usr.UserId);
                }
            }
            else
            {
                //HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return "-1";
            }
        }

    }

   
}