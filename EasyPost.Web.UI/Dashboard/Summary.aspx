﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Async="true" CodeBehind="Summary.aspx.cs" Inherits="EasyPostDemo.Dashboard.Summary" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <script src="http://cdn.kendostatic.com/2013.2.716/js/kendo.all.min.js"></script>
    <link href="../css/kendo.common.min.css" rel="stylesheet" />
    <link href="../css/kendo.metro.min.css" rel="stylesheet" />
    <style type="text/css">
        .k-grid .k-state-selected {
            background: white;
            color: black;
        }

        #grid .k-grid-toolbar {
            padding: .6em 1.3em;
        }

        #generateLabel {
            vertical-align: middle;
        }

        .toolbar {
            float: right;
        }

        html, body {
            height: 100%;
        }
    </style>
    <link href="../css/table.css" rel="stylesheet" />
    <input type="hidden" runat="server" id="hdnUserAddress" value="" />
    <div id="mainDiv" style="padding-top: 10px;">
        <%--   <h3><a href="Default.aspx">Order History</a>   |    Batch History   |    <a href="Report.aspx">Report</a></h3>--%>
        <div id="grid"></div>

    </div>
    <script type="text/javascript">
        var selectedButtonId = 0;

        $(document).ready(function () {
            // getData();
            var onClose = function () {
                mainGrid.data("kendoGrid").refresh();
            }
            // var dataService = getData();
            //  console.log(dataService);
            var selectedRows = [];
            var mainGrid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Summary.aspx/GetOrders",
                            dataType: "JSON"
                        }
                    },
                    schema: {
                        data: "d",
                        total: function (response) {
                            debugger;
                            return response.d.length;
                        },

                        model: {
                            fields: {
                                BatchNumber: { type: "string" },
                                Total: { type: "int" },
                                Failed: { type: "int" },
                                Succeeded: { type: "int" },
                                OverAllStatus: { type: "string" },
                                Processed: { type: "string" }
                            }
                        }
                    },
                    pageSize: 20
                },
                sortable: true,
                selectable: "row",
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if (checkedIds[view[i].id]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                        }
                    }
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                filterable: false,
                pageable: {
                    refresh: true,
                    pageSize: 20,
                    numeric: true,
                    buttonCount: 20,
                    info: true
                },
                change: function (e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                        // console.log(dataItem.id);
                        selectedButtonId = dataItem.id;
                    }
                },
                columns: [
                           { field: "BatchNumber", title: "Summary #", width: 40, template: "<a id='aopen' href='javascript:void(0)' style='cursor:pointer' onclick='OpenDetail(\"#=BatchNumber#\");'>#=BatchNumber#</a>" },
                            { field: "UserName", title: "Processed By", width: 50 },
                            { field: "Count", title: "Total", width: 30 },
                            { field: "OrderNumber", title: "Orders", width: 200 },
                             { field: "ProcessedDate", title: "Summary Date", width: 50 },
                            { field: "BatchNumber", title: "Action", width: 90, template: "<a id='aopen' href='javascript:void(0)' style='cursor:pointer' onclick='ProcessLabel(\"#=BatchNumber#\");'>Manage</a> | <a id='aCancel' href='javascript:void(0)' style='cursor:pointer' onclick='PrintSummaryOrders(\"#=BatchNumber#\");'>Print</a> | <a id='aCancel' href='javascript:void(0)' style='cursor:pointer' onclick='CancelSummary(\"#=BatchNumber#\");'>Cancel</a>" }

                ]
            });
            debugger;
            mainGrid.on("click", ".checkbox", selectRow);
            var checkedIds = {};

            //on click of the checkbox:
            function selectRow() {
                var checked = this.checked,
                row = $(this).closest("tr"),
                grid = $("#grid").data("kendoGrid"),
                dataItem = grid.dataItem(row);
                debugger;
                checkedIds[dataItem.OrderId] = checked;
                if (checked) {
                    //-select the row
                    row.addClass("k-state-selected");
                } else {
                    //-remove selection
                    row.removeClass("k-state-selected");
                }
            }
            mainGrid.find("#generateLabel").bind("click", function () {

            });
        });

        function OpenDetail(args) {
            var dialog = $("#dialog-form").dialog({
                autoOpen: false,
                height: 600,
                width: 600,
                modal: true,
                buttons: {
                    "Print": PrintOrderDetail,
                    "Close": function () {
                        dialog.dialog("close");
                    }
                },
                close: function () {
                    dialog.dialog("close");
                }
            });
            dialog.dialog("open");
            var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while order summary is being generated.</h4>.' +
            '<br />';
            $("#content1").html(msg);
            $.ajax({
                type: 'POST',
                url: "Summary.aspx/GetOrderSummary",
                dataType: 'json',
                data: "{'orderids':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "-1") {
                        alert("Your session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        var total1 = 0;
                        var total2 = 0;
                        var data = JSON.parse(response.d);
                        var header = "";
                        var str = "<table class='customers'><tr><th>SKU Name</th><th>SKU</th><th>Quantity</th></tr>";
                        for (var i = 0; i < data.length; i++) {
                            str = str + "<tr><td>" + data[i].Title + "</td><td>" + data[i].SKU + "</td><td>" + data[i].Quantity + "</td></tr>";
                            if (data[i].SKU.startsWith("SPR")) {
                                total1 = total1 + parseInt(data[i].Quantity);
                            } else {
                                total2 = total2 + parseInt(data[i].Quantity);
                            }
                        }
                        header = data[0].OrderNumbers;
                        header = header.slice(1);
                        header = "<h2>Summary for orders " + header + "</h2>Summary Date : " + new Date().toDateString();
                        str = str + "<tr><td colspan='2' style='text-align:right'>Total:</td><td>" + (total1 > 0 && total2 > 0 ? (total1 + " lbs" + total2 + " pc") : (total1 > 0 && total2 <= 0 ? total1 + " lbs" : total2 + " pc")) + "</td></tr></table>";
                        $("#content1").html(header + "<br/>" + str);
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });

        }

        function refreshGrid() {
            $('#grid').data('kendoGrid').dataSource.read();
            $('#grid').data('kendoGrid').refresh();
        }

        function PrintOrderSummary() {
            var data = $("#orderData").html();
            var mywindow = window.open('', 'Item Details', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Item Details</title>');
            //mywindow.document.write('<style type="text/css">footer { page-break-after: always; } *{transition:none!important} -webkit-print-color-adjust:exact;</style>');
            mywindow.document.write('<link href="../css/table.css" rel="stylesheet" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            //mywindow.print();
            // mywindow.close();

            setTimeout(function () {
                mywindow.print();
            }, 1000);
            setTimeout(function () {
                mywindow.close();
            }, 3000);
            return true;
        }
        function PrintSummaryOrders(args) {
            var dialog4 = $("#dialog-form-5").dialog({
                autoOpen: false,
                height: 480,
                width: 700,
                modal: true,
                buttons: {
                    "Print": PrintOrderSummary,
                    "Close": function () {
                        $("#orderData").html("");
                        dialog4.dialog("close");
                    }
                },
                close: function () {
                    $("#orderData").html("");
                    dialog4.dialog("close");
                }
            });
            dialog4.dialog("open");
            $("#orderData").html("");
            $("#loading").html("");
            $.ajax({
                type: 'POST',
                url: "Summary.aspx/GetOrderIds",
                dataType: 'json',
                data: "{'batchnum':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    debugger;
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        if (response.d != "0") {
                            var msg = '<center><img src="../Images/waiting.gif" /><br /><h4>Please wait while order is being generated.</h4>.';
                            $("#loading").html(msg);
                            var url = "Default.aspx/GetOrderDetails";
                            debugger;
                            var data = response.d.substring(0, response.d.length - 1);
                            var array = data.split(',');
                            var str = "";
                            for (var i = 0; i < array.length; i++) {
                                ShowDetail(array[i], url, str, array.length, msg);
                            }
                        }
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
        var k = 0;
        function ShowDetail(args, url, str, total, msg) {

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: "{'orderids':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    k = k + 1;
                    $("#loading").html(msg + "<center><h4>Loading order " + k + " of " + total + "</h4></center>");

                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        var data = JSON.parse(response.d);
                        var totalPrice = 0;
                        for (var j = 0; j < data.length; j++) {
                            str = str + "<table class='customers'>";
                            var shipdetail = JSON.parse(data[j]).order;
                            var lineItems = shipdetail.line_items[0];
                            str = str + "<tr><th colspan='5' style='text-align:center'>Packing Slip</th></tr>";
                            str = str + "<tr><td colspan='5'>" +
                                "<div style='float:left;width:100%'><span>" + $("#hdnUserAddress").val() + "</span></div>" +
                                "<br/>" +
                                "<div style='float:left'><span><b>Ship To :</b>" + shipdetail.shipping_address.last_name + ", " + shipdetail.shipping_address.first_name + "<br/> " +
                                shipdetail.shipping_address.address1 + " " + shipdetail.shipping_address.address2 + ", " + shipdetail.shipping_address.city + "<br/>" +
                                shipdetail.shipping_address.province + ", " + shipdetail.shipping_address.zip + ", " + shipdetail.shipping_address.country + " " +
                                "</div>" +
                                "<div style='float:right'><span><b>Order Number : </b>" + shipdetail.name + "</span><br/><span><b>Order Date : </b>" + shipdetail.created_at + "</span>" +
                                "<br/><span><b>Tracking# : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? shipdetail.fulfillments[0].tracking_number : 'Not Yet Generated') + "</span>" +
                                "<br/><span><b>Status : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? ("<a href='" + shipdetail.fulfillments[0].tracking_url + "' target='_blank'>Click Here</a>") : "Not Available") + "</span>" +
                                "</div>" +
                                "</td></tr>";
                            str = str + "<tr><th>SKU</th><th>Title</th><th>Quantity</th><th>Price</th><th>Amount</th></tr>";
                            for (var i = 0; i < shipdetail.line_items.length; i++) {
                                //str = str + 
                                str = str + "<tr><td>" + shipdetail.line_items[i].sku + "</td><td>" + shipdetail.line_items[i].title + "</td><td style='text-align:center'>" + shipdetail.line_items[i].quantity + "</td><td style='text-align:center'> $" +
                                        shipdetail.line_items[i].price + "</td><td style='text-align:center'>$" + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price)).toFixed(2) + "</td></tr>";
                                totalPrice = totalPrice + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price));
                            }
                            str = str + "<tr><td colspan='4' style='text-align:right'>Total</td><td style='text-align:center'>$" + totalPrice.toFixed(2) + "</td></tr></table><br/><br/><div style='page-break-after:always'></div>";

                            totalPrice = 0;


                        }
                        // $("#loading").html("<center><h4>Total " + args.length + " orders loaded for printing.</h4></center>");
                        $("#orderData").append(str);
                        $("#loading").html(msg + "<center><h4>Order " + k + " of " + total + " loaded.</h4></center>");
                        if (k == total) {
                            $("#loading").html("<center><h4>Total  " + total + " orders loaded.</h4></center>");
                            k = 0;
                        }
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
        function ProcessLabel(args) {
            var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while orders for this summary is being loaded.</h4>.' +
            '<br /><p> Do not close or refresh this page until process is completed else label generation process will be interupted.</p></center>';
            $("#dialog-form-4").html(msg);
            var dialog = $("#dialog-form-4").dialog({
                autoOpen: false,
                height: 600,
                width: 1000,
                modal: true,
                buttons: {
                    "Close": function () {
                        dialog.dialog("close");
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();
                    }
                },
                close: function () {
                    dialog.dialog("close");
                }
            });

            dialog.dialog("open");
            // GenerateLabels(args);
            var str = "<iframe src='SummaryOrders.aspx?summaryId=" + args + "' height='90%' width='100%'/>";
            $("#dialog-form-4").html(str);

        }
        function OpenDetail1(args) {
            var dialog = $("#dialog-form").dialog({
                autoOpen: false,
                height: 600,
                width: 600,
                modal: true,
                buttons: {
                    "Print": function () {

                    },
                    "Close": function () {
                        dialog.dialog("close");
                    }
                },
                close: function () {
                    dialog.dialog("close");
                }
            });
            function PrintMe() {
                var data = $("#dialog-form").html();
                var mywindow = window.open('', 'Item Details', 'height=400,width=600');
                mywindow.document.write('<html><head><title>Item Details</title>');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10

                mywindow.print();
                mywindow.close();
                return true;
            }
            var flickerAPI = "Summary.aspx/GetBatchDetail";
            $.ajax({
                type: 'POST',
                url: flickerAPI,
                dataType: 'json',
                data: "{'batch':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        var str = "<table style='width:100%' cellspacing='5' cellpadding='5' border='1'>";
                        var data = response.d;
                        str = str + "<tR><td><h3>Batch Number# " + data.BatchNumber + "</h3></td></tr>";
                        str = str + "<tR><td colspan='2'>Batch Processed at <b>" + data.ProcessedDate + "</b> by  <b>" + data.UserName + "</b></td></tr>";
                        if (data.SuccessOrders.length > 0) {
                            str = str + "<tR><td colspan='2'><h3>Successfull Orders: </h3><h4><ol>";
                            for (var i = 0; i < data.SuccessOrders.length; i++) {
                                str = str + "<li>" + (i + 1) + ". Order#" + data.SuccessOrders[i] + "    &nbsp;&nbsp;<a target='_blank' style='color:blue' href='PrintOrderLabel.aspx?data=" + data.SuccessOrders[i].replace("#", "_") + "'>Print Label </a> | <a style='color:blue' href='#' onclick='RegenerateLabel(\"" + data.SuccessOrders[i] + "\",\"" + args + "\");'>Re-Generate Label</a>" +
                                    " | <a style='color:blue' href='#' onclick='PrintOrder(\"" + data.SuccessOrders[i] + "\",\"" + args + "\");'>Print Order</a></li>";
                            }
                            str = str + "</ol></h4></td></tr>";
                        }
                        if (data.FailedOrders.length > 0) {
                            str = str + "<tR><td colspan='2'><h3>Failed Orders:</h3><h4><ol>";
                            for (var i = 0; i < data.FailedOrders.length; i++) {
                                str = str + "<li>" + (i + 1) + ". " + data.FailedOrders[i] + "    &nbsp;&nbsp;<a style='color:blue' href='#' onclick='RegenerateLabel(\"" + data.FailedOrders[i] + "\",\"" + args + "\");'>Re-Generate Label </a></li>";
                            }
                            str = str + "</ol></h4></td></tr>";
                        }
                        str = str + "</table>";
                        $("#dialog-form").html(str);
                        dialog.dialog("open");
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }

        function PrintOrderDetail() {
            debugger;
            var data = $("#content1").html();
            var mywindow = window.open('', 'Item Details', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Item Details</title>');
            mywindow.document.write('<link href="../css/table.css" rel="stylesheet" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            setTimeout(function () {
                mywindow.print();
            }, 1000);
            setTimeout(function () {
                mywindow.close();
            }, 3000);
            return true;
        }
        function PrintOrder(args) {
            var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while data is loading....</center>';
            $("#content1").html(msg);

            var dialog = $("#dialog-form-2").dialog({
                autoOpen: false,
                height: 480,
                width: 800,
                modal: true,
                buttons: {
                    "Print": PrintOrderDetail,
                    "Close": function () {
                        dialog.dialog("close");
                    }
                },
                close: function () {
                    dialog.dialog("close");
                }
            });
            dialog.dialog("open");
            var flickerAPI = "Summary.aspx/GetOrderDetails";
            $.ajax({
                type: 'POST',
                url: flickerAPI,
                dataType: 'json',
                data: "{'orderids':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        var str = "";
                        var data = JSON.parse(response.d);
                        var totalPrice = 0;
                        for (var j = 0; j < data.length; j++) {
                            str = str + "<table class='customers'>";
                            var shipdetail = JSON.parse(data[j]).order;
                            var lineItems = shipdetail.line_items[0];
                            str = str + "<tr><th colspan='5' style='text-align:center'>Packing Slip</th></tr>";
                            str = str + "<tr><td colspan='5'>" +
                                "<div style='float:left;width:100%'><span>" + $("#hdnUserAddress").val() + "</span></div>" +
                                "<br/>" +
                                "<div style='float:left'><span><b>Ship To :</b>" + shipdetail.shipping_address.last_name + ", " + shipdetail.shipping_address.first_name + "<br/> " +
                                shipdetail.shipping_address.address1 + " " + shipdetail.shipping_address.address2 + ", " + shipdetail.shipping_address.city + "<br/>" +
                                shipdetail.shipping_address.province + ", " + shipdetail.shipping_address.zip + ", " + shipdetail.shipping_address.country + " " +
                                "</div>" +
                                "<div style='float:right'><span><b>Order Number : </b>" + shipdetail.name + "</span><br/><span><b>Order Date : </b>" + shipdetail.created_at + "</span>" +
                                "<br/><span><b>Tracking# : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? shipdetail.fulfillments[0].tracking_number : 'Not Yet Generated') + "</span>" +
                                "<br/><span><b>Status : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? ("<a href='" + shipdetail.fulfillments[0].tracking_url + "' target='_blank'>Click Here</a>") : "Not Available") + "</span>" +
                                "</div>" +
                                "</td></tr>";
                            str = str + "<tr><th>SKU</th><th>Title</th><th>Quantity</th><th>Price</th><th>Amount</th></tr>";
                            for (var i = 0; i < shipdetail.line_items.length; i++) {
                                str = str + "<tr><td>" + shipdetail.line_items[i].sku + "</td><td>" + shipdetail.line_items[i].title + "</td><td style='text-align:center'>" + shipdetail.line_items[i].quantity + "</td><td style='text-align:center'> $" +
                                        shipdetail.line_items[i].price + "</td><td style='text-align:center'>$" + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price)).toFixed(2) + "</td></tr>";
                                totalPrice = totalPrice + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price));
                            }
                            str = str + "<tr><td colspan='4' style='text-align:right'>Total</td><td style='text-align:center'>$" + totalPrice.toFixed(2) + "</td></tr></table><br/><br/><div style='page-break-after:always'></div>";
                            totalPrice = 0;
                        }
                        $("#content1").html(str);
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }

        function GenerateLabels(args) {
            $.ajax({
                type: 'POST',
                url: "Summary.aspx/GenerateLables",
                dataType: 'json',
                timeout: 0,
                data: "{'orderids':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    var res = "<br/>Process Completed. See below result.<hr/<h4><ol>";
                    var arr;
                    $.each(response.d, function (index, value) {
                        if (index == "-1") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        } else {
                            arr = index.split('_');
                            res = res + "<li> Order #" + arr[0] + " :  " + value + "</li>";
                        }
                    });
                    res = res + "</ol></h4></hr>";
                    $("#content").html(res);
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
        function CancelSummary(args) {
            if (window.confirm("Are you sure you want to cancel this Summary?")) {
                var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while we are processing your request....</center>';
                $("#dialog-form-3").html(msg);
                var dialog = $("#dialog-form-3").dialog({
                    autoOpen: false,
                    height: 600,
                    width: 600,
                    modal: true,
                    buttons: {
                        "Close": function () {
                            dialog.dialog("close");
                            location.reload();
                        }
                    },
                    close: function () {
                        dialog.dialog("close");
                        location.reload();
                    }
                });
                dialog.dialog("open");
                $.ajax({
                    type: 'POST',
                    url: "Summary.aspx/CancelSummary",
                    dataType: 'json',
                    timeout: 0,
                    data: "{'summarynumber':'" + args + "'}",
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        if (response.d == "-1") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        } else {
                            var res = "<br/>Process Completed. See below result.<hr/>" + " " + response.d;
                            $("#dialog-form-3").html(res);
                        }
                    },
                    error: function (httpObj, textStatus) {
                        if (httpObj.status == "401") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        }
                    }
                });
            }
        }
    </script>

    <div id="dialog-form" title="Order Summary">
        <div id="content1">
        </div>
    </div>
    <div id="dialog-form-1" title="Label Re-Generation">
        <div id="content">
        </div>
    </div>
    <div id="dialog-form-2" title="Order Detail">
    </div>
    <div id="dialog-form-3" title="Cancel Summary">
    </div>
    <div id="dialog-form-4" title="Orders">
    </div>
    <div id="dialog-form-5" title="Order Detail" style="z-index: 12000">
        <div id="loading" style="width: 100%"></div>
        <div id="orderData" style="width: 100%">
        </div>
    </div>
</asp:Content>
