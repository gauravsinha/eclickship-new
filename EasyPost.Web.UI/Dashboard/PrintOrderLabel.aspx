﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="PrintOrderLabel.aspx.cs" Inherits="EasyPostDemo.Dashboard.PrintOrderLabel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <script src="../Scripts/jquery.min.js"></script>
    <style type="text/css">
        .button {
            clear: both;
            width: 80%;
            border: none;
            background: #282828;
            color: #FFF;
            padding: 7px 0px 7px 0px;
            font-size: 16px;
            margin-top: 5px;
        }

        .imgLabels {
            max-height: 400px;
            max-width: 600px;
        }

        /*img {
            height: 60%;
            position: absolute;
        }*/
    </style>
    <script>
        function PrintMe() {

            var data = $("#dvBody").html();
            var mywindow = window.open('', 'Item Details', 'height=400,width=600');
            mywindow.document.write('<html><head><title></title>');
            mywindow.document.write('<style type="text/css">img{height:35pc;position:absolute} .break{ page-break-after:always; }</style>');
            // mywindow.document.write(' <link href="../css/table.css" rel="stylesheet" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            setTimeout(function () {
                mywindow.print();
            }, 1000);
            setTimeout(function () {
                mywindow.close();
            }, 3000);



            return true;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: auto; text-align: center;">
            <%--<input type="button" class="button" style="width: 20%" onclick="PrintMe();" value="Print" /><p>&nbsp;</p>--%>
            <asp:Button ID="printLabels" CssClass="button" Style="width: 20%" runat="server" Text="Print Label" OnClick="printLabels_Click" />
            <div id="dvInfo">*Do not use browser print functionality.Click on the above button to print.</div>
        </div>
        <br /><br /><hr />
        <center>
        <div id="dvContainer" style="margin-left: 0px; margin-top: 0px">
            <div runat="server" id="dvBody">
            </div>
        </div>
        </center>
    </form>
</body>
</html>
