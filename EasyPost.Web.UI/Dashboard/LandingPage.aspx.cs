﻿using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using UFS.Service.BusinessEntities;
using Newtonsoft.Json;

namespace EasyPostDemo.Dashboard
{
    public partial class LandingPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx?");
                }
                //else
                //{
                //    FetchDatafromStore();
                //    Response.Redirect("~/Dashboard/Default.aspx", true);
                //}
            }
            catch (Exception ex)
            {

                mainDiv.InnerHtml = "<h3>Error occured . Contact System Admin. Error is " + ex.Message;
            }
        }

        [WebMethod]
        public static string FetchDatafromStore()
        {
            try
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                string data = GetServiceData(usr, "All", 0);
                UFS.Service.BusinessEntities.CustomerOrder ordr = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue, RecursionLimit = 1000 }.Deserialize<UFS.Service.BusinessEntities.CustomerOrder>(data);
                new MSSQLUserOperations().AddShopifyOrders(ordr, usr.UserId);
                foreach (var item in ordr.orders)
                {
                    AddOrderDetail(item.id.ToString(), usr);
                }
                return "0";

            }
            catch (Exception ex)
            {
                //return "1";// + ex.Message;
                return ex.Message;
            }
        }
        private static void AddOrderDetail(string orderId, UserDetail usr)
        {
            string data = GetServiceData(usr, "Order", Convert.ToInt64(orderId));
            UFS.Web.ShopifyOrder.MainOrder ordr = JsonConvert.DeserializeObject<UFS.Web.ShopifyOrder.MainOrder>(data); // new JavaScriptSerializer().Deserialize<UFS.Web.ShopifyOrder.MainOrder>(data);
            new MSSQLUserOperations().AddShopifyOrderDetail(ordr, usr.UserId);
        }
        public static string GetServiceData(UserDetail usr, string datatoFetch, Int64 orderId)
        {
            Shopify.Web.Interaction.RestServiceAgent agent = new Shopify.Web.Interaction.RestServiceAgent();
            string apiKey = usr.ShopifyApiKey;
            string authKey = usr.AuthCode;
            string pass = usr.ShopifyApiPassword;
            string url = usr.ShpifyStoreUrl.Replace("http://", "").Replace("https://", "");
            string fullUrl = "https://{0}:{1}@{2}/admin/orders.json";
            bool isApp = !(!string.IsNullOrWhiteSpace(usr.ShopifyApiKey) && !string.IsNullOrWhiteSpace(usr.ShopifyApiPassword));
            switch (datatoFetch)
            {
                case "All":
                    fullUrl = isApp ? "https://" + url + "/admin/orders.json?limit=250" : "https://{0}:{1}@{2}/admin/orders.json?limit=250";// "https://{0}:{1}@{2}/admin/orders.json";
                    break;
                case "Order":
                    fullUrl = isApp ? "https://" + url + "/admin/orders/" + orderId.ToString() + ".json" : "https://{0}:{1}@{2}/admin/orders/" + orderId.ToString() + ".json";//   "https://{0}:{1}@{2}/admin/orders/" + orderId.ToString() + ".json";
                    break;
                default:
                    break;
            }
            if (isApp)
            {
                agent.Request = new Shopify.Web.Interaction.RestRequest
                {
                    RequestURL = "https://" + usr.ShpifyStoreUrl + "/admin/oauth/access_token",
                };
                agent.GetAccessToken(authKey);
            }
            agent.Request = new Shopify.Web.Interaction.RestRequest()
            {
                RequestAuthorizationHeader = new Shopify.Web.Interaction.KeyValuePair()
                {
                    Key = isApp ? "X-Shopify-Access-Token" : "Authorization",
                    Value = authKey,
                },
                RequestURL = string.Format(fullUrl, apiKey, pass, url)
            };

            string data = agent.MakeGetRequestAsync();
            return data;
        }


    }
}