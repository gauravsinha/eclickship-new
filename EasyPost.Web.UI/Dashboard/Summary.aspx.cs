﻿using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using UFS.Service.BusinessEntities;
using UFS.Web.Dashboard;
using System.Web.Configuration;
using System.IO;
using UFS.Messaging.EmailService;

namespace EasyPostDemo.Dashboard
{
    public partial class Summary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    Response.Redirect("~/Account/Login.aspx?");
                }
                else
                {

                    UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                    hdnUserAddress.Value = "<b>" + (!string.IsNullOrWhiteSpace(usr.Company) ? usr.Company : usr.FirstName + " " + usr.LastName) + "</b><br/>" + usr.Address.Street1 + " " + usr.Address.Street2 + "<br/>" + usr.Address.City + ", " + usr.Address.State + " " + usr.Address.Zip;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [WebMethod]
        public static string GetOrderDetails(string orderids)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                List<object> res = new List<object>();
                var item = new MSSQLUserOperations().GetOrderId(orderids, usr.UserId);
                res.Add(GetServiceData(usr, "Order", Convert.ToInt64(item)));
                return new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue }.Serialize(res);
            }
            else
            {
              //  HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return "-1";
            }
        }


        private static string GetServiceData(UserDetail usr, string datatoFetch, Int64 orderId)
        {
            Shopify.Web.Interaction.RestServiceAgent agent = new Shopify.Web.Interaction.RestServiceAgent();
            string apiKey = usr.ShopifyApiKey;
            string pass = usr.ShopifyApiPassword;
            string url = usr.ShpifyStoreUrl.Replace("http://", "").Replace("https://", "");
            string fullUrl = "https://{0}:{1}@{2}/admin/orders.json"; ;
            switch (datatoFetch)
            {
                case "All":
                    fullUrl = "https://{0}:{1}@{2}/admin/orders.json";
                    break;
                case "Order":
                    fullUrl = "https://{0}:{1}@{2}/admin/orders/" + orderId.ToString() + ".json"; ;
                    break;
                default:
                    break;
            }

            agent.Request = new Shopify.Web.Interaction.RestRequest()
            {
                RequestAuthorizationHeader = new Shopify.Web.Interaction.KeyValuePair()
                {
                    Key = "Authorization",
                    Value = usr.AuthCode,
                },
                RequestURL = string.Format(fullUrl, apiKey, pass, url)
            };

            string data = agent.MakeGetRequestAsync();
            return data;
        }

        [WebMethod]
        public static string GetOrderSummary(string orderids)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                if (!string.IsNullOrWhiteSpace(orderids))
                {
                    orderids = new MSSQLUserOperations().GetOrderIdsOfSummary(orderids);
                    string orderNums = "";
                    UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                    string[] arr = orderids.Split(new char[] { ',' });
                    UFS.Web.ShopifyOrder.MainOrder ordr = new UFS.Web.ShopifyOrder.MainOrder();
                    List<UFS.Web.ShopifyOrder.LineItem> lstLineItms = new List<UFS.Web.ShopifyOrder.LineItem>();
                    foreach (var item in arr)
                    {
                        //  res.Add();
                        if (!string.IsNullOrWhiteSpace(item))
                        {
                            ordr = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue }.Deserialize<UFS.Web.ShopifyOrder.MainOrder>(GetServiceData(usr, "Order", Convert.ToInt64(item)));
                            if (ordr != null && ordr.Order != null && ordr.Order.line_items != null && ordr.Order.line_items.Count > 0)
                            {
                                orderNums = orderNums + ", #" + ordr.Order.order_number.ToString();
                                foreach (var lItem in ordr.Order.line_items)
                                {
                                    if (lItem.sku == "DOZ101")
                                    {
                                        lItem.sku = "RBW101";
                                        lItem.quantity = lItem.quantity * 12;
                                        lItem.title = "The Original Rainbow Bagelâ„¢";
                                    }
                                    if (lItem.sku == "DOZ102")
                                    {
                                        lItem.sku = "CRG101";
                                        lItem.quantity = lItem.quantity * 12;
                                        lItem.title = "The Original Cragel";
                                    }
                                    lstLineItms.Add(lItem);
                                }
                            } 
                        }
                    }
                    List<SummaryData> lstData = new List<SummaryData>();
                    if (    lstLineItms!=null && lstLineItms.Count>0)
                    {
                        if (lstLineItms != null && lstLineItms.Count > 0)
                        {
                            var item = lstLineItms.GroupBy(m => m.sku);
                            foreach (var val in item)
                            {
                                lstData.Add(new SummaryData()
                                {
                                    SKU = val.Key,
                                    Title = val.FirstOrDefault(m => m.sku == val.Key).title,
                                    Quantity = (val.Key.ToUpper().StartsWith("DOZ") ? (val.Sum(m => m.quantity) * 12).ToString() + " pc" : val.Sum(m => m.quantity).ToString() + " " + (val.Key.ToUpper().StartsWith("SPR") ? "lbs" : "pc")),
                                });
                            }
                            if (lstData != null && lstData.Count > 0)
                            {
                                lstData[0].OrderNumbers = orderNums;
                            }
                        }
                        return new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue }.Serialize(lstData);
                    }
                    else
                    {
                        return " No Order exists";
                    }

                }
                else
                {
                    return "Please select Orders";
                }
            }
            else
            {
                //HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                //return null;
                return "-1";
            }

        }

        [WebMethod]
        public static object GetOrders()
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                var data = new MSSQLUserOperations().GetSummaryOrders(usr.UserId);
                var grp = data.GroupBy(m => m.BatchNumber);
                List<UFS.Service.BusinessEntities.OrderDisplay> ordr = new List<OrderDisplay>();
                foreach (IGrouping<string, OrderDisplay> item in grp)
                {
                    ordr.Add(new OrderDisplay()
                    {
                        BatchNumber = item.Key,
                        OrderNumber = item.FirstOrDefault(m => m.BatchNumber == item.Key).OrderNumber,
                        Count = item.Count(m => m.BatchNumber == item.Key),
                        FailedCount = item.Count(n => n.BatchStatus == "0"),
                        SuccessCount = item.Count(n => n.BatchStatus == "1"),
                        BatchStatus = item.Count(n => n.BatchStatus == "0") == 0 ? "Processed" : (item.Count(n => n.BatchStatus == "1") == 0 ? "Failed" : "Partially Processed"),
                        ProcessedDate = item.FirstOrDefault(m => m.BatchNumber == item.Key).ProcessedDate,
                        UserName = item.FirstOrDefault(m => m.BatchNumber == item.Key).UserName
                    });
                }

                return ordr.OrderByDescending(m => m.BatchNumber);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return null;
            }
        }

        [WebMethod]
        public static string GetBatchOrderIds(string batchnum)
        {
            try
            {
                if (HttpContext.Current.Session["user"] != null)
                {
                    string data = new MSSQLReportDataOperations().GetBatchOrders(batchnum);
                    return data;
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                    return null;
                }
               
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
        [WebMethod]
        public static object GetOrderDetail(string orderid)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                var data = GetServiceData(usr, "Order", Convert.ToInt64(orderid));
                return data;
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return null;
            }
        }

        [WebMethod]
        public static object GetBatchDetail(string batch)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                var data = new MSSQLReportDataOperations().GetBatchData(batch, usr.UserId);
                OrderDisplay ordr = new OrderDisplay()
                {
                    BatchNumber = data[0].BatchNumber,
                    Count = data.Count,
                    BatchStatus = data.Count(m => m.BatchStatus == "0") == 0 ? "Processed" : (data.Count(n => n.BatchStatus == "1") == 0 ? "Failed" : "Partially Processed"),
                    FailedCount = data.Count(m => m.BatchStatus == "0"),
                    SuccessCount = data.Count(m => m.BatchStatus == "1"),
                    ProcessedDate = data[0].ProcessedDate,
                    UserName = data[0].UserName,
                    OrderNumbers = data.Select(m => m.OrderNumber).ToList<string>(),
                    FailedOrders = data.Where(m => m.BatchStatus == "0").Select(n => n.OrderNumber).ToList<string>(),
                    SuccessOrders = data.Where(m => m.BatchStatus == "1").Select(n => n.OrderNumber).ToList<string>()
                };
                return ordr;
            }
            else
            {
               // HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return "-1";
            }
        }


        [WebMethod]
        public static string GetLabels(string shipmentId)
        {
            try
            {
                if (HttpContext.Current.Session["user"] != null)
                { 
                string randString = UFS.Web.Common.CommonUtilities.GetRandomString(10);
                string images = new MSSQLReportDataOperations().GetLabels(shipmentId);
                if (!string.IsNullOrWhiteSpace(images))
                {
                    string[] arrLabels = images.Split(new char[] { ',' });
                    if (arrLabels.Length > 0)
                    {
                        HttpContext.Current.Session[randString] = arrLabels.ToList<string>();
                        return randString;
                    }
                    else
                    {
                        return "Unable to get labels at this time. Please try again in few moments or contatc system admin.";
                    }
                }
                else
                {
                    return "Unable to get labels at this time. Please try again in few moments or contatc system admin.";
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return null;
            }
        }
            catch (Exception)
            {
                throw;
            }
        }

        [WebMethod]
        public static string GetOrderIds(string batchnum)
        {
            try
            {
                if (HttpContext.Current.Session != null)
                {
                    string data = new MSSQLReportDataOperations().GetSummaryOrderIds(batchnum);
                    return data;
                }
                else
                {
                    return "-1";
                }
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        [WebMethod]
        public static string GetShipmentId(string siteOrderId)
        {
            try
            {
                if (HttpContext.Current.Session["user"] != null)
                {
                    string randString = UFS.Web.Common.CommonUtilities.GetRandomString(10);
                    string shipmentId = new MSSQLReportDataOperations().GetShipmentIds(siteOrderId);
                    if (!string.IsNullOrWhiteSpace(shipmentId))
                    {
                        return shipmentId;
                    }
                    return "";
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                    return null;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [WebMethod]
        public static string CancelSummary(string summarynumber)
        {
            if (HttpContext.Current.Session["user"] != null) { 
                return new MSSQLCommonDataOperations().CancelSummary(summarynumber);
            }
            else
            {
                //HttpContext.Current.Response.Redirect("~/Account/Login.aspx?ReturnUrl=../Dashboard/");
                return "-1";
            }
        }

        [WebMethod]
        public static Dictionary<string, string> GenerateLables(string orderids)
        {
            if (HttpContext.Current.Session["user"] != null)
            {
                Dictionary<string, string> response = new Dictionary<string, string>();
                if (!string.IsNullOrWhiteSpace(orderids))
                {
                    orderids = new MSSQLUserOperations().GetOrderIdsOfSummary(orderids);
                    string[] ids = orderids.Split(new char[] { ',' });
                    string BatchNo = DateTime.Now.ToString("yyyyMMddHHmmss");
                    List<string> urls = new List<string>();
                    List<string> trackingNums = new List<string>();
                    decimal amount = 0M;
                    string ordernum = "";
                    foreach (string item in ids)
                    {
                        object result = GetOrderDetail(item);

                        UFS.Web.Dashboard.MainOrder ordr = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue }.Deserialize<UFS.Web.Dashboard.MainOrder>(result.ToString());
                        if (ordr != null)
                        {
                            ordernum = ordernum + " " + ordr.Order.order_number.ToString() + ",";
                            response.Add(ordr.Order.order_number.ToString() + "_" + BatchNo, ProcessOrder(ordr.Order, BatchNo, ref urls, ref trackingNums, ref amount));
                        }
                    }
                    if (amount > 0)
                    {
                        ordernum = ordernum.TrimEnd(new char[] { ',' });
                        SendEmail(urls, BatchNo, trackingNums, amount, ordernum);
                    }
                }
                else
                {
                    response.Add("0", "No order selected");
                }

                return response;
            }
            else
            {
                return new Dictionary<string, string>() { { "-1", "<span style = 'color:red' >Your session has been expired. Please Log out and Login again.</span>" } };
            }
        }
        private static string ProcessOrder(UFS.Web.Dashboard.Order rawOrdr, string batchNo, ref List<string> urls, ref List<string> trackNums, ref decimal totalAmount)
        {
            try
            {
                //  urls = new List<string>();
                // trackNums = new List<string>();
                ShipmentDetail shp = ConvertObject(rawOrdr);
                EasyPost.Order ordr = CreateEshipperOrder(shp);
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                UFS.eShipper.Wrapper.eShipperWrapper wrapper = new UFS.eShipper.Wrapper.eShipperWrapper();
                string service = new MSSQLUserOperations().GetService(rawOrdr.id, usr.UserId);
                service = !string.IsNullOrWhiteSpace(service) ? service : rawOrdr.shipping_lines[0].title;
                wrapper.ServiceId = GetServiceId(service);
                wrapper.OrderNumber = rawOrdr.order_number;
                wrapper.Buy(ordr);
                if (wrapper.Messages != null && wrapper.Messages.Count > 0)
                {
                    //Task.Factory.StartNew(() => UpdateBatch(rawOrdr.id.ToString(), 0, batchNo, "", "","0.00",string.Empty));
                    //UpdateBatch(rawOrdr.id.ToString(), 0, batchNo, "", "");
                    return "<span style = 'color:red' > " + wrapper.Messages["100"] + "</span>";
                }
                else
                {
                    string labelUrls = "";
                    string trackingNums = "";
                    ordr.id = wrapper.UniqueObjectCode;
                    ordr.shipments = new List<EasyPost.Shipment>();
                    foreach (var item in wrapper.ShippingLabel)
                    {
                        ordr.shipments.Add(new EasyPost.Shipment()
                        {
                            postage_label = new EasyPost.PostageLabel()
                            {
                                label_url = item.Value
                            },
                            tracking_code = item.Key
                        });
                        labelUrls = labelUrls + " " + item.Value + ",";
                        trackingNums = trackingNums + "" + item.Key + ",";
                        totalAmount = totalAmount + decimal.Parse(wrapper.AmountCharged);
                        urls.Add(item.Value);
                        trackNums.Add(item.Key);
                    }
                    if (!string.IsNullOrWhiteSpace(labelUrls) && !string.IsNullOrWhiteSpace(trackingNums))
                    {
                        labelUrls = labelUrls.TrimEnd(new char[] { ',' });
                        trackingNums = trackingNums.TrimEnd(new char[] { ',' });
                        UpdateBatch(rawOrdr.id.ToString(), 1, batchNo, trackingNums, labelUrls, wrapper.AmountCharged, wrapper.CarrierName);
                        if (rawOrdr.fulfillments.Count > 0)
                        {
                            //UpdateShopifyLog(rawOrdr.id.ToString(), rawOrdr.fulfillments[0].id.ToString(), trackingNums, wrapper.ServiceName, wrapper.CarrierName);
                        }
                        else
                        {
                            // AddShopifyLog(rawOrdr.id.ToString(), trackingNums, wrapper.ServiceName, wrapper.CarrierName);
                        }
                        return "<span style='color:green'>Label Generated Successfully. <a href='PrintOrderLabel.aspx?data=" + rawOrdr.order_number + "' target='_blank'>Print</a></span>";
                    }
                    else
                    {
                        return "<span style='color:red'> Unable to generate labels this time. Please try again later or contact system administrator</span>";
                    }
                }
            }
            catch (Exception ex)
            {
                //Task.Factory.StartNew(() => UpdateBatch(rawOrdr.id.ToString(), 0, batchNo, "", "","0.00",string.Empty));
                return "<span style='color:green'> Unable to generate labels this time. Please try again later or contact system administrator</span>";

            }
        }

        private static void SendEmail(List<string> imageUrl, string batchNum, List<string> trackingNums, decimal amount, string ids)
        {
            try
            {
                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                EmailService email = new EmailService("", usr.EmailAddress);
                Dictionary<Stream, string> lstStream = new Dictionary<Stream, string>();
                email.Attachments = new Dictionary<Stream, string>();
                email.CCMailAddressList = new List<System.Net.Mail.MailAddress>();
                if (!string.IsNullOrWhiteSpace(usr.EmailAddress1))
                    email.CCMailAddressList.Add(new System.Net.Mail.MailAddress(usr.EmailAddress1));

                if (!string.IsNullOrWhiteSpace(usr.EmailAddress2))
                    email.CCMailAddressList.Add(new System.Net.Mail.MailAddress(usr.EmailAddress2));

                foreach (var item in imageUrl)
                {

                    Stream strm = null;
                    if (item.Contains("../Labels/"))
                    {
                        strm = new FileStream(HttpContext.Current.Server.MapPath(item.Replace("..", "~")), FileMode.Open, FileAccess.Read);
                    }
                    if (strm != null)
                    {
                        string[] fil = item.Split(new char[] { '.' });
                        string extn = fil[fil.Length - 1].ToString();
                        lstStream.Add(strm, extn);
                    }
                }
                email.Attachments = lstStream;
                email.MailBody = "<img src='http://www.eclickship.com/images/logo-name.png' alt='logo'><p>Hello " + usr.FirstName + " " + usr.LastName + " !</p> <br/><p>Find the attached label and below detail of your created on eClickShip <br/></p><p>" +
                    "<b>Tracking Numbers : </b><br/>" + (trackingNums != null && trackingNums.Count > 0 ? String.Join(",", trackingNums.Select(x => x.ToString()).ToArray()) : "Not generated.") + "</p><br/><p>" +
                    "<b>Order Numbers : </b><br/>" + (ids) + "<br/></p><p>" +
                    "<b>Batch Number :</b><br/>" + batchNum + "<br/></p><p>" +
                    "Total Amount chargs : " + amount.ToString("0.00") + "</p>";

                if (WebConfigurationManager.AppSettings["LabelSubject"] != null)
                {
                    email.Subject = WebConfigurationManager.AppSettings["LabelSubject"].ToString();// "Package Details Created on United Freight Savers";
                }
                else
                {
                    email.Subject = "Package Details Created on eClickShip : Batch# " + batchNum;
                }
                string resp = email.SendMail();

            }
            catch (Exception ex)
            {
                // dvResult1.InnerHtml = "Unale to Send Mail. Error is : " + ex.Message;
            }
        }


        private static void UpdateBatch(string id, int status, string batch, string trackingCode, string labelUrl, string amount, string carr)
        {
            try
            {
                new MSSQLReportDataOperations().UpdateBatch(id, status, batch, trackingCode, labelUrl, amount, carr);
            }
            catch (Exception exs)
            {
            }
        }
        private static string ProcessOrder(UFS.Web.Dashboard.Order rawOrdr, string batchNo)
        {
            try
            {
                ShipmentDetail shp = ConvertObject(rawOrdr);
                EasyPost.Order ordr = CreateEshipperOrder(shp);
                UFS.eShipper.Wrapper.eShipperWrapper wrapper = new UFS.eShipper.Wrapper.eShipperWrapper();
                wrapper.ServiceId = GetServiceId(rawOrdr.shipping_lines[0].title);
                wrapper.Buy(ordr);
                if (wrapper.Messages != null && wrapper.Messages.Count > 0)
                {

                    return "<span style = 'color:red' > " + wrapper.Messages["100"] + "</span>";
                }
                else
                {
                    string labelUrls = "";
                    string trackingNums = "";
                    ordr.id = wrapper.UniqueObjectCode;
                    ordr.shipments = new List<EasyPost.Shipment>();
                    foreach (var item in wrapper.ShippingLabel)
                    {
                        ordr.shipments.Add(new EasyPost.Shipment()
                        {
                            postage_label = new EasyPost.PostageLabel()
                            {
                                label_url = item.Value
                            },
                            tracking_code = item.Key
                        });
                        labelUrls = labelUrls + " " + item.Value + ",";
                        trackingNums = trackingNums + "" + item.Key + ",";
                    }
                    if (ordr.shipments != null && ordr.shipments.Count > 0)
                    {
                        // UpdateBatch(rawOrdr.id.ToString(), 1, batchNo, trackingNums, labelUrls);
                        if (!string.IsNullOrWhiteSpace(labelUrls) && !string.IsNullOrWhiteSpace(trackingNums))
                        {
                            labelUrls = labelUrls.TrimEnd(new char[] { ',' });
                            trackingNums = trackingNums.TrimEnd(new char[] { ',' });
                            UpdateBatch(rawOrdr.id.ToString(), 1, batchNo, trackingNums, labelUrls, wrapper.AmountCharged, wrapper.CarrierName);
                            if (rawOrdr.fulfillments.Count > 0)
                            {
                                // UpdateShopifyLog(rawOrdr.id.ToString(), rawOrdr.fulfillments[0].id.ToString(), trackingNums, wrapper.ServiceName, wrapper.CarrierName);
                            }
                            else
                            {
                                // AddShopifyLog(rawOrdr.id.ToString(), trackingNums, wrapper.ServiceName, wrapper.CarrierName);
                            }
                            return "<span style='color:green'>Label Generated Successfully. <a href='PrintOrderLabel.aspx?data=" + rawOrdr.order_number + "' target='_blank'>Print</a></span>";
                        }
                        else
                        {
                            return "<span style='color:red'> Unable to generate labels this time. Please try again later or contact system administrator</span>";
                        }
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                return "<span style = 'color:red' > " + ex.Message + "</span>";
            }
        }

        private static string GetServiceId(string title)
        {
            return title.ToUpper().Contains("EXPEDITED") ? (title.ToUpper().Contains("ONE") ? "28" : "29") : (title.ToUpper().Contains("ECONOMY") ? "3" : "");
        }

        private static ShipmentDetail ConvertObject(UFS.Web.Dashboard.Order rawOrdr)
        {
            try
            {

                UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
                ShipmentDetail shp = new ShipmentDetail();
                shp.FromAddress = new Address()
                {
                    Name = usr.FirstName + " " + usr.LastName,
                    Company = usr.Company,
                    Street1 = usr.Address.Street1,
                    Street2 = usr.Address.Street2,
                    City = usr.Address.City,
                    Country = usr.Address.Country,
                    Phone = usr.Address.FormattedPhone,
                    State = usr.Address.State,
                    Zip = usr.Address.Zip
                };
                shp.ToAddress = new Address()
                {
                    Name = rawOrdr.shipping_address.name,
                    Company = rawOrdr.shipping_address.company != null ? rawOrdr.shipping_address.company.ToString() : string.Empty,
                    Street1 = rawOrdr.shipping_address.address1,
                    Street2 = rawOrdr.shipping_address.address2,
                    City = rawOrdr.shipping_address.city,
                    Country = rawOrdr.shipping_address.country_code,
                    Phone = rawOrdr.shipping_address.phone,
                    State = rawOrdr.shipping_address.province_code,
                    Zip = rawOrdr.shipping_address.zip
                };
                shp.Carrier = "EShipper";
                shp.Parcel = new ParcelDetails()
                {
                    CarrierCode = "EShipper",
                    CarrierName = "EShipper",
                    CODAmount = "0.00",
                    Dimension = GetDimensions(rawOrdr.line_items),
                };

                return shp;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private static List<PackageDimensions> GetDimensions(List<LineItem> line_items)
        {
            List<PackageDimensions> dim = new List<PackageDimensions>();
            long count;
            double weight;
            weight = line_items.Where(m => m.sku.ToUpper().StartsWith("ART") || m.sku.ToUpper().StartsWith("CRG") || m.sku.ToUpper().StartsWith("RBW")).Sum(l => l.grams);
            count = line_items.Where(m => m.sku.ToUpper().StartsWith("ART") || m.sku.ToUpper().StartsWith("CRG") || m.sku.ToUpper().StartsWith("RBW")).Sum(l => l.quantity);
            if (count > 0)
            {
                string weightLBS = (Math.Ceiling((weight * 0.00220462) + 1)).ToString();
                if (count <= 12)
                {
                    dim.Add(new PackageDimensions()
                    {
                        Length = "12",
                        Width = "12",
                        Height = "5",
                        Weight = weightLBS,
                        Qunatity = "1"
                    });
                }
                if (count > 12 && count <= 24)
                {
                    dim.Add(new PackageDimensions()
                    {
                        Length = "12",
                        Width = "12",
                        Height = "8",
                        Weight = weightLBS,
                        Qunatity = "1"
                    });
                }
                if (count > 24 && count <= 36)
                {
                    dim.Add(new PackageDimensions()
                    {
                        Length = "12",
                        Width = "12",
                        Height = "10",
                        Weight = weightLBS,
                        Qunatity = "1"
                    });
                }
                if (count > 36 && count <= 48)
                {
                    dim.Add(new PackageDimensions()
                    {
                        Length = "12",
                        Width = "12",
                        Height = "10",
                        Weight = weightLBS,
                        Qunatity = "1"
                    });
                    dim.Add(new PackageDimensions()
                    {
                        Length = "12",
                        Width = "12",
                        Height = "5",
                        Weight = weightLBS,
                        Qunatity = "1"
                    });

                }
            }
            count = line_items.Where(m => m.sku.ToUpper().StartsWith("SPR")).Sum(l => l.quantity);
            if (count > 0)
            {
                decimal result = count / 3;
                for (int i = 0; i < Math.Ceiling(result); i++)
                {
                    dim.Add(new PackageDimensions()
                    {
                        Length = "10",
                        Width = "6",
                        Height = "5",
                        Weight = "5",
                        Qunatity = "1"
                    });
                }


            }

            return dim;

        }

        private static EasyPost.Order CreateEshipperOrder(ShipmentDetail shp)
        {
            EasyPost.Order ordr = new EasyPost.Order();
            ordr.to_address = new EasyPost.Address()
            {
                name = shp.ToAddress.Name,
                company = shp.ToAddress.Company,
                street1 = shp.ToAddress.Street1,
                street2 = shp.ToAddress.Street2,
                city = shp.ToAddress.City,
                state = shp.ToAddress.State,
                country = shp.ToAddress.Country,
                phone = shp.ToAddress.Phone,
                zip = shp.ToAddress.Zip
            };
            ordr.from_address = new EasyPost.Address()
            {
                name = shp.FromAddress.Name,
                company = shp.FromAddress.Company,
                street1 = shp.FromAddress.Street1,
                street2 = shp.FromAddress.Street2,
                city = shp.FromAddress.City,
                state = shp.FromAddress.State,
                country = shp.FromAddress.Country,
                phone = shp.FromAddress.Phone,
                zip = shp.FromAddress.Zip
            };
            ordr.shipments = new List<EasyPost.Shipment>();
            shp.Parcels = new List<ParcelDetails>();
            foreach (var item in shp.Parcel.Dimension)
            {
                if (!string.IsNullOrWhiteSpace(item.Qunatity) && item.Qunatity != "0")
                {
                    FIllParcelsObjectForUPS(shp, item);
                    Dictionary<string, object> dcOptions = null;
                    for (int i = 0; i < Convert.ToInt32(item.Qunatity); i++)
                    {
                        dcOptions = new Dictionary<string, object>();
                        dcOptions.Add("declared_value", item.DecVal);
                        dcOptions.Add("delivery_confirmation", shp.Parcel.Signature);
                        dcOptions.Add("hold_for_pickup", shp.Parcel.HoldForPickup);
                        if (item.IsCOD)
                        {
                            dcOptions.Add("cod_amount", item.CODAmount);
                            dcOptions.Add("cod_method", item.CODMethod);
                            dcOptions.Add("currency", "USD");
                        }
                        if (item.PredefinedParcel != null && item.PredefinedParcel != "Custom")
                        {

                            ordr.shipments.Add(new EasyPost.Shipment()
                            {
                                parcel = new EasyPost.Parcel()
                                {
                                    weight = !string.IsNullOrWhiteSpace(item.Weight) ? double.Parse(item.Weight) : 0,
                                    predefined_package = item.PredefinedParcel
                                },
                                options = dcOptions
                            });

                        }
                        else
                        {
                            ordr.shipments.Add(new EasyPost.Shipment()
                            {
                                parcel = new EasyPost.Parcel()
                                {
                                    length = !string.IsNullOrWhiteSpace(item.Length) ? double.Parse(item.Length) : 0,
                                    width = !string.IsNullOrWhiteSpace(item.Width) ? double.Parse(item.Width) : 0,
                                    height = !string.IsNullOrWhiteSpace(item.Height) ? double.Parse(item.Height) : 0,
                                    //weight = !string.IsNullOrWhiteSpace(item.Weight) ? double.Parse(item.Weight) * 16 : 0,
                                    weight = !string.IsNullOrWhiteSpace(item.Weight) ? double.Parse(item.Weight) : 0,
                                },
                                options = dcOptions
                            });
                        }
                    }
                }
            }
            ordr.reference = "OrderRef";
            UFS.eShipper.Wrapper.eShipperWrapper serv = new UFS.eShipper.Wrapper.eShipperWrapper();
            UserDetail usr = (UserDetail)HttpContext.Current.Session["user"];
            List<string> emailAddresses = new List<string>();
            if (usr != null && !string.IsNullOrWhiteSpace(usr.EmailAddress))
            {
                emailAddresses.Add(usr.EmailAddress);
                if (!string.IsNullOrWhiteSpace(usr.EmailAddress1))
                    emailAddresses.Add(usr.EmailAddress1);
                if (!string.IsNullOrWhiteSpace(usr.EmailAddress2))
                    emailAddresses.Add(usr.EmailAddress2);

                shp.EmailAddress = emailAddresses.ToArray<string>();
            }
            serv.Shipment = shp;
            /*serv.Create();
             ordr.rates = new List<EasyPost.Rate>();
             if (serv.Rates != null && serv.Rates.Count > 0)
             {
                 foreach (var item in serv.Rates)
                 {
                     ordr.rates.Add(new EasyPost.Rate()
                     {
                         carrier = "EShipper",
                         currency = item.Currency,
                         id = item.RateId,
                         rate = item.Amount,
                         service = item.ServiceId,
                         mode = item.ServiceDesc,
                         delivery_date = new DateTime()
                     });
                 }
             }
             if (serv.Messages != null && serv.Messages.Count > 0)
             {
                 ordr.messages = new List<string>();
                 foreach (var item in serv.Messages)
                 {
                     ordr.messages.Add(item.Value.ToString());
                 }
             }*/
            return ordr;
        }
        private static void FIllParcelsObjectForUPS(ShipmentDetail shp, PackageDimensions item)
        {
            for (int i = 0; i < Convert.ToInt32(item.Qunatity); i++)
            {
                shp.Parcels.Add(new ParcelDetails()
                {
                    CarrierCode = shp.Parcel.CarrierCode,
                    CarrierName = shp.Parcel.CarrierName,
                    CODAmount = item.CODAmount,
                    CODMethod = item.CODMethod,
                    IsCOD = item.IsCOD,
                    DeclaredValue = item.DecVal,
                    Height = item.Height,
                    HoldForPickup = shp.Parcel.HoldForPickup,
                    Length = item.Length,
                    PredefinedParcel = item.PredefinedParcel,
                    Signature = shp.Parcel.Signature,
                    Weight = item.Weight,
                    Width = item.Width

                });
            }
        }

    }
}