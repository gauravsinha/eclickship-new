﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SummaryOrders.aspx.cs" Inherits="UFS.Web.UI.Dashboard.SummaryOrders" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
    <link href="../Content/style.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../Scripts/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="http://cdn.kendostatic.com/2013.2.716/js/kendo.all.min.js"></script>
    <link href="../css/kendo.common.min.css" rel="stylesheet" />
    <link href="../css/kendo.metro.min.css" rel="stylesheet" />
    <style type="text/css">
        .k-grid .k-grid-header .k-header .k-link {
            height: auto;
        }

        .k-grid .k-grid-header .k-header {
            white-space: normal;
        }

        .k-grid .k-state-selected {
            background: white;
            color: black;
        }

        #grid .k-grid-toolbar {
            padding: .6em 1.3em;
        }


        .toolbar {
            float: right;
        }

        @media print {
            footer {
                page-break-after: always;
            }
        }

        .re-button {
            clear: both;
            width: 98%;
            border: none;
            background: #282828;
            color: #FFF;
            padding: 7px 0px 7px 0px;
            font-size: 16px;
            margin-top: 5px;
        }
    </style>
    <link href="../css/table.css" rel="stylesheet" />
</head>
<body>
    <div id="mainDiv" style="padding-top: 10px;">
        <%--   <h3>Order History   |    <a href="Batch.aspx">Batch History</a>   |    <a href="Report.aspx">Report</a></h3>--%>
        <div id="grid"></div>
        <script type="text/x-kendo-template" id="template">
                <div class="toolbar" style='width:100%'>
                   <div style='float:left;'>* Select Orders from the below grid and clcik on 'Generate Label' button to generate labels.</div> 
            <div style='float:right;'><input type="button" class="re-button" id="generateLabel" style="width: 150px" value="Generate Label"/>
            <input type="button" class="re-button" id="removeOrders" style="width: 150px" value="Remove Orders"/> </div>
                </div>
        </script>
        <input type="hidden" runat="server" id="hdnUserAddress" value="" />

    </div>
    <script type="text/javascript">
        var selectedButtonId = 0;
        $(document).on("click", "#checkAll", function () {
            $(".checkbox").trigger('click');
            $(".checkbox").attr("checked", this.checked);
        });
        $(document).ready(function () {
            var onClose = function () {
                mainGrid.data("kendoGrid").refresh();
            }
            var selectedRows = [];
            var mainGrid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "SummaryOrders.aspx/GetSummaryOrders",
                            dataType: "JSON"
                        }
                    },
                    schema: {
                        data: "d",
                        total: function (response) {
                            debugger;
                            return response.d.length;
                        },

                        model: {
                            fields: {
                                OrderId: { type: "int" },
                                CustomerName: { type: "string" },
                                CustomerAddress: { type: "string" },
                                RecieverName: { type: "string" },
                                RecieverAddress: { type: "string" },
                                TrackingUrl: { type: "string" }
                            }
                        }
                    },
                    pageSize: 500
                },
                sortable: true,
                selectable: "row",
                toolbar: kendo.template($("#template").html()),
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if (checkedIds[view[i].id]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                        }
                    }
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                filterable: false,
                pageable: {
                    refresh: true,
                    pageSize: 500,
                    numeric: true,
                    buttonCount: 500,
                    info: true,
                    change: function (e) { checkedIds = []; }
                },
                change: function (e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                        // console.log(dataItem.id);
                        selectedButtonId = dataItem.id;
                    }
                },
                columns: [
                            {
                                template: "<input type='checkbox' class='checkbox' />",
                                width: 30,
                                title: "<span style='color:white'>Select</span><input id='checkAll', type='checkbox', class='check-box' /> ",
                                field: "OrderId",
                                sortable: false
                            },
                            {
                                field: "OrderDate",
                                title: "Order Date",
                                width: 70
                            },
                            {
                                field: "OrderNumber",
                                title: "Order Number",
                                width: 40
                            },
                            {
                                field: "RecieverName",
                                title: "Name",
                                width: 120
                            },
                            {
                                field: "OrderStatus",
                                title: "Status",
                                width: 60
                            },
                            {
                                field: "ShippingService",
                                title: "Service",
                                width: 90
                            },
                            {
                                field: "OrderNumber",
                                title: "Remove",
                                width: 60,
                                template: "<a id='aCancel' href='javascript:void(0)' style='cursor:pointer' onclick='CancelSummary(\"#=OrderNumber#\",0);'>Remove</a>"
                            }
                ]
            });


            mainGrid.on("click", ".checkbox", selectRow);
            var checkedIds = {};

            //on click of the checkbox:
            function selectRow() {
                var checked = this.checked,
                row = $(this).closest("tr"),
                grid = $("#grid").data("kendoGrid"),
                dataItem = grid.dataItem(row);
                debugger;
                checkedIds[dataItem.OrderId] = checked;
                if (checked) {
                    //-select the row
                    row.addClass("k-state-selected");
                } else {
                    //-remove selection
                    row.removeClass("k-state-selected");
                }
            }
            mainGrid.find("#removeOrders").bind("click", function () {
                var checked = [];
                for (var i in checkedIds) {
                    if (checkedIds[i]) {
                        checked.push(i);
                    }
                }
                if (checked.length > 0) {
                    CancelSummary(checked.join(','), 1);
                } else {
                    alert("Please select orders to remove from summary");
                }
            });
            mainGrid.find("#generateLabel").bind("click", function () {
                var checked = [];
                for (var i in checkedIds) {
                    if (checkedIds[i]) {
                        checked.push(i);
                    }
                }
                if (checked.length > 0) {
                    if (checked.length > 2600) {
                        alert("At a time only 25000 orders are allowed to generate Labels. Please select only 25000 orders to proceed.");
                    } else {
                        var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while label is being generated.</h4>.' +
                        '<br /><p> Do not close or refresh this page until process is completed else label generation process will be interupted.</p></center>';
                        $("#content").html(msg);
                        debugger;
                        var dialog = $("#dialog-form-1").dialog({
                            autoOpen: false,
                            height: 500,
                            width: 480,
                            modal: true,
                            buttons: {
                                "Print All Labels": function () {
                                    var batchId = $("#hdnBatchNumber").val();
                                    window.open("PrintOrderLabel.aspx?data=" + batchId);
                                },
                                "Close": function () {
                                    dialog.dialog("close");
                                }
                            },
                            close: function () {
                                dialog.dialog("close");
                            }
                        });
                        dialog.dialog("open");
                        GenerateLabels(checked);
                    }
                } else {
                    alert("Please select orders to generate Labels");
                }
                // alert(checked);
            });
        });

        function GenerateLabels(args) {
            $.ajax({
                type: 'POST',
                url: "Default.aspx/GenerateSummaryLabel",
                dataType: 'json',
                timeout: 0,
                data: "{'orderids':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        parent.location.reload();
                        checkedIds = [];
                    } else {
                        var res = "<br/>Process Completed. See below result.<hr/<h4><ol>";
                        var arr;
                        $.each(response.d, function (index, value) {
                            arr = index.split('_');
                            res = res + "<li> Order #" + arr[0] + " :  " + value + "</li>";
                        });
                        res = res + "</ol></h4></hr>";
                        $("#hdnBatchNumber").val(arr[1]);
                        $("#content").html(res);
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();
                        parent.refreshGrid();
                        checkedIds = [];
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        parent.location.reload();
                        checkedIds = [];
                    }
                }
            });
        }

        function PrintMe() {
            debugger;
            var data = $("#dialog-form").html();
            var mywindow = window.open('', 'Item Details', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Item Details</title>');
            mywindow.document.write(' <link href="../css/table.css" rel="stylesheet" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            setTimeout(function () {
                mywindow.print();
            }, 1000);
            setTimeout(function () {
                mywindow.close();
            }, 3000);


            return true;
        }
        function CancelSummary(args, mainButton) {
            if (window.confirm("Are you sure you want to cancel this Summary?")) {
                $.ajax({
                    type: 'POST',
                    url: "SummaryOrders.aspx/CancelSummary",
                    dataType: 'json',
                    timeout: 0,
                    data: "{'summarynumber':'" + args + "','fromMainButton':" + mainButton + "}",
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        if (response.d == "-1") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        } else {
                            //var res = "<br/>Process Completed. See below result.<hr/>" + " " + response.d;
                            //$("#dialog-form-3").html(res);
                            alert(response.d);
                            $('#grid').data('kendoGrid').dataSource.read();
                            $('#grid').data('kendoGrid').refresh();
                        }
                    },
                    error: function (httpObj, textStatus) {
                        if (httpObj.status == "401") {
                            alert("You session has been expired. Please login again to continue.");
                            window.location.reload();
                        }
                    }
                });
            }
        }

    </script>

    <div id="dialog-form-1" title="Label Generation">
        <input type="hidden" id="hdnBatchNumber" runat="server" value="" />
        <div id="content">
        </div>
    </div>
</body>
</html>
