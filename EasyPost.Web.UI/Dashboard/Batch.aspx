﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Async="true" CodeBehind="Batch.aspx.cs" Inherits="EasyPostDemo.Dashboard.Batch" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <script src="http://cdn.kendostatic.com/2013.2.716/js/kendo.all.min.js"></script>
    <link href="../css/kendo.common.min.css" rel="stylesheet" />
    <link href="../css/kendo.metro.min.css" rel="stylesheet" />
    <style type="text/css">
        .k-grid .k-state-selected {
            background: white;
            color: black;
        }

        #grid .k-grid-toolbar {
            padding: .6em 1.3em;
        }

        #generateLabel {
            vertical-align: middle;
        }

        .toolbar {
            float: right;
        }
    </style>
    <link href="../css/table.css" rel="stylesheet" />
    <input type="hidden" runat="server" id="hdnUserAddress" value="" />
    <div id="mainDiv" style="padding-top: 10px;">
        <%--   <h3><a href="Default.aspx">Order History</a>   |    Batch History   |    <a href="Report.aspx">Report</a></h3>--%>
        <div id="grid"></div>

    </div>
    <script type="text/javascript">
        var selectedButtonId = 0;
        $(document).ready(function () {
            // getData();
            var onClose = function () {
                mainGrid.data("kendoGrid").refresh();
            }
            // var dataService = getData();
            //  console.log(dataService);
            var selectedRows = [];
            var mainGrid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Batch.aspx/GetOrders",
                            dataType: "JSON"
                        }
                    },
                    schema: {
                        data: "d",
                        total: function (response) {
                            debugger;
                            return response.d.length;
                        },

                        model: {
                            fields: {
                                BatchNumber: { type: "string" },
                                Total: { type: "int" },
                                Failed: { type: "int" },
                                Succeeded: { type: "int" },
                                OverAllStatus: { type: "string" },
                                Processed: { type: "string" }
                            }
                        }
                    },
                    pageSize: 20
                },
                sortable: true,
                selectable: "row",
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if (checkedIds[view[i].id]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                        }
                    }
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                filterable: false,
                pageable: {
                    refresh: true,
                    pageSize: 20,
                    numeric: true,
                    buttonCount: 20,
                    info: true
                },
                change: function (e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                        // console.log(dataItem.id);
                        selectedButtonId = dataItem.id;
                    }
                },
                columns: [
                           // { template: "<input type='checkbox' class='checkbox' />", width: 50, title: "Select", field: "OrderId" },
                           // { field: "OrderId", title: "Order Date", width: 80, template: "<a id='aprint' href='javascript:void(0)' style='cursor:pointer' onclick='OpenDetail(\"#=OrderId#\");'>#=OrderDate#</a>" },
                           { field: "BatchNumber", title: "Batch #", width: 90, template: "<a id='aopen' href='javascript:void(0)' style='cursor:pointer' onclick='OpenDetail(\"#=BatchNumber#\");'>#=BatchNumber#</a>" },
                           // { field: "BatchNumber", title: "Batch #", width: 80 },
                            { field: "UserName", title: "Processed By", width: 80 },
                            { field: "ProcessedDate", title: "Processed", width: 80 },
                            { field: "Count", title: "Total", width: 80 },
                            { field: "SuccessCount", title: "Label Generated", width: 80 },
                            { field: "FailedCount", title: "Failed", width: 80 },
                            { field: "BatchStatus", title: "Status ", width: 80 },
                           // { field: "OrderStatus", title: "Status", width: 80 },
                           // { field: "ShippingService", title: "Service", width: 130 }
                           // { field: "TrackingUrl", title: "Track Status", width: 90, template: "#=GetTrackingDetail(\"#=OrderId#\")#" },
                          //  { field: "TrackingUrl", title: "Track Status", width: 90, template: "<a  href='#=TrackingUrl#' style='cursor:pointer' target='_blank'>Track</a>" }
                ]
            });
            debugger;
            mainGrid.on("click", ".checkbox", selectRow);
            var checkedIds = {};

            //on click of the checkbox:
            function selectRow() {
                var checked = this.checked,
                row = $(this).closest("tr"),
                grid = $("#grid").data("kendoGrid"),
                dataItem = grid.dataItem(row);
                debugger;
                checkedIds[dataItem.OrderId] = checked;
                if (checked) {
                    //-select the row
                    row.addClass("k-state-selected");
                } else {
                    //-remove selection
                    row.removeClass("k-state-selected");
                }
            }
            mainGrid.find("#generateLabel").bind("click", function () {
                var checked = [];
                for (var i in checkedIds) {
                    if (checkedIds[i]) {
                        checked.push(i);
                    }
                }
                if (checked.length > 0) {
                    var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while label is being generated.</h4>.' +
                    '<br /><p> Do not close or refresh this page until process is completed else label generation process will be interupted.</p></center>';
                    $("#content").html(msg);
                    var dialog = $("#dialog-form-1").dialog({
                        autoOpen: false,
                        height: 600,
                        width: 600,
                        modal: true,
                        buttons: {
                            "Close": function () {
                                dialog.dialog("close");
                            }
                        },
                        close: function () {
                            dialog.dialog("close");
                        }
                    });
                    dialog.dialog("open");
                    debugger;
                    GenerateLabels(checked);
                } else {
                    alert("Please select orders to generate Labels");
                }
                // alert(checked);
            });
        });


        function OpenDetail(args) {
            var dialog = $("#dialog-form").dialog({
                autoOpen: false,
                height: 600,
                width: 600,
                modal: true,
                buttons: {
                    "Print All" : function () {
                        PrintOrders(args);
                    },
                    "Print All Labels": function () {
                        //alert("Args is " + args);
                        window.open("PrintOrderLabel.aspx?data=" + args);
                    },
                    "Re-Generate Label for Batch": function () {
                        //alert("Args is " + args);

                        GenerateLabelsForBatch(args);
                    },
                    // "Packing Slips": PrintMe,
                    "Close": function () {
                        dialog.dialog("close");
                    }
                },
                close: function () {
                    dialog.dialog("close");
                    // form[0].reset();
                    // allFields.removeClass("ui-state-error");
                }
            });
            function PrintMe() {
                var data = $("#dialog-form").html();
                var mywindow = window.open('', 'Item Details', 'height=400,width=600');
                mywindow.document.write('<html><head><title>Item Details</title>');
                /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10

                mywindow.print();
                mywindow.close();
                return true;
            }
            var flickerAPI = "Batch.aspx/GetBatchDetail";
            $.ajax({
                type: 'POST',
                url: flickerAPI,
                dataType: 'json',
                data: "{'batch':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        var str = "<table style='width:100%' cellspacing='5' cellpadding='5' border='1'>";
                        var data = response.d;
                        str = str + "<tR><td><h3>Batch Number# " + data.BatchNumber + "</h3></td></tr>";
                        str = str + "<tR><td colspan='2'>Batch Processed at <b>" + data.ProcessedDate + "</b> by  <b>" + data.UserName + "</b></td></tr>";
                        //   str = str + "<tR><td colspan='2'>Total Orders Processed : " + data.Count + " and Successfully Label Generated for  " + data.SuccessCount + " orders  and " + data.FailedCount + " orders failed.</td></tr>";
                        if (data.SuccessOrders.length > 0) {
                            str = str + "<tR><td colspan='2'><h3>Successfull Orders: </h3><h4><ol>";
                            for (var i = 0; i < data.SuccessOrders.length; i++) {
                                str = str + "<li>" + (i + 1) + ". Order#" + data.SuccessOrders[i] + "    &nbsp;&nbsp;<a target='_blank' style='color:blue' href='PrintOrderLabel.aspx?data=" + data.SuccessOrders[i].replace("#", "_") + "'>Print Label </a> | <a style='color:blue' href='#' onclick='RegenerateLabel(\"" + data.SuccessOrders[i] + "\",\"" + args + "\");'>Re-Generate Label</a>" +
                                    " | <a style='color:blue' href='#' onclick='PrintOrder(\"" + data.SuccessOrders[i] + "\",\"" + args + "\");'>Print Order</a></li>";
                            }
                            str = str + "</ol></h4></td></tr>";
                        }
                        if (data.FailedOrders.length > 0) {
                            str = str + "<tR><td colspan='2'><h3>Failed Orders:</h3><h4><ol>";
                            for (var i = 0; i < data.FailedOrders.length; i++) {
                                str = str + "<li>" + (i + 1) + ". " + data.FailedOrders[i] + "    &nbsp;&nbsp;<a style='color:blue' href='#' onclick='RegenerateLabel(\"" + data.FailedOrders[i] + "\",\"" + args + "\");'>Re-Generate Label </a></li>";
                            }
                            str = str + "</ol></h4></td></tr>";
                        }
                        str = str + "</table>";
                        $("#dialog-form").html(str);
                        dialog.dialog("open");
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });


        }
        function RegenerateLabel(args, args2) {
            var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while label is being generated.</h4>.' +
                    '<br /><p> Do not close or refresh this page until process is completed else label generation process will be interupted.</p></center>';
            $("#content").html(msg);
            var dialog = $("#dialog-form-1").dialog({
                autoOpen: false,
                height: 600,
                width: 600,
                modal: true,
                buttons: {
                    "Close": function () {
                        dialog.dialog("close");
                    }
                },
                close: function () {
                    dialog.dialog("close");
                }
            });
            dialog.dialog("open");
            GenerateLabels(args, args2);
        }

        function PrintOrderDetail() {
            debugger;
            var data = $("#content1").html();
            var mywindow = window.open('', 'Item Details', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Item Details</title>');
            mywindow.document.write('<link href="../css/table.css" rel="stylesheet" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            setTimeout(function () {
                mywindow.print();
            }, 1000);
            setTimeout(function () {
                mywindow.close();
            }, 3000);
            return true;
        }
        function PrintOrder(args) {
            var msg = '<br /><p>&nbsp;</p><center><img src="../Images/waiting.gif" /><br /><h4>Please wait while data is loading....</center>';
            $("#content1").html(msg);

            var dialog = $("#dialog-form-2").dialog({
                autoOpen: false,
                height: 480,
                width: 800,
                modal: true,
                buttons: {
                    "Print": function () {
                        PrintOrderDetail();
                    },
                    "Close": function () {
                        dialog.dialog("close");
                    }

                },
                close: function () {
                    dialog.dialog("close");
                }
            });
            dialog.dialog("open");
            var flickerAPI = "Batch.aspx/GetOrderDetails";
            $.ajax({
                type: 'POST',
                url: flickerAPI,
                dataType: 'json',
                data: "{'orderids':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        var str = "";
                        var data = JSON.parse(response.d);
                        var totalPrice = 0;
                        for (var j = 0; j < data.length; j++) {
                            str = str + "<table class='customers'>";
                            var shipdetail = JSON.parse(data[j]).order;
                            var lineItems = shipdetail.line_items[0];
                            str = str + "<tr><th colspan='5' style='text-align:center'>Packing Slip</th></tr>";
                            str = str + "<tr><td colspan='5'>" +
                                "<div style='float:left;width:100%'><span>" + $("#hdnUserAddress").val() + "</span></div>" +
                                "<br/>" +
                                "<div style='float:left'><span><b>Ship To :</b>" + shipdetail.shipping_address.last_name + ", " + shipdetail.shipping_address.first_name + "<br/> " +
                                shipdetail.shipping_address.address1 + " " + shipdetail.shipping_address.address2 + ", " + shipdetail.shipping_address.city + "<br/>" +
                                shipdetail.shipping_address.province + ", " + shipdetail.shipping_address.zip + ", " + shipdetail.shipping_address.country + " " +
                                "</div>" +
                                "<div style='float:right'><span><b>Order Number : </b>" + shipdetail.name + "</span><br/><span><b>Order Date : </b>" + shipdetail.created_at + "</span>" +
                                "<br/><span><b>Tracking# : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? shipdetail.fulfillments[0].tracking_number : 'Not Yet Generated') + "</span>" +
                                "<br/><span><b>Status : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? ("<a href='" + shipdetail.fulfillments[0].tracking_url + "' target='_blank'>Click Here</a>") : "Not Available") + "</span>" +
                                "</div>" +
                                "</td></tr>";
                            str = str + "<tr><th>SKU</th><th>Title</th><th>Quantity</th><th>Price</th><th>Amount</th></tr>";
                            for (var i = 0; i < shipdetail.line_items.length; i++) {
                                //str = str + 
                                str = str + "<tr><td>" + shipdetail.line_items[i].sku + "</td><td>" + shipdetail.line_items[i].title + "</td><td style='text-align:center'>" + shipdetail.line_items[i].quantity + "</td><td style='text-align:center'> $" +
                                        shipdetail.line_items[i].price + "</td><td style='text-align:center'>$" + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price)).toFixed(2) + "</td></tr>";
                                totalPrice = totalPrice + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price));
                            }
                            str = str + "<tr><td colspan='4' style='text-align:right'>Total</td><td style='text-align:center'>$" + totalPrice.toFixed(2) + "</td></tr></table><br/><br/><div style='page-break-after:always'></div>";
                            totalPrice = 0;

                        }
                        $("#content1").html(str);
                        // dialog.dialog("open");
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
       
        function PrintOrderSummary() {
            debugger;
            var data = $("#orderData").html();
            var mywindow = window.open('', 'Item Details', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Item Details</title>');
            //mywindow.document.write('<style type="text/css">footer { page-break-after: always; } *{transition:none!important} -webkit-print-color-adjust:exact;</style>');
            mywindow.document.write('<link href="../css/table.css" rel="stylesheet" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            //mywindow.print();
            // mywindow.close();

            setTimeout(function () {
                mywindow.print();
            }, 1000);
            setTimeout(function () {
                mywindow.close();
            }, 3000);
            return true;
        }
        function PrintOrders(args) {
            var dialog4 = $("#dialog-form-3").dialog({
                autoOpen: false,
                height: 480,
                width: 700,
                modal: true,
                buttons: {
                    "Print": PrintOrderSummary,
                    "Close": function () {
                        $("#orderData").html("");
                        dialog4.dialog("close");
                    }
                },
                close: function () {
                    $("#orderData").html("");
                    dialog4.dialog("close");
                }
            });
            dialog4.dialog("open");
            $("#orderData").html("");
            $("#loading").html("");
            $.ajax({
                type: 'POST',
                url: "Batch.aspx/GetOrderIds",
                dataType: 'json',
                data: "{'batchnum':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    debugger;
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        if (response.d != "0") {
                            var msg = '<center><img src="../Images/waiting.gif" /><br /><h4>Please wait while order is being generated.</h4>.';
                            $("#loading").html(msg);
                            var url = "Default.aspx/GetOrderDetails";
                            debugger;
                            var data = response.d.substring(0, response.d.length - 1);
                            var array = data.split(',');
                            var str = "";
                            for (var i = 0; i < array.length; i++) {                                
                                ShowDetail(array[i], url, str, array.length, msg);
                            }
                        }
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
        var k = 0;
        function ShowDetail(args, url, str, total, msg) {

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: "{'orderids':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    k = k + 1;
                    $("#loading").html(msg + "<center><h4>Loading order " + k + " of " + total + "</h4></center>");

                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        var data = JSON.parse(response.d);
                        var totalPrice = 0;
                        for (var j = 0; j < data.length; j++) {
                            str = str + "<table class='customers'>";
                            var shipdetail = JSON.parse(data[j]).order;
                            var lineItems = shipdetail.line_items[0];
                            str = str + "<tr><th colspan='5' style='text-align:center'>Packing Slip</th></tr>";
                            str = str + "<tr><td colspan='5'>" +
                                "<div style='float:left;width:100%'><span>" + $("#hdnUserAddress").val() + "</span></div>" +
                                "<br/>" +
                                "<div style='float:left'><span><b>Ship To :</b>" + shipdetail.shipping_address.last_name + ", " + shipdetail.shipping_address.first_name + "<br/> " +
                                shipdetail.shipping_address.address1 + " " + shipdetail.shipping_address.address2 + ", " + shipdetail.shipping_address.city + "<br/>" +
                                shipdetail.shipping_address.province + ", " + shipdetail.shipping_address.zip + ", " + shipdetail.shipping_address.country + " " +
                                "</div>" +
                                "<div style='float:right'><span><b>Order Number : </b>" + shipdetail.name + "</span><br/><span><b>Order Date : </b>" + shipdetail.created_at + "</span>" +
                                "<br/><span><b>Tracking# : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? shipdetail.fulfillments[0].tracking_number : 'Not Yet Generated') + "</span>" +
                                "<br/><span><b>Status : </b>" + (shipdetail.fulfillments.length > 0 && shipdetail.fulfillments[0].tracking_number != undefined ? ("<a href='" + shipdetail.fulfillments[0].tracking_url + "' target='_blank'>Click Here</a>") : "Not Available") + "</span>" +
                                "</div>" +
                                "</td></tr>";
                            str = str + "<tr><th>SKU</th><th>Title</th><th>Quantity</th><th>Price</th><th>Amount</th></tr>";
                            for (var i = 0; i < shipdetail.line_items.length; i++) {
                                //str = str + 
                                str = str + "<tr><td>" + shipdetail.line_items[i].sku + "</td><td>" + shipdetail.line_items[i].title + "</td><td style='text-align:center'>" + shipdetail.line_items[i].quantity + "</td><td style='text-align:center'> $" +
                                        shipdetail.line_items[i].price + "</td><td style='text-align:center'>$" + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price)).toFixed(2) + "</td></tr>";
                                totalPrice = totalPrice + (parseFloat(shipdetail.line_items[i].quantity) * parseFloat(shipdetail.line_items[i].price));
                            }
                            str = str + "<tr><td colspan='4' style='text-align:right'>Total</td><td style='text-align:center'>$" + totalPrice.toFixed(2) + "</td></tr></table><br/><br/><div style='page-break-after:always'></div>";

                            totalPrice = 0;


                        }
                        // $("#loading").html("<center><h4>Total " + args.length + " orders loaded for printing.</h4></center>");
                        $("#orderData").append(str);
                        $("#loading").html(msg + "<center><h4>Order " + k + " of " + total + " loaded.</h4></center>");
                        if (k == total) {
                            $("#loading").html("<center><h4>Total  " + total + " orders loaded.</h4></center>");
                            k = 0;
                        }
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
        function GenerateLabelsForBatch(args) {
            $.ajax({
                type: 'POST',
                url: "Batch.aspx/GetBatchOrderIds",
                dataType: 'json',
                data: "{'batchnum':'" + args + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    } else {
                        if (response.d != "0") {
                            RegenerateLabel(response.d, args);
                        }
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
        function GenerateLabels(args, args2) {

            $.ajax({
                type: 'POST',
                url: "Batch.aspx/GenerateLables",
                dataType: 'json',
                data: "{'orderids':'" + args + "','batchNo':'" + args2 + "'}",
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.d == "-1") {
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                        checkedIds = [];
                    } else {
                        checkedIds = [];
                        var res = "<br/>Process Completed. See below result.<hr/<h4><ol>";
                        $.each(response.d, function (index, value) {
                            if (index == "-1") {
                                alert("Your session has been expired. Please login again to continue");
                                window.location.reload();
                            } else {
                                res = res + "<li> Order #" + index + " :  " + value + "</li>";
                            }
                        });
                        res = res + "</ol></h4></hr>";
                        checkedIds = [];
                        $("#content").html(res);
                    }
                },
                error: function (httpObj, textStatus) {
                    if (httpObj.status == "401") {
                        checkedIds = [];
                        alert("You session has been expired. Please login again to continue.");
                        window.location.reload();
                    }
                }
            });
        }
    </script>

    <div id="dialog-form" title="Label Batch Status">
    </div>
    <div id="dialog-form-1" title="Label Re-Generation">
        <div id="content">
        </div>
    </div>
    <div id="dialog-form-2" title="Order Detail">
        <div id="content1">
        </div>
    </div>
    <div id="dialog-form-3" title="Order Detail" style="z-index:12000">
        <div id="loading" style="width: 100%"></div>
        <div id="orderData" style="width: 100%">
        </div>
    </div>
</asp:Content>
