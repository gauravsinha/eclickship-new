﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AccountCreated.aspx.cs" Inherits="EasyPostDemo.Account.AccountCreated" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: justify"><b>You have created your account successfully. You account is currently under review and once it is approved you will be able to login using your email address and password. We will send you email once your account is approved.</b></div>
</asp:Content>
