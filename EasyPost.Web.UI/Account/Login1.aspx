﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login1.aspx.cs" Inherits="EasyPostDemo.Account.Login" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
  
   <div class="col-lg-6 login-left">
       <asp:Literal runat="server"  id="FailureText"></asp:Literal>
     <div>
      <label>Email Address</label><br>
      <asp:TextBox id="UserName" runat="server" CssClass="fo-input"></asp:TextBox></div>
      <div style="padding-top:20px;padding-bottom:20px;">
      <label>Password</label><br>
      <asp:TextBox id="Password" CssClass="fo-input" runat="server" TextMode="Password"></asp:TextBox></div>
      <div style="padding-bottom:20px;">
       <input type="checkbox" name="checkbox"> Remember me? <a href="~/Account/ForgotPwd" runat="server">Forgot Password ?</a>
      </div>
      <div>
          <asp:Button ID="btnLogin" runat="server" CssClass="fo-button" Text="Login" OnClick="btnLogin_Click" />
        
      </div>
   </div>
   <div class="col-lg-6 login-right">
    <img src="<%=Page.ResolveClientUrl("~/Images/login.JPG")%>" height="300"  alt=""/> </div>
   <script>
       $(document).ready(function () {
           $("#log").addClass("pagebutton");
       });
</script>

</asp:Content>
