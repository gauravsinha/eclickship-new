﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageUsers.aspx.cs" Inherits="UFS.Web.UI.Account.ManageUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <style type="text/css">
        .grid-container {
            width: 100%;
            max-width: 1200px;
        }

        /*-- our cleafix hack -- */
        .row1:before,
        .row1:after {
            content: "";
            display: table;
            clear: both;
        }

        [class*='coll-'] {
            float: left;
            min-height: 1px;
            width: 16.66%;
            /*-- our gutter -- */
            padding: 12px;
            background-color: #0079ca;
        }

        .coll-1 {
            width: 27.5%;
        }

        .coll-12 {
            width: 9%;
        }

        .coll-2 {
            width: 27.5%;
            background-color: white;
            color: black;
            height: 50px;
        }

        .coll-21 {
            width: 9%;
            background-color: white;
            color: black;
            height: 50px;
        }


        .coll-3 {
            width: 50%;
        }

        .colll-4 {
            width: 66.66%;
        }

        .coll-5 {
            width: 83.33%;
        }

        .coll-6 {
            width: 100%;
        }

        .outline, .outline * {
            outline: 1px solid #0089da;
        }

        /*-- some extra column content styling --*/
        [class*='coll-'] > p {
            background-color: transparent;
            padding: 0;
            margin: 0;
            text-align: center;
            color: white;
            outline: none;
        }
    </style>
    <div style="width: 100%; top: 200px; text-align: center" id="dvLoading">
        <h2>Loading Users List<br />
            <img src="../Images/waiting.gif" />
        </h2>
    </div>
    <hr />
    <div class="grid-container outline" id="mainGrid">
        <div class="row1" id="dvHeader">
            <div class="coll-1">
                <p>Name / Company</p>
            </div>
            <div class="coll-1">
                <p>Email Address</p>
            </div>
            <div class="coll-12">
                <p>Fedex</p>
            </div>
            <div class="coll-12">
                <p>USPS</p>
            </div>
            <div class="coll-12">
                <p>UPS</p>
            </div>
            <div class="coll-12">
                <p>Status</p>
            </div>
            <div class="coll-12">
                <p>Action</p>
            </div>
        </div>

    </div>
    <script>
        //$(function () {
        LoadUser();
        function LoadUser() {
            $.ajax({
                type: "GET",
                url: "ManageUsers.aspx/GetData",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    //console.log(msg.d);
                    debugger;
                    if (msg.d != null) {
                        if (msg.d.length > 0) {
                            var content = "";
                            // alert(msg.d.length);
                            for (var i = 0; i < msg.d.length; i++) {
                                // alert(msg.d[i].Company);
                                //  debugger;
                                var displayApp = msg.d[i].IsApproved == 0 ? "" : "none";
                                var displaySave = msg.d[i].IsApproved == 1 ? "" : "none";
                                var displayDel = msg.d[i].IsApproved == 1 ? "" : "none";
                                var status = msg.d[i].IsApproved == 1 ? "Approved" : "UnApproved";
                                var user = msg.d[i].LastName + ", " + msg.d[i].FirstName;
                                // alert(displayApprove);
                                content = '<div class="row1"><div class="coll-2">' + user + " / " + msg.d[i].Company + '</div>' +
                                                '<div class="coll-2">' + msg.d[i].EmailAddress + '</div><div class="coll-21" style="text-align:center">' +
                                                   ' <img  src="../Images/setting.png" alt="Manage Rates"  style="outline: none; cursor: pointer;" title="Manage UPS Rates" onclick="ManageRates(this,\'Federal Express\',\'' + user + '\');" />' +
                                                '</div>' +
                                                '<div class="coll-21"  style="text-align:center">' +
                                                     ' <img  src="../Images/setting.png"  style="outline: none; cursor: pointer;" alt="Manage Rates" title="Manage UPS Rates" onclick="ManageRates(this,\'USPS\',\'' + user + '\');" />' +
                                                '</div>' +
                                                '<div class="coll-21"  style="text-align:center">' +
                                                    ' <img  src="../Images/setting.png" alt="Manage Rates"  style="outline: none; cursor: pointer;" title="Manage UPS Rates" onclick="ManageRates(this,\'UPS\',\'' + user + '\');" />'
                                                + '</div>'
                                                + '<div class="coll-21" style="text-align: center" id="dvstatus">' + status + '</div>'
                                                + '<div class="coll-21" style="text-align: center">' +
                                                '<input type="hidden" name="userId" value="' + msg.d[i].UserId + '"/>'
                                                    + '<img src="../Images/accept.png" title="Approve" id="approve" style="outline: none; cursor: pointer;display:' + displayApp + '" onclick="Approve(this,1)" >'
                                                  //+ '  <img src="../Images/update.PNG" title="Update Discounts" id="update"  style="outline: none; cursor: pointer;display:' + displaySave + '" onclick="UpdateDiscounts(this);">'
                                                + '   <img src="../Images/delete.png" Title="Disable" id="disable"  style="outline: none; cursor: pointer;display:' + displayDel + '" onclick="Approve(this,0)" />'
                                              + '  </div>'
                                            + '</div>';
                                //  alert(content);
                                $(content).appendTo($("#mainGrid")).slideDown();
                            }
                            $("#dvLoading").hide();
                        } else {
                            $("#dvLoading").html("<b>No user found</b>");
                        }
                    }
                },
                error: function (x, e) { $("#dvLoading").html("<span style='color:red'><b>Unable to load users. Please try again or contain System Administrator.</b></span>"); }
            });
        }
        function Approve(args, arg2) {
            $(args).prop("src", "../Images/waiting.gif");
            var userId = $(args).parent().parent().find("input[name=userId]").val();
            var ret = "1";
            if (arg2 == "1") {
                //  alert("Updating");
                $(args).parent().parent().find("#dvstatus").html("Updating..");
                // ret = UpdateDiscounts(args);
            }
            if (ret != "0") {
                $.ajax({
                    type: "POST",
                    url: "ManageUsers.aspx/Approve",
                    data: JSON.stringify({ 'userId': userId, 'isApprove': arg2 }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (msg) {
                        if (msg.d == true) {
                            if (arg2 == "1") {
                                $(args).parent().parent().find("#dvstatus").html("Approved");
                                $(args).hide();
                                $(args).parent().find("#disable").show();
                                $(args).parent().find("#update").show();
                                $(args).parent().find("#disable").prop("src", "../Images/delete.png");
                            }
                            if (arg2 == "0") {
                                $(args).parent().parent().find("#dvstatus").html("UnApproved");
                                $(args).parent().find("#approve").show();

                                $(args).parent().find("#disbale").hide();
                                $(args).parent().find("#update").hide();
                                $(args).hide();
                            }
                        } else {
                            alert("Unable to approve user this time");
                        }
                    },
                    error: function (x, e) { alert("Unable to approve user at this time please try again later"); }
                });
            } else {
                $(args).parent().parent().find("#dvstatus").html("UnApproved");
                $(args).prop("src", "../Images/accept.png");
            }
        }
        function UpdateDiscounts(args) {
            $(args).prop("src", "../Images/waiting.gif");
            var fedex = $(args).parent().parent().find("input[name=fedex]").val();
            var usps = $(args).parent().parent().find("input[name=usps]").val();
            var ups = $(args).parent().parent().find("input[name=ups]").val();
            var userId = $(args).parent().parent().find("input[name=userId]").val();
            if (fedex == "" || usps == "" || ups == "") {
                alert("Discount rates are mandatory");
                $(args).prop("src", "../Images/update.PNG");
                return 0;
            } else {
                var f = UpdateRates(args, userId, fedex, "FedEx");
                var us = UpdateRates(args, userId, usps, "USPS");
                var up = UpdateRates(args, userId, ups, "UPS");
            }
        }
        function UpdateRates(args, userId, discount, carrier, service) {
            $.ajax({
                type: "POST",
                url: "ManageUsers.aspx/UpdateDiscountRates",
                data: JSON.stringify({ 'userId': userId, 'carriername': carrier, 'discount': discount }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (msg) {
                    //alert(msg.d);
                    if (msg.d == "true") {
                        // alert("Approved Succesfully");
                        //  LoadUser();
                        if (carrier == "UPS") {
                            $(args).prop("src", "../Images/update.PNG");
                        }
                        return 1;
                    } else {
                        alert("Unable to update rate for carrier " + carrier);
                        return -1;
                    }
                },
                error: function (x, e) { alert("Error while updating rate for carrier " + carrier); }
            });
        }
        function ManageRates(arg, carrier, user) {
            debugger;
            var userId = $(arg).parent().parent().find("input[name=userId]").val();
            var url = "ManageDiscounts.aspx?userid=" + userId + "&car=" + carrier + "&user=" + user;
            window.open(url, "Manage Discounts", "channelmode=0,directories=0,fullscreen=0,height=500px,width=600px,location=0,menubar=0,resizable=0,scrollbars=0,status=0,titlebar=0,toolbar=0,", true);
        }

        //  });

    </script>
    <script>
        $(document).ready(function () {
            $("#manageUsers").addClass("pagebutton");
        });
    </script>

</asp:Content>
