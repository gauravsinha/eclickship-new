﻿using System;
using System.Collections.Generic;
using System.Linq;
using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EasyPostDemo.Account
{
    public partial class Login1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] != null)
                {
                    Response.Redirect("~/Dashboard/LandingPage.aspx");
                }
                if (Request.QueryString["hmac"] != null && Request.QueryString["shop"] != null && Request.QueryString["code"] != null)
                {
                    Response.Redirect("Register.aspx?code=" + Request.QueryString["code"].ToString() + "&hmac=" + Request.QueryString["hmac"] + "&shop=" + Request.QueryString["shop"]);                   
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                MSSQLUserOperations usrOp = new MSSQLUserOperations();
                UserDetail ssUser = usrOp.ValidateLogin(UserName.Text, Password.Text);
                if (ssUser != null)
                {
                    Session["user"] = ssUser;
                    var returnUrl = Request.QueryString != null && Request.QueryString["ReturnUrl"] != null ? Request.QueryString["ReturnUrl"] : "";
                    if (!string.IsNullOrWhiteSpace(returnUrl))
                        Response.Redirect(returnUrl);
                    else
                    {
                        if(chkSync.Checked)
                        Response.Redirect("~/Dashboard/LandingPage.aspx");
                        else
                            Response.Redirect("~/Dashboard/Default.aspx");
                    }
                }
                else
                {
                    FailureText.Text = "Invalid Login Detail. Try Again with valid credentials";
                }
            }
            catch (Exception ex)
            {
                FailureText.Text = ex.Message;
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("Register.aspx");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}