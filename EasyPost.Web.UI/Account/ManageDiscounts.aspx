﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageDiscounts.aspx.cs" EnableEventValidation="false" Inherits="UFS.Web.UI.Account.ManageDiscounts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="row container_page container" style="">
                <div class="col-md-12" style="padding: 20px 20px 20px 20px;">
                    <h2 class="main_heading" style="margin-bottom: 30px;">
                        <div id="dvCarrier" runat="server"></div>
                    </h2>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="grdDiscounts" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" runat="server" OnRowDataBound="grdDiscounts_RowDataBound" AllowPaging="true" ShowFooter="true"
                                OnPageIndexChanging="OnPaging" OnRowEditing="EditDiscount"
                                OnRowUpdating="UpdateDiscount" OnRowCancelingEdit="CancelEdit"
                                PageSize="40" CellPadding="4" BackColor="White" BorderColor="#0079ca" BorderStyle="Solid" Width="100%" BorderWidth="1px">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="1%" HeaderText="" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiscountId" runat="server"
                                                Text='<%# Eval("Id")%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblDiscountId" runat="server"
                                                Text=''></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="40%" HeaderText="Service">
                                        <ItemTemplate>
                                            <asp:Label ID="lblService" runat="server"
                                                Text='<%# Eval("ServiceName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblService" runat="server"
                                                Text='<%# Eval("ServiceName")%>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlService" runat="server" Width="99%"></asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField ItemStyle-Width="40%" HeaderText="Discount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiscount" runat="server"
                                                Text='<%# Eval("Discount")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDiscount" ForeColor="Black" Width="98%" runat="server"
                                                Text='<%# Eval("Discount")%>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtDiscount" ForeColor="Black" Width="98%" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" HeaderText="Edit" />
                                    <asp:TemplateField HeaderText="Add/Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRemove" runat="server"
                                                CommandArgument='<%# Eval("Id")%>'
                                                OnClientClick="return confirm('Do you want to delete?')"
                                                Text="Delete" OnClick="DeleteDiscount"></asp:LinkButton>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btnAdd" ForeColor="Black" runat="server" Text="Add"
                                                OnClick="AddDiscount" />
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <FooterStyle Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#0079ca" Font-Bold="True" BorderColor="#0079ca" ForeColor="White" Height="40px" CssClass="list-pad" />
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="grdDiscounts" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
