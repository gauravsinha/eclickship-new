﻿using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UFS.Web.UI.Account
{
    public partial class ManageUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["user"] == null)
                {
                    Response.Redirect("~/Account/Login?ReturnUrl=~/Account/ManageUsers");
                }
                else
                {
                    //FillData();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static List<UserDetail> GetData()
        {
            try
            {
                return new MSSQLUserOperations().GetUsers();
            }
            catch (Exception)
            {
                return null;
            }
        }

        [WebMethod]
        public static bool Approve(string userId, string isApprove)
        {
            try
            {
                return new MSSQLUserOperations().ApproveUser(userId, Convert.ToInt32(isApprove));
            }
            catch (Exception)
            {
                return false;
            }
        }

        [WebMethod]
        public static string UpdateDiscountRates(string userId, string carriername, List<string> discount, List<string> service)
        {
            try
            {
                for (int i = 0; i < discount.Count; i++)
                {
                    new MSSQLUserOperations().UpdateDiscountRates(userId, carriername, discount[i], service[i].Trim());
                }

                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [WebMethod]
        public static List<CarierDiscount> GetUserDiscountRates(string userId, string carriername)
        {
            try
            {
                List<CarierDiscount> discounts = new MSSQLUserOperations().GetDiscounts(Convert.ToInt32(userId), carriername);

                return discounts;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}