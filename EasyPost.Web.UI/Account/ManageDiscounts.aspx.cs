﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UFS.Web.BusinesEntities;
using UFS.Web.DataOperations;

namespace UFS.Web.UI.Account
{
    public partial class ManageDiscounts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Request.QueryString != null && Request.QueryString["userid"] != null && Request.QueryString["car"] != null)
                {
                    int userId = Convert.ToInt32(Request.QueryString["userid"]);
                    string carr = Request.QueryString["car"].ToString();
                    string userName = Request.QueryString["user"].ToString();
                    dvCarrier.InnerHtml = "Manage User Discounts for " + carr + " of user " + userName;
                    BindGrid();
                }
            }
        }
        public static List<CarierDiscount> GetUserDiscountRates(string userId, string carriername)
        {
            try
            {
                List<CarierDiscount> discounts = new MSSQLUserOperations().GetDiscounts(Convert.ToInt32(userId), carriername);

                return discounts;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();
            grdDiscounts.PageIndex = e.NewPageIndex;
            grdDiscounts.DataBind();
        }

        private void BindGrid()
        {
            int userId = Convert.ToInt32(Request.QueryString["userid"]);
            string carriername = Request.QueryString["car"].ToString();
            List<CarierDiscount> data = new MSSQLUserOperations().GetDiscounts(Convert.ToInt32(userId), carriername);
            if (data.Count <= 0)
            {
                data = new List<CarierDiscount>();
                data.Add(new CarierDiscount() { CareerName = "", Id = 0, Discount = "", ServiceName = "" });
            }
            // if (data == null || (data != null && data.Count> 0))
            // {
            grdDiscounts.DataSource = data;
            grdDiscounts.DataBind();
            //  }

        }

        protected void AddDiscount(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(Request.QueryString["userid"]);
            string carriername = Request.QueryString["car"].ToString();
            GridViewRow row = (GridViewRow)((sender as Button).NamingContainer);
            string discount = ((TextBox)row.FindControl("txtDiscount")).Text;
            string service = ((DropDownList)row.FindControl("ddlService")).SelectedValue.ToString();
            new MSSQLUserOperations().AddDiscounts(userId, carriername, discount, service);
            BindGrid();
        }
        protected void EditDiscount(object sender, GridViewEditEventArgs e)
        {
            grdDiscounts.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdDiscounts.EditIndex = -1;
            BindGrid();
        }

        protected void UpdateDiscount(object sender, GridViewUpdateEventArgs e)
        {
            string Id = ((Label)grdDiscounts.Rows[e.RowIndex]
                                .FindControl("lblDiscountId")).Text;
            string discount = ((TextBox)grdDiscounts.Rows[e.RowIndex]
                                .FindControl("txtDiscount")).Text;
            string service = ((Label)grdDiscounts.Rows[e.RowIndex]
                                .FindControl("lblService")).Text;
            new MSSQLUserOperations().AddDiscounts(0, "", discount, service, true, Convert.ToInt32(Id));
            grdDiscounts.EditIndex = -1;
            BindGrid();
        }
        protected void DeleteDiscount(object sender, EventArgs e)
        {
            LinkButton lnkRemove = (LinkButton)sender;
            int id = Convert.ToInt32(lnkRemove.CommandArgument);
            new MSSQLUserOperations().DeleteDiscounts(id);

            BindGrid();
        }

        protected void grdDiscounts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                        string carriername = Request.QueryString["car"].ToString();
                        //Find the DropDownList in the Row
                        DropDownList ddlService = (e.Row.FindControl("ddlService") as DropDownList);
                        ddlService.DataSource = new MSSQLUserOperations().GetDiscounts(carriername);
                        //ddlCountries.DataTextField = "";
                        //ddlCountries.DataValueField = "";
                        ddlService.DataBind();

                        //Add Default Item in the DropDownList
                        ddlService.Items.Insert(0, new ListItem("Please select"));
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}