﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ForgotPwd.aspx.cs" Inherits="EasyPostDemo.Account.ForgotPwd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p class="validation-summary-errors">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:ValidationSummary ID="valSumm" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <div class="col-md-12 re-heading" runat="server" id="dvTitle">
        Forgot Password
    </div>
    <div class="col-md-12">
        <div class="col-lg-6 login-left">
            <div class="row re-div">
                <div class="col-md-3">
                    <label>Enter Email Address</label>
                </div>
                <div class="col-md-9">
                    <asp:TextBox runat="server" ID="Email" CssClass="fo-input" /><asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                        CssClass="field-validation-error" Text="*" ErrorMessage="The email address field is required." /><asp:RegularExpressionValidator runat="server" ControlToValidate="Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            CssClass="field-validation-error" Text="*" ErrorMessage="Invalid Email Address"></asp:RegularExpressionValidator>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-6 login-left">
            <div class="row re-div">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <div class="col-md-5">
                        <input type="reset" name="btnReset" class="re-button" value="Clear" style="width:90%" />
                    </div>
                    
                    <div class="col-md-5">
                        
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" CssClass="re-button" Style="width: 90%;" />

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
