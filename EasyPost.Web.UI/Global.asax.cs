﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using EasyPostDemo;
using UFS.Web.UI;

namespace EasyPostDemo
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            // AuthConfig.RegisterOpenAuth();
           // RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }
        void Application_AcquireRequestState(object sender, EventArgs e)
        {
            //CurrentHandler).AppRelativeVirtualPath
            // Session is Available here
            HttpContext context = HttpContext.Current;
            //if(context.CurrentHandler!=null && context.CurrentHandler is System.Web.UI.TemplateControl && !((System.Web.UI.TemplateControl)context.CurrentHandler).AppRelativeVirtualPath.ToUpper().Contains("LOGIN") &&   context.Session["user"] == null)
            //{
            //    Response.Redirect("~/Account/Login.aspx");
            //}
        }
        void  Application_BeginRequest(object sender,EventArgs e)
        {
           
        }
        void Session_End(object sender,EventArgs e)
        {
            try
            {
              Response.Redirect("~/Account/Login.aspx");
            }
            catch (Exception ex)
            {
            }
        }
        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }
    }
}
