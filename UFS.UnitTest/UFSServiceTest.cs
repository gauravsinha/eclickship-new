﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;

namespace UFS.UnitTest
{
    [TestClass]
    public class UFSServiceTest
    {
        UFSService.UFSServiceClient _client;
        OnlineUFS.UFSServiceClient _onlineClient;
        
        [TestMethod]
        public void GetRatesTest()
        {
            _client = new UFSService.UFSServiceClient();
            UFSService.GetRateRequest rq = new UFSService.GetRateRequest();
           // rq.Carrier = "FedEx";
            rq.Dimension = new UFSService.Dimension();
            rq.Dimension.Height = 2;
            rq.Dimension.Length = 2;
            rq.Dimension.Width = 2;
            rq.FromZipCode = "11507";
            rq.ToZipCode = "60642";
            rq.Weight = 4;
            UFSService.GetRateResponse[] rs = _client.GetAllRates(rq);
            StringBuilder sb = new StringBuilder();
            foreach (UFSService.GetRateResponse item in rs)
            {
                sb.AppendLine(item.Rate.carrier + " - " + item.Rate.service + " " + item.Rate.rate);
            }
           // global::System.Windows.Forms.MessageBox.Show(sb.ToString());
            Assert.AreNotEqual(rs, null);
        }
        [TestMethod]
        public void GetOnineRatesTest()
        {
            _onlineClient = new OnlineUFS.UFSServiceClient();
            OnlineUFS.GetRateRequest rq = new OnlineUFS.GetRateRequest();
           // rq.Carrier = "FedEx";
            rq.Dimension = new OnlineUFS.Dimension();
            rq.Dimension.Height = 10;
            rq.Dimension.Length = 10;
            rq.Dimension.Width = 10;
            rq.FromZipCode = "10001";
            rq.ToZipCode = "29223";
            rq.Weight = 10;
            OnlineUFS.GetRateResponse[] rs = _onlineClient.GetAllRates(rq);
           UFS.UnitTest.OnlineUFS.GetRateResponse rss = _onlineClient.GetLowestRate(rq);
            Assert.AreNotEqual(rs.Length, 0);
        }


    }
}
