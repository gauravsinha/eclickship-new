﻿using UFS.Web.BusinesEntities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.UnitTest
{
    [TestClass]
    public class PackgeAcceptTest
    {
        [TestMethod]
        public void TestXMLLoad1()
        {
            UFS.UPSWrapper.USFService service = new UPSWrapper.USFService();
            List<ParcelDetails> lstParcel = new List<ParcelDetails>();
            lstParcel.Add(new ParcelDetails()
            {
                Signature = "1",
                Weight = "1",
                Width = "2",
                Height = "2",
                Length = "2",
                DeclaredValue = "140",
                PredefinedParcel = "Custom"
            });
            // lstParcel.Add(new ParcelDetails()
            // {
            //     Signature = "1",
            //     Weight = "4",
            //     PredefinedParcel = "Custom"
            // }
            // );
            //lstParcel.Add(
            //   new ParcelDetails()
            //    {
            //        Signature = "1",
            //        Weight = "3",
            //        PredefinedParcel = "Custom"
            //    });
            service.Shipment = new ShipmentDetail
            {
                FromAddress = new Address()
                {
                    Name = "Dharmendra Kumar",
                    Company = "ABC Associates",
                    Country = "US",
                    State = "SC",
                    Street1 = "1 E 161st St.",
                    Street2 = " ",
                    Phone = "San Francisco",
                    Zip = "29223",
                    City = "San Francisco"
                },
                ToAddress = new Address()
                {
                    Name = "Icomm Int",
                    Company = "Test Compy",
                    Country = "US",
                    State = "NY",
                    Street1 = "New Co 1230",
                    Street2 = " Broadway 2nd Floor",
                    Phone = "2125942177",
                    Zip = "10001",
                    City = "New York"
                },
                Parcel = lstParcel[0],
                Parcels = lstParcel
            };
            service.Create();
            if (service.Messages == null)
            {
                service.ShipmentDigest = service.Rates[0].RateId;
                service.Buy();
            }
            //  Assert.AreEqual(service.Image, "");

        }

        [TestMethod]
        public void GetRateTest()
        {
            UFS.UPSWrapper.USFService service = new UPSWrapper.USFService();
            service.Shipment = new ShipmentDetail()
            {
                Parcel = new ParcelDetails()
                {
                    Height = "2",
                    Width = "2",
                    Length = "2",
                    Weight = "2"

                },
                FromAddress = new Address()
                {
                    Name = "Dharmendra Kumar",
                    Company = "ABC Associates",
                    Country = "US",
                    State = "SC",
                    Street1 = "1 E 161st St.",
                    Street2 = " ",
                    Phone = "San Francisco",
                    Zip = "29223",
                    City = "San Francisco"
                },
                ToAddress = new Address()
                {
                    Name = "Icomm Int",
                    Company = "Test Compy",
                    Country = "US",
                    State = "NY",
                    Street1 = "New Co 1230",
                    Street2 = " Broadway 2nd Floor",
                    Phone = "2125942177",
                    Zip = "10001",
                    City = "New York"
                }
            };
             service.CreateNegotiatedRate();
            if (service!=null && service.Rates.Count>0)
            {
                //do it here
            }
        }
    }
}
