﻿using UFS.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Configuration;

namespace UFS.Messaging.EmailService
{
    public class EmailService
    {
        private static config _config;
        public static config Config
        {
            get
            {
                if (_config == null)
                    _config = ConfigReader.Instance.LoadData();

                return _config;
            }
        }
        private bool _isBCCRequired = true;
        private MailAddress _fromAddress;

        private MailAddress FromAddress
        {
            get { return _fromAddress; }
            set { _fromAddress = value; }
        }
        private MailAddress _toAddress;

        public MailAddress ToAddress
        {
            get { return _toAddress; }
            set { _toAddress = value; }
        }
        private MailAddress _bccAddress;

        public MailAddress BccAddress
        {
            get { return _bccAddress; }
            set { _bccAddress = value; }
        }
        private MailAddress _ccAddress;

        public List<MailAddress> CCMailAddressList { get; set; }

        public MailAddress CcAddress
        {
            get { return _ccAddress; }
            set { _ccAddress = value; }
        }
        private string _mailBody;

        public string MailBody
        {
            get { return _mailBody; }
            set { _mailBody = value; }
        }
        private string _subject;

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }

        private List<Attachment> _attachment;

        public string AttachmentFilePath
        {
            set
            {
                if (_attachment == null)
                {
                    _attachment = new List<Attachment>();
                }
                _attachment.Add(new Attachment(value, "label.jpg"));
            }
        }
        public Stream AttachmentStream
        {
            set
            {
                if (_attachment == null)
                {
                    _attachment = new List<Attachment>();
                }
                _attachment.Add(new Attachment(value, "Label.jpg", System.Net.Mime.MediaTypeNames.Image.Jpeg));
            }
        }
        public Dictionary<Stream,string> Attachments
        {
            set
            {
                if (_attachment == null)
                {
                    _attachment = new List<Attachment>();
                }
                int i = 0;
                foreach (var item in value)
                {
                    i++;
                    if (item.Value == "pdf")
                    {
                        _attachment.Add(new Attachment(item.Key, "Label" + i.ToString() + "." + item.Value, System.Net.Mime.MediaTypeNames.Application.Pdf));
                    }
                    else
                    {
                        _attachment.Add(new Attachment(item.Key, "Label" + i.ToString() + "." + item.Value, System.Net.Mime.MediaTypeNames.Image.Jpeg));
                    }
                }

            }
        }

        public EmailService()
        {

        }

        public EmailService(string to, bool isBCC = true)
        {
            _isBCCRequired = isBCC;
            ToAddress = new MailAddress(to);
        }
        public EmailService(string from, string to)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(from))
                {
                    _fromAddress = new MailAddress(from);
                }
                _toAddress = new MailAddress(to);
                _isBCCRequired = true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public EmailService(string from, string to, string cc)
            : this(from, to)
        {
            try
            {


            }
            catch (Exception)
            {

                throw;
            }
        }
        public EmailService(string from, string to, string cc, string bcc)
            : this(from, to, cc)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(from))
                {
                    _fromAddress = new MailAddress(from);
                }
                _toAddress = new MailAddress(to);
                _bccAddress = new MailAddress(bcc);
                _ccAddress = new MailAddress(cc);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string SendMail()
        {

            SmtpClient client = null;// GetSMTPClient();
            if (Config.Common.EmailSetting.UseSMTPWithCredential)
            {
                client = GetSMTPClient();
            }
            else
            {
                client = GetSMTPClientWithoutCredential();
                _fromAddress = new MailAddress("noreply@unitedfreightsavers.com");
            }

            try
            {
                client.Send(BuildEmailMessage());
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        private static SmtpClient GetSMTPClient()
        {
            SmtpClient client = new SmtpClient()
            {
                Host = Config.Common.EmailSetting.EmailHostName,// WebConfigurationManager.AppSettings["EmailHostName"],
                Port = Convert.ToInt32(Config.Common.EmailSetting.EmailHostPort),// WebConfigurationManager.AppSettings["EmailHostPort"]),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(Config.Common.EmailSetting.EmailFrom,Config.Common.EmailSetting.EmailAccountPassword),// WebConfigurationManager.AppSettings["EmailFrom"], WebConfigurationManager.AppSettings["EmailAccountPassword"])
            };
            return client;
        }
        private static SmtpClient GetSMTPClientWithoutCredential()
        {
            SmtpClient client = new SmtpClient();
            return client;
        }
        private MailMessage BuildEmailMessage()
        {
            if (_fromAddress == null)
            {
                _fromAddress = new MailAddress(Config.Common.EmailSetting.EmailFrom, Config.Common.EmailSetting.FromDisplayName);// WebConfigurationManager.AppSettings["EmailFrom"], WebConfigurationManager.AppSettings["FromDisplayName"]);
            }
            MailMessage msg = new MailMessage(_fromAddress, _toAddress);

            msg.Body = _mailBody;
            msg.Subject = _subject;

            if (_attachment != null && _attachment.Count > 0)
            {
                foreach (var item in _attachment)
                {
                    msg.Attachments.Add(item);
                }

            }
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
            if (_ccAddress != null)
            {
                msg.CC.Add(_ccAddress);
            }
            if (CCMailAddressList != null && CCMailAddressList.Count > 0)
            {
                foreach (var item in CCMailAddressList)
                {
                    msg.CC.Add(item);
                }
            }

            if (Config.Common.EmailSetting.CCEmailAddress != null && _isBCCRequired)
            {
                string cc1Mail = Config.Common.EmailSetting.CCEmailAddress;// WebConfigurationManager.AppSettings["CCEmailAddress"];
                if (!string.IsNullOrWhiteSpace(cc1Mail))
                {
                    msg.Bcc.Add(cc1Mail);
                }
            }
            if (_bccAddress != null && _isBCCRequired)
            {
                msg.Bcc.Add(_bccAddress);
            }
            return msg;
        }
    }
}
