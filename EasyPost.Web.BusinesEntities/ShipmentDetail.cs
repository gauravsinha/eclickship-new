﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.Web.BusinesEntities
{
    [Serializable]
    public class ShipmentDetail
    {
        public string Carrier { get; set; }
        public Address FromAddress { get; set; }
        public Address ToAddress { get; set; }
        public ParcelDetails Parcel { get; set; }

        public string MessageDigest { get; set; }
        public List<ParcelDetails> Parcels { get; set; }
        public string[] EmailAddress { get; set; }
    }
}
