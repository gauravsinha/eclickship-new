﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.Web.BusinesEntities
{
    [Serializable]
    public class UserAddress
    {
        public int AddressId { get; set; }
        public string Street1 { get; set; }

        public string Street2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string FormattedPhone
        {
            get
            {
                return Phone.Replace(")", "").Replace("(", "").Replace("-", "");
            }
        }

        public bool IsResidential { get; set; }

    }
}
