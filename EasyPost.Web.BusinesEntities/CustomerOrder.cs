﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.Web.BusinesEntities
{
    [Serializable]
    public class OrderDetail
    {
        public string SiteItemID { get; set; }
        public string OrderStatus { get; set; }
        public string DateShipped { get; set; }
        public string SKU { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public int? QuantityOrdered { get; set; }
        public int? QuantityShipped { get; set; }
        public int? QuantityUnfillable { get; set; }
        public string CurrencyISO { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? UnitTax { get; set; }
        public decimal? ShippingPrice { get; set; }
        public decimal? ShippingTax { get; set; }
        public decimal? ShippingDiscount { get; set; }
        public string GiftMessage { get; set; }
        public decimal? GiftWrapPrice { get; set; }
        public decimal? GiftWrapTax { get; set; }
        public string ShippingServiceOrdered { get; set; }
        public string ShippingServiceActual { get; set; }
        public string ShippingTracking { get; set; }
        public string ShippingActualWeight { get; set; }
        public decimal? ShippingActualCharge { get; set; }
        public string ShippingCarrier { get; set; }
        public string Vendor { get; set; }
        public List<BundledItem> BundledItems { get; set; }
    }

    [Serializable]
    public class BundledItem
    {
        public string SKU { get; set; }
        public string Title { get; set; }
        public int? QuantityOrdered { get; set; }
    }

    [Serializable]
    public class CustomerOrder
    {
        public string SiteOrderID { get; set; }
        public string Site { get; set; }
        public string OrderStatus { get; set; }
        public string DateOrdered { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string StateOrRegion { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
    }
    public class SummaryData
    {
        public string Title { get; set; }
        public string SKU { get; set; }
        public string Quantity { get; set; }
        public string Weight { get; set; }
        public string OrderNumbers { get; set; }

        public string SummaryDate { get; set; }
    }

}
