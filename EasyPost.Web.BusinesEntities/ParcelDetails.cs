﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.Web.BusinesEntities
{
    [Serializable]
    public class ParcelDetails
    {
        public string Height { get; set; }
        public string Width { get; set; }
        public string Length { get; set; }
        public string Weight { get; set; }

        public string CarrierName { get; set; }
        public List<PackageDimensions> Dimension { get; set; }
        public bool IsSinglePackage { get; set; }
        public bool IsMultiplePackage { get; set; }
        public string PredefinedParcel { get; set; }

        public string CarrierCode { get; set; }
        public string Signature { get; set; }
        public string HoldForPickup { get; set; }
        public string DeclaredValue { get; set; }
        public bool IsCOD { get; set; }
        public string CODAmount { get; set; }
        public string CODMethod { get; set; }
        public UFSOptions UFSOption { get; set; }
    }

    [Serializable]
    public class PackageDimensions
    {
        public string Qunatity { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string DecVal { get; set; }
        public string PredefinedParcel { get; set; }        
        public string PredefinedParcelDesc { get; set; }
        public string CODAmount { get; set; }
        public string CODMethod { get; set; }

        decimal cd;
        public bool IsCOD { 
            get
            {
                return Decimal.TryParse(CODAmount, out cd) && cd > 0M;
            }
        }
    }

    [Serializable]
    public class UFSOptions
    {
        public bool VefiryAddress { get; set; }
        public bool SendEmailNotification { get; set; }
        public bool RecieveDeliveryConf { get; set; }
        public bool DeliverWithoutSign { get; set; }
        public bool DeliverOnSunday { get; set; }
        public bool UFSCarbonNuetral { get; set; }
    }
}
