﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.Web.BusinesEntities
{
    [Serializable]
   public class AuthorizationDetails
    {
        public AuthorizationDetails()
        {
            FedExKey = "";
            USPSKey = "";
            UPSKey = new UPSKeys();
        }
        public string FedExKey { get; set; }
        public string USPSKey { get; set; }

        public UPSKeys UPSKey { get; set; }

        public class UPSKeys
        {
            public UPSKeys()
            {
                AccessLicenseNumber = "";
                ShipperNumber = "";
                ShipperAccountNumber = "";
                UserName = "";
                Password = "";
                
            }
            public string AccessLicenseNumber { get; set; }
            public string ShipperNumber { get; set; }
            public string ShipperAccountNumber { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
        }
    }

    [Serializable]
    public class ShopifyAuthentication
    {
        public string AuthKey { get; set; }

        public string APIKey { get; set; }
        public string APIPassword { get; set; }

        public string StoreURL { get; set; }
    }
}
