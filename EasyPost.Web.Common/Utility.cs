﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UFS.Web.Common
{
    public static class CommonUtilities
    {
        public static string GetRandomString(int length = 10)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
        public static void SendErrorasMail(Exception ex)
        {
            try
            {
                UFS.Messaging.EmailService.EmailService srv = new Messaging.EmailService.EmailService();
                srv.ToAddress = new System.Net.Mail.MailAddress("dharmendra.hbti@gmail.com", "Dharmendra Kumar");
                srv.Subject = "Error in UFS Site";
                srv.MailBody = "<b>Error is </b><br/>" + ex.Message + "<br/> <b>Stack Trace is </b>" + ex.StackTrace+ "<br/> <b>Inner Exceptions is </b><br/>"+ex.InnerException;
                srv.SendMail();
            }
            catch (Exception)
            {
            }
        }
        public static void SendErrorasMail(string message)
        {

        }
    }
}
