﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace UFS.Web.Common
{
    public static class ControlsOperation
    {
        public static void BindDropDown(object dataSource,DropDownList control)
        {
            control.DataSource = dataSource;
            control.DataValueField = "Key";
            control.DataTextField = "Value";
            control.DataBind();
        }
        public static void BindDropDown(object dataSource, DropDownList control,string dataValueField,string dataTextField)
        {
            control.DataSource = dataSource;
            control.DataValueField = dataValueField;
            control.DataTextField = dataTextField;
            control.DataBind();
        }

    }
}
